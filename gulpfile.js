var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss', 'public/css', {
      includePaths: [__dirname + '/bower_components/']
    });

    mix.sass('admin.scss', 'public/css', {
      includePaths: [__dirname + '/bower_components/']
    });

    mix.copy('bower_components/font-awesome/fonts', 'public/fonts');

    mix.scripts([
      '../../../bower_components/jquery/dist/jquery.js',
      '../../../bower_components/foundation-sites/dist/foundation.js',
      '../../../bower_components/fullpage.js/jquery.fullPage.js',
      'app.js'
    ]);

    mix.scripts([
      '../../../bower_components/jquery/dist/jquery.js',
      '../../../bower_components/foundation-sites/dist/foundation.js',
      '../../../bower_components/foundation-datepicker/js/foundation-datepicker.js',
      '../../../bower_components/foundation-datepicker/js/locales/foundation-datepicker.bg.js',
      'admin.js',
    ], 'public/js/admin.js');

    mix.version(['css/app.css', 'css/admin.css', 'js/all.js', 'js/admin.js']);
});
