<?php namespace NescafeAlegria\Providers;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider {

	/**
	 * The event handler mappings for the application.
	 *
	 * @var array
	 */
	protected $listen = [
		'event.name' => [
			'EventListener',
		],
	];

	/**
	 * Register any other events for your application.
	 *
	 * @param  \Illuminate\Contracts\Events\Dispatcher  $events
	 * @return void
	 */
	public function boot(DispatcherContract $events)
	{
		parent::boot($events);
		if ( env('APP_ENV') === 'local' ) {
	    \Event::listen('kernel.handled', function ( $request, $response ) {
	        if ( $request->has('sql-debug') ) {
		            $queries = \DB::getQueryLog();
		            dd($queries);
		        }
		    });
		}
		//
	}

}
