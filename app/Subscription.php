<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use NescafeAlegria\CanBeImportedTrait;
use NescafeAlegria\Observers\SubscriptionObserver;
use Carbon\Carbon;


class Subscription extends \SleepingOwl\Models\SleepingOwlModel {

  use CanBeImportedTrait;
  use CanBeFilteredByUserTrait;

	public static function boot()
	{
		parent::boot();

		parent::observe(new SubscriptionObserver);
	}

	protected $fillable = [
		'email',
		'source'
	];


	public static $originalNames = [
		'email' => 'Мейл',
	];

	public function scopeActive($query)
	{
		return $query->where('subscribed', 1);
	}

	public function unsubscribe()
	{
		$this->subscribed = 0;
		$this->save();
	}

	public function subscribe()
	{
		$this->subscribed = 1;
		$this->save();
	}

	public function toggle()
	{
		if ($this->subscribed == 1) {
			$this->unsubscribe();
		}
		elseif ($this->subscribed == 0) {
			$this->subscribe();
		}
	}
}
