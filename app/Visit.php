<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use NescafeAlegria\CanBeFilteredByUserTrait;
use Carbon\Carbon;
use Auth;

class Visit extends Model {
	use CanBeFilteredByUserTrait;

	protected $fillable = [
		'trading_person_name',
		'poc_name',
		'bmb_number',
		'calendar_date',
		'audit_type',
		'start_date',
		'period',
		'machine_model',
		'machine_sn'
	];

	public static function findAndMarkAsVisited($verification)
	{
		$visit = static::where('calendar_date', $verification->calendar_date->toDateString())
			->where('trading_person_name', $verification->trading_person_name)
			->where('bmb_number', $verification->bmb_number)->first();

		if(!is_null($visit))
		{
			$visit->audit_type = $verification->audit_type;
			$visit->visited = true;
			$visit->save();
			return true;
		}

		return false;
	}

	public static function scopeForUser($query, $user_names)
	{
		if(is_array($user_names))
		{
			return $query->whereIn('trading_person_name', $user_names);
		}
		return $query->where('trading_person_name', $user_names);
	}

	public static function scopeOwned($query)
	{
		return $query->where('trading_person_name', Auth::user()->name);
	}

	public function getCalendarDateAttribute($value)
	{
		return Carbon::parse($this->attributes['calendar_date'])->format('d.m.Y');
	}

	public function getStartDateAttribute($value)
	{
		return Carbon::parse($this->attributes['start_date'])->format('d.m.Y');
	}

}
