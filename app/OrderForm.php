<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use NescafeAlegria\CanBeImportedTrait;
use NescafeAlegria\Observers\OrderFormObserver;
use Carbon\Carbon;

class OrderForm extends \SleepingOwl\Models\SleepingOwlModel {

	use CanBeImportedTrait;
	use CanBeFilteredByUserTrait;

	public static function boot()
	{
		parent::boot();

		parent::observe(new OrderFormObserver);
	}

	protected $fillable = ['first_name','last_name','phone_number','email','organization_name', 'city', 'beverages', 'employees', 'message', 'order_id', 'recipe_name', 'business_type'];

		public static $originalNames = [
		'first_name' => 'Име',
		'last_name' => 'Фамилия',
		'phone_number' => 'Телфон',
		'email' => 'Мейл',
		'organization_name' => 'Фирма',
		'city' => 'населено място',
		'beverages' => 'Брой напитки',
		'employees' => 'Брой Служители',
		'message' => 'Съобщение',
		'order_id' => 'Поръчка номер',
		'recipe_name' => 'Рецепта',
		'business_type' => 'Вид Бизнес',
	];

}
