<?php namespace NescafeAlegria;

trait CanBeFilteredByUserTrait {

	public function scopeFilterByUser($query, $filter)
	{
		if(is_array($filter))
		{
			return $query->whereIn('trading_person_name', $filter);
		}

		return $query->where('trading_person_name', $filter);
	}
}
