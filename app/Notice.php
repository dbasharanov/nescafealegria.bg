<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Notice extends \SleepingOwl\Models\SleepingOwlModel implements ModelWithImageFieldsInterface, SluggableInterface {

  // 
  use ModelWithImageOrFileFieldsTrait;
  use SluggableTrait;

  protected $fillable = ['title', 'slug', 'image', 'banner', 'summary','description','approved','meta_description','keywords'];
  protected $sluggable = [
    'build_from' => 'title',
    'save_to'    => 'slug',
  ];

  public function getImageFields()
  {
      return [
          'image' => ['attachments/notices/', function($directory, $originalName, $extension)
            {
              return $this->getSlug().'.'.$extension;
            }],
          'banner' => ['attachments/notices/banners', function($directory, $originalName, $extension)
            {
              return $this->getSlug().'.'.$extension;
            }]
      ];
  }

}
