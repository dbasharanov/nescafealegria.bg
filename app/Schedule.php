<?php namespace NescafeAlegria;

use Auth;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Schedule extends Model {

	protected $fillable = [
		'calendar_date',
		'period',
		'trading_person_name',
		'poc_name',
		'machine_sn',
		'machine_model',
		'location',
		'address',
		'visited',
		'bmb_number'
	];

	public static function scopeOwned($query)
	{
		return $query->where('trading_person_name', Auth::user()->name);
	}

	public function getCalendarDateAttribute($value)
	{
		return Carbon::parse($this->attributes['calendar_date'])->format('d.m.Y');
	}

}
