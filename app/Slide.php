<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
//use Cviebrock\EloquentSluggable\SluggableInterface;
//use Cviebrock\EloquentSluggable\SluggableTrait;

class Slide extends \SleepingOwl\Models\SleepingOwlModel implements ModelWithImageFieldsInterface {

  //
  use ModelWithImageOrFileFieldsTrait;
  //use SluggableTrait;

  protected $fillable = ['title','description','position','image','bullet', 'slug'];
  //protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug',];

  public function getImageFields()
  {
      return [
          'image' => ['attachments/slides/large', function($directory, $originalName, $extension)
            {
              return $originalName;
            }],
          'bullet' => ['attachments/slides/small', function($directory, $originalName, $extension)
            {
              return $originalName;
            }]
      ];
  }
}
