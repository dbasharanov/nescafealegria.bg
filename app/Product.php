<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Product extends \SleepingOwl\Models\SleepingOwlModel  implements ModelWithImageFieldsInterface, SluggableInterface {

	// 
  use ModelWithImageOrFileFieldsTrait;
  use SluggableTrait;

  protected $fillable = ['name', 'summary', 'description','beverages','image','meta_description','keywords'];
  protected $sluggable = [
    'build_from' => 'name',
    'save_to'    => 'slug',
  ];

  public function getImageFields()
  {
      return [
          'image' => ['attachments/products/', function($directory, $originalName, $extension)
            {
              return $this->getSlug().'.'.$extension;
            }]
      ];
  }

  public function beverages()
  {
    return $this->belongsToMany('NescafeAlegria\Beverage');
  }

  public static function getList()
  {
    return static::lists('name', 'id');
  }

  public function setBeveragesAttribute($beverages)
  {
    $this->beverages()->detach();
    if ( ! $beverages) return;

    if ( ! $this->exists) $this->save();

    $this->beverages()->attach($beverages);
  }

}
