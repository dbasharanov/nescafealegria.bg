<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Testimonial extends \SleepingOwl\Models\SleepingOwlModel implements ModelWithImageFieldsInterface, SluggableInterface {

  //
  use ModelWithImageOrFileFieldsTrait;
  use SluggableTrait;

  protected $fillable = ['name','description','position','image', 'company', 'type', 'business_id'];
  protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug',];

  public function getImageFields()
  {
      return [
          'image' => ['attachments/testimonials/', function($directory, $originalName, $extension)
            {
              return $this->getSlug() .'.'.$extension;
            }]
      ];
  }

}
