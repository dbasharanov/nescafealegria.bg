<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Machine extends \SleepingOwl\Models\SleepingOwlModel implements ModelWithImageFieldsInterface, SluggableInterface {

  // 
  use ModelWithImageOrFileFieldsTrait;
  use SluggableTrait;

  protected $fillable = ['name', 'slug', 'slogan','summary','description','image','meta_description','keywords', 'position'];
  protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug',];

  public function getImageFields()
  {
      return [
          'image' => ['attachments/machines/', function($directory, $originalName, $extension)
            {
              return $this->getSlug() .'.'.$extension;
            }]
      ];
  }

}
