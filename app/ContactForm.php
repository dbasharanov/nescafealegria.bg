<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithFileFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use NescafeAlegria\Observers\ContactFormObserver;
use NescafeAlegria\CanBeImportedTrait;
use Carbon\Carbon;

class ContactForm extends \SleepingOwl\Models\SleepingOwlModel implements ModelWithFileFieldsInterface {
	use ModelWithImageOrFileFieldsTrait;
	use CanBeImportedTrait;
	use CanBeFilteredByUserTrait;

	public static function boot()
	{
		parent::boot();

		parent::observe(new ContactFormObserver);
	}

	protected $fillable = ['first_name','last_name','phone_number','email','question_type', 'city', 'file', 'message'];

	public static $originalNames = [
		'first_name' => 'Име',
		'last_name' => 'Фамилия',
		'phone_number' => 'Телфон',
		'email' => 'Мейл',
		'question_type' => 'Въпрос',
		'city' => 'населено място',
		'file' => 'Прикачен файл',
		'message' => 'Съобщение',
	];

	public function getFileFields()
	{
		return [
			'file' => 'attachments/contact_form/',
			'other' => ['other_files/', function($directory, $originalName, $extension)
			{
				return $originalName;
			}]
		];
	}

}
