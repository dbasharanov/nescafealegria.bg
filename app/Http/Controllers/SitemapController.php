<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Controller;

use Illuminate\Http\Request;

use NescafeAlegria\Beverage;
use NescafeAlegria\Machine;
use NescafeAlegria\Product;
use DB;

class SitemapController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	  $beverage = DB::table('beverages')->orderBy('updated_at', 'desc')->first();
	  $machine =  DB::table('machines')->orderBy('updated_at', 'desc')->first();
	  $product = 	DB::table('products')->orderBy('updated_at', 'desc')->first();

	  return response()->view('sitemap.index', [
	      'beverage' => $beverage,
	      'machine' => $machine,
	      'product' => $product,
	  ])->header('Content-Type', 'text/xml');
	}

	public function beverages()
	{
	  $beverages = DB::table('beverages')->get();

	  return response()->view('sitemap.beverages', [
	      'beverages' => $beverages,
	  ])->header('Content-Type', 'text/xml');
	}

	public function machines()
	{
	  $machines = DB::table('machines')->get();

	  return response()->view('sitemap.machines', [
	      'machines' => $machines,
	  ])->header('Content-Type', 'text/xml');
	}

	public function products()
	{
	  $products = DB::table('products')->get();

	  return response()->view('sitemap.products', [
	      'products' => $products,
	  ])->header('Content-Type', 'text/xml');
	}

}
