<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Product;
use DB;

class ProductsController extends Controller {
	/**
 	 * Shows a list of available products
 	 *
 	 * @return Response
 	 */
	public function index() {
    $products = Product::all();
    $meta = [ 'title' => 'Продукти',
              'description' => 'Кафе, мляко, какао, 3 в 1',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/attachments/products/'.$products[0]->image ,
              'keywords' => 'Продукти, кафе, мляко, какао, 3 в 1'
        ];
    return view('products.index', compact('products','meta'));
	}

    public function show($slug) {
      $product = Product::findBySlug($slug);
      $prev = DB::table('products')->where('id', '<', $product->id)->orderBy('id', 'desc')->first();
      $next = DB::table('products')->where('id', '>', $product->id)->orderBy('id', 'asc')->first();

      $meta = [ 'title' => $product->name,
                'description' => strip_tags(html_entity_decode($product->meta_description)),
                'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/attachments/products/'.$product->image ,
                'keywords' => $product->keywords
              ];
      return view('products.show', compact('product', 'prev', 'next', 'meta'));
    }
}
