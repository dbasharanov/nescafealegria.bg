<?php namespace NescafeAlegria\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

  use DispatchesCommands, ValidatesRequests;

  #$actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  #$this->actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

}
