<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Controller;
use NescafeAlegria\OrderForm;
use NescafeAlegria\ContactForm;
use NescafeAlegria\Subscription;
use NescafeAlegria\Consumer;


use Illuminate\Http\Request;

use NescafeAlegria\Http\Requests\ImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use File;
use Exception;
use Storage;
#use Illuminate\Support\Facades\Auth;

class ExportsController extends Controller {

    public function __construct()
    {
        $this->beforeFilter('admin.auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('exports.index');
	}

	public function orders(Request $request)
	{
		$from_date = $request->has('from_date') ? new Carbon($request->get('from_date')) : null;
		$to_date = $request->has('to_date') ? new Carbon($request->get('to_date')) : null;
		$original_names = OrderForm::getOriginalNames();
		switch($request->get('form_type')) {
			case "0":
		  	$data = OrderForm::fillableColumns()->whereBetween('created_at', [$from_date, $to_date->addDay()])->get();
		  	$original_names = OrderForm::getOriginalNames();
				$file_name = "orders" . date('Y_m_d-H_i_s');
		  	break;
			case "1":
				$data = ContactForm::fillableColumns()->whereBetween('created_at', [$from_date, $to_date->addDay()])->get();
				$original_names = ContactForm::getOriginalNames();
				$file_name = "contacts" . date('Y_m_d-H_i_s');
			break;
			case '3':
				$data = Consumer::all()->toArray();
				$tmp = [];
				foreach ($data as $row) {
					unset($row['id']);
					unset($row['created_at']);
					unset($row['updated_at']);
					array_push($tmp, $row);
				}
				$data = $tmp;
				$original_names = array_map("strtoupper", Consumer::getOriginalNames());
				$file_name = "Consumer" . date('Ymd');
				\Config::set('excel.csv.delimiter', ',');
				Excel::setDelimiter(',');
				return Excel::create($file_name, function($excel) use ($data, $original_names, $file_name) {
					$excel->sheet('consumers', function($sheet) use ($data, $original_names) {
						$sheet->appendRow($original_names);
						$sheet->rows($data);
					});

				})->store('csv')->download('csv');
			break;
			case '4':
				$data = Subscription::all()->toArray();
				$tmp = [];
				foreach ($data as $row) {
					$row['ACCEPTANCE_DATE'] = ($row['subscribed'] == 0) ? $row['created_at'] : $row['updated_at'];
					$row['CONSUMER_ID'] = Consumer::whereEmailValue($row['email'])->first()->consumer_id;
					$row['ACCEPTANCE_APP_CODE'] = 'MXNINWEB';
					$row['ACCEPTANCE_DETAILS'] = 'Added manually';
					$row['REVOCATION_APP_CODE'] = 'Added ';
					$row['REVOCATION_DATE'] = ($row['subscribed'] == 0) ? $row['updated_at'] : '';
					$row['REVOCATION_DETAILS'] = '';
					$row['SFMC_Entry_Date'] = $row['created_at'];
					$row['SFMC_Last_Modified_Date'] = $row['updated_at'];
					$row['MARKETING_GROUP_SERVICE_CODE'] = 'BGnestleprf-Oonescafea';
					unset($row['id']);
					unset($row['email']);
					unset($row['source']);
					unset($row['created_at']);
					unset($row['updated_at']);
					unset($row['subscribed']);
					array_push($tmp, $row);
				}
				$data = $tmp;
				$original_names = ['ACCEPTANCE_DATE', 'CONSUMER_ID', 'ACCEPTANCE_APP_CODE', 'ACCEPTANCE_DETAILS',
								   'REVOCATION_APP_CODE', 'REVOCATION_DATE', 'REVOCATION_DETAILS', 'SFMC_Entry_Date',
								   'SFMC_Last_Modified_Date', 'MARKETING_GROUP_SERVICE_CODE'];

				$file_name = "OptIn" . date('Ymd');
				\Config::set('excel.csv.delimiter', ',');
				Excel::setDelimiter(',');
				return Excel::create($file_name, function($excel) use ($data, $original_names, $file_name) {
					$excel->sheet('optin', function($sheet) use ($data, $original_names) {
						$sheet->appendRow($original_names);
						$sheet->rows($data);
					});

				})->store('csv')->download('csv');
			break;
		}
		//dd($original_names);
		return  Excel::create($file_name, function($excel) use ($data, $original_names, $file_name) {
								$excel->sheet($file_name, function($sheet) use ($data, $original_names) {
								$sheet->setAutoFilter('A1:S256000');
								$sheet->setAutoSize(true);
								$sheet->appendRow($original_names);
								$sheet->rows($data->toArray());
								$sheet->freezeFirstRow();
								$sheet->row(1, function($row) use ($sheet) {
									$row->setBackground('#BFD2E2');
									$sheet->cell($row->cells, function($cells) {
										$cells->setAlignment('center');
										$cells->setBorder('solid', null, null, 'solid');
									});
								});
							});
						})->store('xls')->download('xls');
						redirect()->back();
	}


}
