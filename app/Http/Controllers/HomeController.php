<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Notice;
use NescafeAlegria\Slide;
use DB;

class HomeController extends Controller {
	/**
	 * Show the application homepage
	 *
	 * @return Response
	 */
	public function index()
	{
		$slides = DB::table('slides')->orderBy('position', 'asc')->get();
    $notice = DB::table('notices')->where('approved', '=', 1)->orderBy('id', 'desc')->first();
    $meta = [ 'title' => 'Професионални кафемашини и кафе автомати.',
              'description' => 'Разнообразие от предложения за кафе напитки и професионални кафе машини за бизнеса.',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/coffee-machine.png' ,
              'keywords' => 'кафемашина, кафе машина, професионална кафе машина'

            ];
    return view('home.index', compact('notice', 'slides', 'meta'));
	}

  public function promotions()
  {
    $promotion = DB::table('notices')->where('approved', '=', 1)->orderBy('id', 'desc')->first();
    return redirect("promotions/$promotion->slug");
  }

  public function promotion($slug)
  {
    $promotion = Notice::findBySlug($slug);

    $meta = [ 'title' => $promotion->title,
              'description' => strip_tags(html_entity_decode($promotion->meta_description)),
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/attachments/notices/'.$promotion->image ,
              'keywords' => $promotion->keywords
            ];
    return view('home.promotion', compact('promotion', 'meta'));
  }
}
