<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Controller;

use Illuminate\Http\Request;

class BenefitsController extends Controller {

	public function index()
	{
    $meta = [ 'title' => 'ПРЕДИМСТВА',
              'description' => 'Без високи разходи. Инсталация, поддръжка и сервиз. Обучение. Рецепти. Контрол на качеството. Аксесоари и комуникационна подкрепа.',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'разходи, инсталация, сервиз, рецепти, качеството, аксесоари'
            ];
		return view('benefits.index', compact('meta'));
	}
}
