<?php namespace NescafeAlegria\Http\Controllers;
use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Controller;
use NescafeAlegria\OrderForm;
use NescafeAlegria\Machine;
use NescafeAlegria\Subscription;

use Illuminate\Http\Request;

class OrderController extends Controller {
	/**
	 * Shows the contact form
	 * @return Response
	 */
	public function index() {
		$meta = [ 'title' => 'Заяви кафемашина',
							'description' => 'Попълнете формата и ще се свържем с Вас.',
							'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
							'keywords' => 'заявка, кафемашина, решение, бизнес' ];
		return view('order.index', compact('meta'));
	}


	public function thankyou() {
		$meta = [ 'title' => 'Благодарим Ви за изпратената заявка!',
							'description' => 'Благодарим Ви за изпратената заявка! Наш служител ще се свърже с Вас в рамките на 24 часа (в работни дни), за да уточните удобно време за посещение от наш представител.',
							'image' => 'http://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
							'keywords' => 'заявка, кафемашина, решение, бизнес' ];
		return view('order.thankyou', compact('meta'));
	}

	public function store(Request $request)
	{
		$today = date("Ymd");
		$rand = strtoupper(substr(uniqid(sha1(time())),0,4));
		$order_id = $today . $rand;
		$machine_name = $request->has('machine_id') ? Machine::find($request->get('machine_id'))->name : null;
		//dd($request->only(['first_name','last_name','phone_number','email','organization_name', 'city', 'beverages', 'employees', 'message', 'recipe_name', 'business_type']));
		$order = OrderForm::create($request->only(['first_name','last_name','phone_number','email','organization_name', 'city', 'beverages', 'employees', 'message', 'recipe_name', 'business_type']));
		$order->update(['order_id' => $order_id]);

		$data = $request->only(['first_name','last_name','phone_number','email','organization_name', 'city', 'beverages', 'employees', 'message', 'recipe_name', 'business_type']);

		$email = $request->get('email');
		$subscription = Subscription::where('email', '=', $email)->first();
		if( $request['accept_subscription'] == true) {
			if ($subscription===null) {
				$subscription = Subscription::create(['email' => $email, 'source' => 2]);
			}
			else {
				$subscription->toggle();
			}
		}
		else {
			if ($subscription!==null) {
				$subscription->unsubscribe();
			}
		}
		$arr = array(''=> '', 0 => '20 до 50', 1 => 'над 50');
		\Mail::send(['emails.order_client_html', 'emails.order_client_text'], ['machine_name' => $machine_name, 'order_id' => $order_id], function($message) use($email, $order_id, $machine_name){
			$message->to($email)->subject('Order '.$order_id);
		});
		\Mail::send(['emails.order_html', 'emails.order_text'], ['order_id' => $order_id,
																 'first_name' => $data['first_name'],
																 'last_name' => $data['last_name'],
																 'phone_number' => $data['phone_number'],
																 'email' => $data['email'],
																 'organization_name' => $data['organization_name'],
																 'city' => $data['city'],
																 'machine_name' =>  $machine_name,
																 'recipe_name' => $data['recipe_name'],
																 'business_type' => $data['business_type'],
																 'beverages' => $data['beverages'],
																 'employees' => $arr[$data['employees']],
																 'text' => $data['message']], function($message) use($order_id){
			$message->to('nestle.professional@bg.nestle.com')->subject('Order '.$order_id);
		});
		return redirect('order/thankyou');
		#return redirect()->back()->with(['status' => 'success', 'message' => "Благодарим Ви за изпратената заявка! В рамките на 24 часа наш служител ще се свърже с Вас."]);
	}
}
