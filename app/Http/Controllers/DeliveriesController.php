<?php namespace NescafeAlegria\Http\Controllers;

class DeliveriesController extends Controller {
	public function index() {
    $meta = [ 'title' => 'УСЛОВИЯ ЗА ДОСТАВКА',
              'description' => 'Създайте решението за Вашия бизнес',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'заявка, кафемашина, инсталиране, монтаж'
            ];
		return view('deliveries.index', compact('meta'));
	}

}
