<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Machine;
use NescafeAlegria\Recipe;
use NescafeAlegria\Product;
use DB;

class ConfigurationController extends Controller {
  /**
   * Shows a list of available products
   *
   * @return Response
   */
  public function step_1() {
    $meta = [ 'title' => 'Кафе решение за Вашия бизнес | Вид бизнес',
              'description' => 'Вид бизнес',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'заявка, кафемашина, решение, бизнес' ];
    return view('configuration.step_1', compact('meta'));
  }
  public function step_2() {
    $meta = [ 'title' => 'Кафе решение за Вашия бизнес | Брой посетители',
              'description' => 'Брой клиенти/служители',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'заявка, кафемашина, решение, клиенти, служители' ];
    return view('configuration.step_2', compact('meta'));
  }
  public function step_3() {
    $meta = [ 'title' => 'Кафе решение за Вашия бизнес | Брой кафе напитки',
              'description' => 'Брой консумирани чаши',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'заявка, кафемашина, решение, чаши' ];
    return view('configuration.step_3', compact('meta'));
  }
  public function result($id) {
    $menu = isset($_GET['menu']) ? $_GET['menu'] : '';
    $recipe = Recipe::where('name', '=', $menu)->orderBy('id', 'desc')->first();
    $machine = Machine::find($id);
    $meta = [ 'title' => 'РЕШЕНИЕ ЗА ВАС | '.$machine->name,
              'description' => 'Вашето решение',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'заявка, кафемашина, решение, чаши' ];
    return view('configuration.result', compact('machine', 'recipe', 'meta'));
  }
}