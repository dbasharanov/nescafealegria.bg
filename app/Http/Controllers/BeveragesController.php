<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Beverage;
use NescafeAlegria\Category;
use DB;

class BeveragesController extends Controller {
  /**+
   * Shows a list of available drinks
   *
   * @return Response
   */
  public function index() {
    $categories = Category::where('is_active', '=', 1)->orderBy('id', 'asc')->get();
    $meta = [ 'title' => 'МЕНЮ НАПИТКИ',
              'description' => 'Топли напитки, Напитки с въображение, Студени напитки',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'Топли напитки, Напитки с въображение, Студени напитки'
            ];
    return view('beverages.index', compact('categories', 'meta'));
  }

  public function show($slug) {
    $beverage = Beverage::findBySlug($slug);
    $prev = DB::table('beverages')->where('id', '<', $beverage->id)->orderBy('id', 'desc')->first();
    $next = DB::table('beverages')->where('id', '>', $beverage->id)->orderBy('id', 'asc')->first();

    $meta = [ 'title' => $beverage->name,
              'description' => strip_tags(html_entity_decode($beverage->meta_description)),
              'image' => 'http://'.$_SERVER['SERVER_NAME'].'/images/attachments/beverages/'.$beverage->image ,
              'keywords' => $beverage->keywords
            ];
    return view('beverages.show', compact('beverage', 'prev', 'next','meta'));
  }

  public function category($slug) {
    $category = Category::findBySlug($slug);
    return view('beverages.category', compact('category'));
  }
}
