<?php namespace NescafeAlegria\Http\Controllers;

use Illuminate\Http\Request;
use NescafeAlegria\Machine;
use NescafeAlegria\Product;
use NescafeAlegria\Business;
use NescafeAlegria\Beverage;


class SearchController extends Controller {

	public function index(Request $request)
	{
		$query = $request->has('query') ? $request->get('query') : NULL;
		$machines = Machine::whereRaw("MATCH(name, description) AGAINST(? )", array($query))->get();
		$products = Product::whereRaw("MATCH(name, description) AGAINST(? )", array($query))->get();
		$beverages = Beverage::whereRaw("MATCH(name, description) AGAINST(? )", array($query))->get();
		$businesses = Business::whereRaw("MATCH(name, description) AGAINST(? )", array($query))->get();
		$count = sizeof($machines) + sizeof($products) + sizeof($beverages) + sizeof($businesses);
		$results = array($machines, $products, $beverages, $businesses);
    $meta = [ 'title' => 'Резултати от търсене',
              'description' => 'Резултати от търсене',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'търсене, резултати, кафе, мляко, какао, 3 в 1'
             ];

		return view('search.index', compact('results', 'query', 'count', 'meta'));
	 }
}
