<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Admin\AdminController;

use Illuminate\Http\Request;

use NescafeAlegria\Visit;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use NescafeAlegria\User;
use Carbon\Carbon;

class VisitsController extends AdminController {
	public function index(Request $request)
	{
		Carbon::setToStringFormat('d.m.Y');
		$from_date = $request->has('from_date') ? new Carbon($request->get('from_date')) : Carbon::today()->subMonth();
		$to_date = $request->has('to_date') ? new Carbon($request->get('to_date')) : Carbon::today();
		$user_id = $request->has('user_id') ? (int)$request->get('user_id') : 0;
		$only_not_visited = $request->has('only_not_visited') ? (int)$request->get('only_not_visited') : 0;

		if(Auth::user()->isAdministrator() || Auth::user()->isSupervisor())
			$userFilters = [0 => 'Всички'] + Auth::user()->getSubjectsNames();
		else
			$userFilters = Auth::user()->getSubjectsNames();
		$visits = new Visit;
		$_vv = new Visit;
		if(Auth::user()->isSubject($user_id))
		{
			$visits = $visits->filterByUser($user_id ? User::findOrFail($user_id)->name : Auth::user()->getSubjectsNames());
			$_vv = $_vv->filterByUser($user_id ? User::findOrFail($user_id)->name : Auth::user()->getSubjectsNames());
		}
		else
		{
			abort(403, 'Unauthorized action.');
		}
		if($only_not_visited)
		{
			$visits = $visits->where('visited', '=', '0');
			$_vv = $_vv->where('visited', '=', '0');
		}

		$visits = $visits->whereBetween('calendar_date', [$from_date, $to_date]);
		$_vv = $_vv->whereBetween('calendar_date', [$from_date, $to_date]);
		$all = $_vv->count();
		$visits = $visits->paginate(100);

		if($user_id != 0)
			$visits = $visits->appends('user_id', $user_id);

		if($only_not_visited)
		{
			$not_visited = $_vv->get()->count();
			$visited = 0;
			$visits = $visits->appends('only_not_visited', $only_not_visited);
		}
		else
		{
			$visited = $_vv->where('visited', '=', 1)->get()->count();
			$not_visited = $all - $visited;
		}
		return view('admin.visits.index', compact('visits', 'from_date', 'to_date', 'userFilters', 'user_id', 'only_not_visited', 'not_visited', 'visited'));
	}
}
