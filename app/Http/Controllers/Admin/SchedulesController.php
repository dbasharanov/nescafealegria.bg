<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Controllers\Admin\AdminController;
use NescafeAlegria\Schedule;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class SchedulesController extends AdminController {

	public function index()
	{
		$userFilters = Auth::user()->getSubjectsNames();
		$schedules = Schedule::whereIn('calendar_date', [Carbon::today(), Carbon::tomorrow()])
			->whereIn('trading_person_name', array_values($userFilters))
			->orderBy('calendar_date', 'asc')->paginate(1000);
		return view('admin.schedules.index', compact('schedules'));
	}

	public function toggle($id)
	{
		$schedule = Schedule::owned()->findOrfail($id);
		$schedule->visited = !$schedule->visited;
		return "{$schedule->save()}";
	}

}
