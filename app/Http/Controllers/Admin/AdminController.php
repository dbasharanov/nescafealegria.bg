<?php namespace NescafeAlegria\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Cookie;
use Illuminate\Support\Facades\Auth;
use NescafeAlegria\Http\Controllers\Controller;
use Carbon\Carbon;

use Kodeine\Acl\Models\Eloquent\Role;

class AdminController extends Controller {
	public function __construct(Request $request)
	{
		Carbon::setToStringFormat('d.m.Y');
		$cookie = Cookie::queue('last_path', $request->path());
		Cookie::queue('role', Auth::user()->getRoles()[0]);
	}

	protected function getRoles()
	{
		$roles = Role::lists('name', 'id');
		foreach ($roles as $key => $value) {
			$roles[$key] = trans("misc.${value}");
		}
		return $roles;
	}
}
