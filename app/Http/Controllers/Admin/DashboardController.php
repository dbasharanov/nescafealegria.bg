<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Controllers\Admin\AdminController;
use Illuminate\Support\Facades\Auth;
use NescafeAlegria\Schedule;
use Carbon\Carbon;

class DashboardController extends AdminController {
	public function index()
	{
		$visits_today = Schedule::where('calendar_date', Carbon::today())
			->where('trading_person_name', Auth::user()->name)
			->count();
		if(Auth::user()->isAdministrator())
		{
			return redirect('admin_rlAPkBI5RRPqmty1pVu4/visits');
		}
		return redirect('admin_rlAPkBI5RRPqmty1pVu4/schedules');
	}
}
