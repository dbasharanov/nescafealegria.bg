<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Admin\AdminController;

use Illuminate\Http\Request;

use NescafeAlegria\User;
use NescafeAlegria\Schedule;
use NescafeAlegria\Cycle;
use NescafeAlegria\Http\Requests\CreateUserRequest;
use NescafeAlegria\Http\Requests\UpdateUserRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use DB;

use Kodeine\Acl\Models\Eloquent\Role;

class UsersController extends AdminController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$users = User::paginate(50);
		return view('admin.users.index', compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$user = new User;
		$roles = $this->getRoles();
		$operators = Cycle::where('operator_name', '<>', '')->groupBy('operator_name')->get();
		$possibleSupervisors = Role::where('slug', 'employee')->first()->users;
		$supervisors = User::all();
		return view('admin.users.create', compact('user', 'roles', 'possibleSupervisors', 'operators'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param CreateUserRequest $request
	 * @return Response
	 */
	public function store(CreateUserRequest $request)
	{
		$request->merge(['password' => Hash::make($request->password)]);
		$user = User::create($request->all());

		$user->assignRole($request->get('role'));

		if($user->is('third_party_employee')) {
			$user->operator = Request::get('operator');
			$user->save();
		}

		return redirect('admin_rlAPkBI5RRPqmty1pVu4/users')->with(['status' => 'success', 'message' => "Успешно създадохте потребител."]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$user = User::findOrFail($id);
		return view('admin.users.show', compact('user'));
	}

	public function change_password()
	{
		return view('admin.users.change_password');
	}

	public function update_password(Request $request)
	{
		$user = Auth::user();

		if(!Hash::check($request->get('current_password'), $user->password)) {
			return redirect()->back()->with(['status' => 'alert', 'message' => 'Текущата парола не е правилна']);
		}

		$this->validate($request, [
            'password' => 'required|confirmed|min:6',
        ]);
		$user->password = bcrypt($request->get('password'));
        $user->save();
		return redirect('admin_rlAPkBI5RRPqmty1pVu4')->with(['status' => 'success', 'message' => "Успешно си променихте паролата."]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = User::findOrFail($id);
		$roles = $this->getRoles();
		$operators = Cycle::where('operator_name', '<>', '')->groupBy('operator_name')->get();

		$possibleSupervisors = Role::where('slug', 'employee')->first()
			->users()->where('users.id', '<>', $user->id)->get();
		return view('admin.users.edit', compact('operators', 'user', 'roles', 'possibleSupervisors'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, UpdateUserRequest $request)
	{
		$user = User::findOrFail($id);
		$data = $request->all();
		if($data['password'] != '')
			$data['password'] = $user->password = bcrypt($data['password']);

		DB::transaction(function() use ($user, $data)
		{
			$user->update($data);
			$user->revokeAllRoles();
			$user->assignRole($data['role']);

		});
		if($user->is('third_party_employee')) {
			dd('here');
			$user->operator = $request->operator;
			$user->save();
		}
		return redirect('admin_rlAPkBI5RRPqmty1pVu4/users')->with(['status' => 'success', 'message' => "Успешно променихте потребител"]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$user = User::findOrFail($id);
		$user->delete();
		return redirect('admin_rlAPkBI5RRPqmty1pVu4/users');
	}

}
