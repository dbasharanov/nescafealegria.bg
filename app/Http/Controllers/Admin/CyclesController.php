<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Admin\AdminController;
use NescafeAlegria\Http\Requests\ImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Exception;
use NescafeAlegria\Cycle;
use NescafeAlegria\User;
use NescafeAlegria\Http\Requests\AdminEditCycleRequest;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class CyclesController extends AdminController {
	public function index(Request $request)
	{
		Carbon::setToStringFormat('d.m.Y');
		if(Auth::user()->isAdministrator() || Auth::user()->isSupervisor())
			$userFilters = [0 => 'Всички'] + Auth::user()->getSubjectsNames();
		else
			$userFilters = Auth::user()->getSubjectsNames();
		$from_date = $request->has('from_date') ? new Carbon($request->get('from_date')) : Carbon::today();
		$to_date = $request->has('to_date') ? new Carbon($request->get('to_date')) : Carbon::today()->addDays(7);
		$show_inactive = $request->has('show_inactive') ? true : false;
		$user_id = $request->has('user_id') ? (int)$request->get('user_id') : 0;
		$operator_name = $request->has('operator_name') ? $request->get('operator_name') : NULL;
		$bmb_number = $request->has('bmb_number') ? $request->get('bmb_number') : NULL;
		$poc_name = $request->has('poc_name') ? $request->get('poc_name') : NULL;
		$poc_responsible_name = $request->has('poc_responsible_name') ? $request->get('poc_responsible_name') : NULL;
		// $operator_name = $request->has('operator_name') ? $request->get('operator_name') : NULL;
		$operators = Auth::user()->getOperatorsNames();
		$additional_filters = [];
		$cycles = [];
		$summary = [];
		$tmp_start = new Carbon($from_date);
		$period_in_days = $from_date->diffInDays($to_date);
		$days_to_check[0] = $from_date;

		$where_query = new Cycle;
		if(Auth::user()->isSubject($user_id))
		{
			if(!$show_inactive)
				$where_query = $where_query->filterByUser($user_id ? User::findOrFail($user_id)->name : Auth::user()->getSubjectsNames());
		}
		else
		{
			abort(403, 'Unauthorized action.');
		}

		if($bmb_number)
		{
			$additional_filters['bmb_number'] = $bmb_number;
			if(strlen($bmb_number) > 5)
				$where_query = $where_query->where('machine_sn', $bmb_number);
			else
				$where_query = $where_query->where('bmb_number', $bmb_number);
		}

		if($poc_name)
		{
			$additional_filters['poc_name'] = $poc_name;
			$where_query = $where_query->where('poc_name', 'LIKE', "%{$poc_name}%");
		}

		if($poc_responsible_name)
		{
			$additional_filters['poc_responsible_name'] = $poc_responsible_name;
			$where_query = $where_query->where('poc_responsible_name', 'LIKE', "%{$poc_responsible_name}%");
		}

		if($operator_name && $operator_name != 'Всички')
		{
			$additional_filters['operator_name'] = $operator_name;
			$where_query = $where_query->where('operator_name', '=', "{$operator_name}");
		}

		for($i = 1; $i <= $period_in_days; $i++) {
			$date = new Carbon($tmp_start->addDays(1));
			array_push($days_to_check, $date);
		}

		if(!$show_inactive)
		{
			$where_query = $where_query->where('period', '>', 0);
			$candidates = $where_query->get();
			foreach ($days_to_check as $date_of_interest)
			{
				$summary[$date_of_interest->format('d.m.Y')] = 0;
				foreach ($candidates as $cycle)
				{
					$row = $cycle->checkDateForVisit($date_of_interest);
					if($row)
					{
						$summary[$date_of_interest->format('d.m.Y')]++;
						array_push($cycles, $row);
					}
				}
			}
		}
		else
		{
			if(Auth::user()->isThirdPartyEmployee())
				$where_query = $where_query->where('period', '=', 0)->where('bmb_number', '<>', 0)->where('operator_name', '=', Auth::user()->operator);
			else
				$where_query = $where_query->where('period', '=', 0)->where('bmb_number', '<>', 0);
			$cycles = $where_query->get();
		}

		return view('admin.cycles.index', compact('cycles', 'from_date', 'to_date', 'show_inactive', 'summary', 'userFilters', 'user_id', 'additional_filters', 'operators'));
	}

	public function import(ImportRequest $request)
	{
		$file = $request->file('file')->move(storage_path() . '/imports', 'cikli_na_poseshtenie_' . date('Y_m_d-H_i_s-U') . '.' . $request->file('file')->getClientOriginalExtension());
		$excel = new Excel;
		if($request->get('truncate')) {
			Cycle::truncate();
		}
		try
		{
			Excel::load($file->getPathName(), function($reader) {
				$reader->each(function($data) {
					if($data['ime_na_operator'] == null) {
						return;
					}
					$cycle = Cycle::createFromExcel($data);
				});
			});
		}
		catch(\Exception $e)
		{
			dd($e->getMessage());
		}
		return redirect('admin_rlAPkBI5RRPqmty1pVu4/cycles')->with(['status' => 'success', 'message' => "Успешно импортирахте циклите на посещение."]);
	}

	public function edit($id)
	{
		$cycle = Cycle::findOrFail($id);
		$userFilters = Auth::user()->getSubjectsNames();
		return view('admin.cycles.edit', compact('cycle', 'userFilters'));
	}

	public function create()
	{
		if(Auth::user()->isAdministrator() || Auth::user()->isSupervisor())
			$userFilters = [0 => 'Всички'] + Auth::user()->getSubjectsNames();
		else
			$userFilters = Auth::user()->getSubjectsNames();
		$cycle = new Cycle;
		$start_date = Carbon::today();
		return view('admin.cycles.create', compact('cycle', 'start_date', 'userFilters'));
	}

	public function update($id, AdminEditCycleRequest $request)
	{
		$cycle = Cycle::findOrFail($id);
		if($cycle->update($request->all()))
		{
			return redirect('admin_rlAPkBI5RRPqmty1pVu4/cycles')->with(['status' => 'success', 'message' => 'Цикълът на посещение е обновен успешно.']);
		}
	}

	public function disable($id)
	{
		$cycle = Cycle::findOrFail($id);
		$cycle->trading_person_name = '';
		$cycle->period = 0;
		if($cycle->save())
		{
			return redirect('admin_rlAPkBI5RRPqmty1pVu4/cycles')->with(['status' => 'success', 'message' => 'Цикълът на посещение е обновен успешно.']);
		}
	}

	public function change_cycle($id)
	{
		$cycle = Cycle::findOrFail($id);
		return view('admin.cycles.change_cycle', compact('cycle'));
	}

	public function update_cycle($id, EmployeeEditCycleRequest $request)
	{
		$cycle = Cycle::findOrFail($id);
		if($cycle->update($request->all()))
		{
			return redirect('admin_rlAPkBI5RRPqmty1pVu4/cycles')->with(['status' => 'success', 'message' => 'Цикълът на посещение е обновен успешно.']);
		}
	}

	public function export()
	{
		$data = Cycle::fillableColumns()->get();
		$data_array = $data->toArray();
		$data_array = array_map(array($this, 'format_date'), $data_array);

		return Excel::create('cikli_na_poseshtenie_' . date('Y_m_d-H_i_s-u'), function($excel) use ($data_array) {
			$excel->sheet('cikli_na_poseshtenie', function($sheet) use ($data_array) {
				$sheet->appendRow(Cycle::getOriginalNames());
				$sheet->rows($data_array);
				$sheet->freezeFirstRow();
			});
		})->store('csv')->download('csv');
	}

	public function destroy($id)
	{
		$cycle = Cycle::destroy($id);

		return redirect('admin_rlAPkBI5RRPqmty1pVu4/cycles')->with(['status' => 'success', 'message' => 'Цикълът на посещение е изтрит успешно.']);
	}

	public function store(Request $request)
	{
		$cycle = Cycle::create($request->all());
		$cycle->trading_person_name = Auth::user()->name;
		$cycle->machine_model = 'Gourmet';
		$cycle->save();
		return redirect('admin_rlAPkBI5RRPqmty1pVu4/cycles')->with(['status' => 'success', 'message' => 'Цикълът на посещение е обновен успешно.']);
	}

	private function searchForId($id, $array) {
		foreach ($array as $key => $val) {
			if ($val['bmb_number'] == $id) {
				return $key;
			}
		}
		return null;
	}

	private static function format_date($date) {
		$date['start_date'] = Carbon::parse($date['start_date'])->format('m/d/Y');
		return $date;
	}
}
