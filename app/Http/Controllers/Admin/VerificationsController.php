<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Admin\AdminController;

use Illuminate\Http\Request;

use NescafeAlegria\Verification;
use NescafeAlegria\Http\Requests\ImportRequest;
use Maatwebsite\Excel\Facades\Excel;

class VerificationsController extends AdminController {

	public function import(ImportRequest $request)
	{
		$file = $request->file('file');

		$excel = new Excel;

		Excel::load($file->getPathName(), function($reader)
		{
			$i = 0;
			$reader->each(function($data)
			{
				if($data['vd_calyearmonthyyyymm'] == null) {
					return;
				}
				$verification = Verification::createFromExcel($data);
			});
		});

		return redirect('admin_rlAPkBI5RRPqmty1pVu4/visits')->with(['status' => 'success', 'message' => "Успешно импортирахте история на посещенията."]);
	}
}
