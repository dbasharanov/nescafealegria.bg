<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Admin\AdminController;
use NescafeAlegria\Http\Requests\ImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use NescafeAlegria\Order;
use Carbon\Carbon;
use File;
use Exception;
use Storage;
use Illuminate\Support\Facades\Auth;

class OrdersController extends AdminController {
	function unique_multidim_array($array, $key) {
	    $temp_array = array();
	    $i = 0;
	    $key_array = array();

	    foreach($array as $val) {
	        if (!in_array($val[$key], $key_array)) {
	            $key_array[$i] = $val[$key];
	            $temp_array[$i] = $val;
	        }
	        $i++;
	    }
	    return $temp_array;
	}

	public function index(Request $request)
	{
		$from_date = $request->has('from_date') ? new Carbon($request->get('from_date')) : null;
		$to_date = $request->has('to_date') ? new Carbon($request->get('to_date')) : null;
		$poc_name = $request->has('poc_name') ? $request->get('poc_name') : NULL;
		$filters = array_filter($request->only(['bmb_number', 'machine_sn']));
		$clients = Order::whereBetween('calendar_date', [$from_date, $to_date])->where($filters);
		if(Auth::user()->isThirdPartyEmployee())
		{
			$clients = $clients->where('operator_name', '=', Auth::user()->operator);
		}

		if($poc_name)
		{
			$clients = $clients->where('poc_name', 'LIKE', "%{$poc_name}%");
		}
		# $clients = $clients->groupBy('poc_name')->get();
		$clients = $clients->get();
		#$userFilters = Auth::user()->getSubjectsNames();
		$statistics = array();
		if($from_date == null || $to_date == null)
			return view('admin.orders.index', compact('statistics'));
		foreach($clients as $client) {
			if(!in_array($client->poc_name, $statistics))
				$statistics[$client->poc_name] = array();
		}
		foreach($clients as $client) {
			if(!in_array($client->bmb_number, $statistics[$client->poc_name]))
				$statistics[$client->poc_name][$client->bmb_number] = array();
		}

		foreach($clients as $client) {
			if(!in_array($client->machine_config_code, $statistics[$client->poc_name][$client->bmb_number]))
				$statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code] = array('doi' => array(), 'info' => $client->toArray(), 'recipes' => array());
		}
		foreach ($clients as $client) {
			if(!in_array($client['recipe_name'], $statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['recipes'], true))
				$statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['recipes'][$client['recipe_name']] = $client->cups_served;
		}
		foreach($clients as $client) {
			if(!in_array($client->calendar_date, $statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['doi']))
				$statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['doi'][$client->calendar_date] = array();
		}
		foreach($clients as $client) {
			if(!in_array($client->calendar_date, $statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['doi']))
				#array_push($statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['doi'][$client->calendar_date], $client->toArray());
				$statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['doi'][$client->calendar_date] = array();
		}
		foreach($clients as $client) {
			if(!in_array($client->recipe_name, $statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['doi'][$client->calendar_date],true))
				$statistics[$client->poc_name][$client->bmb_number][$client->machine_config_code]['doi'][$client->calendar_date][$client->recipe_name] = $client->cups_per_day;
		}

#dd($statistics);

	/*foreach($clients as $client) {
			#$statistics[$client->bmb_number] = $client->toArray();
			$poc_names = Order::whereBetween('calendar_date', [$from_date, $to_date])->where('bmb_number', $client->bmb_number)->groupBy('poc_name')->get();
			foreach ($poc_names as $poc_name) {
				$recipes = Order::whereBetween('calendar_date', [$from_date, $to_date])
								->where('bmb_number', $client->bmb_number)
								->where('poc_name', $poc_name['poc_name'])
								->groupBy('recipe_name')
								->get();

				$dates = Order::whereBetween('calendar_date', [$from_date, $to_date])
								->where('bmb_number', $client->bmb_number)
								->where('poc_name', $poc_name['poc_name'])
								#->where('recipe_name', $recipe['recipe_name'])
								->groupBy('calendar_date')
								->lists('calendar_date');
				$poc_dates = array();
				foreach($recipes as $recipe) {
					#$recipe['dates'] = array();
					$tmp_dates = array();
					foreach($dates as $date) {
						$stats = Order::where('bmb_number', $client->bmb_number)
										->where('recipe_name', $recipe['recipe_name'])
										->where('poc_name', $poc_name['poc_name'])
										->where('calendar_date', new Carbon($date))
										->sum('cups_per_day');
						#$recipe['dates'][$date] = $stats ? $stats : 0;
						array_push($tmp_dates, array($date => $stats ? $stats : 0));
						array_push($poc_dates, $date );
					}
					$recipe['doi'] = $tmp_dates;
				}
				$poc_name['doi'] = array_unique($poc_dates);
				$poc_name['recipes'] = $recipes;
				$poc_name['sum'] = Order::selectRaw('sum(cups_per_day) as sum')->whereBetween('calendar_date', [$from_date, $to_date])
										->where('bmb_number', $client->bmb_number)
										->where('poc_name', $poc_name['poc_name'])
										->groupBy('calendar_date')
										->lists('sum');
			}
			$statistics[$client->bmb_number]['poc_names'] = $poc_names->toArray();
		}*/
		return view('admin.orders.index', compact('statistics'));
	}

	public function import(ImportRequest $request)
	{
		$file = $request->file('file')->move(storage_path() . '/imports', 'orders_' . date('Y_m_d-H_i_s-U') . '.' . $request->file('file')->getClientOriginalExtension());
		$excel = new Excel;
		if($request->get('truncate')) {
			Order::truncate();
		}
		Excel::load($file->getPathName(), function($reader) {
			$reader->each(function($data) {
				if($data['poc_responsible_name'] == null) {
					return;
				}
				try
				{
					Order::createFromExcel($data);
				}
				catch(\Exception $e)
				{
					echo 'rescue from duplicate';
				}
			});
		})->store('csv');

		return redirect('admin_rlAPkBI5RRPqmty1pVu4/orders')->with(['status' => 'success', 'message' => "Успешно импортирахте поръчките."]);
	}

	public function export()
	{
		$data = [];
		$excel_file =  Excel::create('orders.temp', function($excel) use ($data) {
			$excel->sheet('orders', function($sheet) use ($data) {

				$sheet->appendRow(Order::getOriginalNames());

			});
		})->store('csv');

		Order::fillableColumns()->chunk(200, function ($data) {
			foreach ($data as $row) {
				$file = fopen(storage_path('exports/orders.temp.csv'), 'a');
				$row_array = $row->toArray();
				$row_array = $this->format_date($row_array);

				fputcsv($file, $row_array, '|', chr(0));
				fclose($file);
			}
		});
		$file_name = 'orders_' . date('Y_m_d-H_i_s-U') . '.csv';
		rename(storage_path('exports/orders.temp.csv'), storage_path('exports/' . $file_name));
		return response()->download(storage_path('exports/' . $file_name), $file_name);
	}

	private static function format_date($date) {
		$date['calendar_date'] = Carbon::parse($date['calendar_date'])->format('m/d/Y');
		return $date;
	}
}
