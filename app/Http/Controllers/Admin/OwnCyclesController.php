<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Admin\AdminController;
use NescafeAlegria\Http\Requests\ImportRequest;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

use NescafeAlegria\Cycle;
use NescafeAlegria\Http\Requests\EmployeeEditCycleRequest;
use Illuminate\Support\Facades\Auth;

class OwnCyclesController extends AdminController {
	public function edit($id)
	{
		$cycle = Cycle::owned()->findOrFail($id);
		$userFilters = Auth::user()->getSubjectsNames();
		return view('admin.own_cycles.edit', compact('cycle', 'userFilters'));
	}

	public function update($id, EmployeeEditCycleRequest $request)
	{
		$cycle = Cycle::owned()->findOrFail($id);
		if($cycle->update($request->all()))
		{
			return redirect('admin_rlAPkBI5RRPqmty1pVu4/cycles')->with(['status' => 'success', 'message' => 'Цикълът на посещение е обновен успешно.']);
		}
	}
}
