<?php namespace NescafeAlegria\Http\Controllers\Admin;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Admin\AdminController;

use Illuminate\Http\Request;
use NescafeAlegria\Cycle;
use NescafeAlegria\User;
use Illuminate\Support\Facades\Auth;

class ObjectsController extends AdminController {

	public function index(Request $request)
	{
		if(Auth::user()->isAdministrator() || Auth::user()->isSupervisor())
			$userFilters = [0 => 'Всички'] + Auth::user()->getSubjectsNames();
		else
			$userFilters = Auth::user()->getSubjectsNames();
		$user_id = $request->has('user_id') ? (int)$request->get('user_id') : 0;
		$operator_name = $request->has('operator_name') ? $request->get('operator_name') : NULL;
		$bmb_number = $request->has('bmb_number') ? $request->get('bmb_number') : NULL;
		$poc_name = $request->has('poc_name') ? $request->get('poc_name') : NULL;
		$poc_responsible_name = $request->has('poc_responsible_name') ? $request->get('poc_responsible_name') : NULL;
		// $operator_name = $request->has('operator_name') ? $request->get('operator_name') : NULL;
		$operators = Auth::user()->getOperatorsNames();
		$additional_filters = [];
		#$objects = [];
		$where_query = new Cycle;
		if(Auth::user()->isSubject($user_id))
		{
			$where_query = $where_query->filterByUser($user_id ? User::findOrFail($user_id)->name : Auth::user()->getSubjectsNames());
		}
		else
		{
			abort(403, 'Unauthorized action.');
		}
		if($bmb_number)
		{
			$additional_filters['bmb_number'] = $bmb_number;
			if(strlen($bmb_number) > 5)
				$where_query = $where_query->where('machine_sn', $bmb_number);
			else
				$where_query = $where_query->where('bmb_number', $bmb_number);
		}

		if($poc_name)
		{
			$additional_filters['poc_name'] = $poc_name;
			$where_query = $where_query->where('poc_name', 'LIKE', "%{$poc_name}%");
		}

		if($poc_responsible_name)
		{
			$additional_filters['poc_responsible_name'] = $poc_responsible_name;
			$where_query = $where_query->where('poc_responsible_name', 'LIKE', "%{$poc_responsible_name}%");
		}

		if($operator_name && $operator_name != 'Всички')
		{
			$additional_filters['operator_name'] = $operator_name;
			$where_query = $where_query->where('operator_name', '=', "{$operator_name}");
		}
		$cycles = $where_query->get();
		return view('admin.objects.index', compact('cycles', 'userFilters', 'user_id', 'additional_filters', 'operators'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
