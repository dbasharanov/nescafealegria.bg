<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Business;
use NescafeAlegria\Testimonial;
use DB;

class BusinessesController extends Controller {
  /**
   * Shows a list of available products
   *
   * @return Response
   */
  public function show($slug) {
    $business = Business::findBySlug($slug);
    $testimonials = $business->testimonials;

    $meta = [ 'title' => $business->name,
              'description' => strip_tags(html_entity_decode($business->meta_description)),
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/attachments/businesses/'.$business->image ,
              'keywords' => $business->keywords
            ];
    return view('businesses.show', compact('business', 'testimonials','meta'));
  }
/*  public function petrolstation() {
    $name = 'Бензиностанции';
    $slug = 'petrol';
    $slogan = ' ';
    $pic_url = '/images/business/petrolstation.jpg';
    $list = '<li>Елегантен дизайн - лице на Вашия бизнес с кафе и топли напитки</li><li>Богато меню, вкючително Schwarz кафе и кафе кана</li><li>Кафемашина - лесна за зареждане почистване и работа</li><li>100% одит контрол</li>';
    $testimonials = DB::table('testimonials')->get();
    return view('BusinessesController.show', compact('name', 'slug', 'slogan', 'pic_url', 'list', 'testimonials'));
  }
  public function hotel() {
    $name = 'Хотели';
    $slug = 'hotel';
    $slogan = 'NESCAFE Alegria Ви дава решение за кафе консумация във всяка тояка на Вашия хотел - от лоби бара и блок масата за закуска до конферентните зали и ВИП апартаментите.';
    $pic_url = '/images/business/hotel.jpg';
    $list = '<li>Бърза и ефективна работа</li><li>Богато меню, вкючително Schwarz кафе и кафе кана</li><li>Кафемашина - лесна за зареждане почистване и работа</li><li>100% одит контрол</li>';
    $testimonials = DB::table('testimonials')->get();
    return view('BusinessesController.show', compact('name', 'slug', 'slogan', 'pic_url', 'list', 'testimonials'));
  }
  public function coffeeshop() {
    $name = 'Кафетерии';
    $slug = 'coffeeshop';
    $slogan = 'Богатото меню ще изненада дори баристи:';
    $pic_url = '/images/business/coffeeshop.jpg';
    $list = '<li>Гъвкаво предложение спрямо нуждите и очакванията на Вашите клиенти</li><li>Осигурени аксесоари от висококачествем порцелан и стъкло, отговарящи на стандартите ни за качество и сигурност.</li><li>Лесна за зареждане, почистване и работа кафемашина</li><li>Допълнително меню от Бариста специалисти - ограничени сме само от Вашето въображение.</li>';
    $testimonials = DB::table('testimonials')->get();
    return view('BusinessesController.show', compact('name', 'slug', 'slogan', 'pic_url', 'list', 'testimonials'));
  }
  public function office() {
    $name = 'Офиси';
    $slug = 'office';
    $slogan = 'Богатото меню ще изненада дори баристи:';
    $pic_url = '/images/business/office.jpg';
    $list = '<li>Разнообразно меню за нуждите на Вашите служители</li><li>Безплатна доставка и осигурено обслужване</li><li>Лесна за работа, бърза и компактна кафемашина</li>';
    $testimonials = DB::table('testimonials')->get();
    return view('BusinessesController.show', compact('name', 'slug', 'slogan', 'pic_url', 'list', 'testimonials'));
  }
  public function shop() {
    $name = 'Магазини';
    $slug = 'shop';
    $slogan = ' ';
    $pic_url = '/images/business/shop.jpg';
    $list = '<li>Гъвкаво предложение спрямо нуждите и очакванията на Вашите клиенти</li><li>Възможност за обособяване на кафе кът, който да ви превърне в кафе дестинация.</li><li>Лесна за работа, бърза и компактна кафемашина</li><li>Осигурени аксесоари от материали, отговарящи на стандартите ни за качество и сигурност.</li>';
    $testimonials = DB::table('testimonials')->get();
    return view('BusinessesController.show', compact('name', 'slug', 'slogan', 'pic_url', 'list', 'testimonials'));
  }*/

}
