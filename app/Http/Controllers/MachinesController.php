<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Machine;
use DB;


class MachinesController extends Controller {
  /**
   * Shows a list of available machines
   *
   * @return Response
   */

  public function index() {
    $machines = DB::table('machines')->orderBy('position', 'asc')->get();
    $meta = [ 'title' => 'Професионални кафемашини',
              'description' => 'Професионални кафемашини NESCAFE Alegria за офис, магазин, хотел, бензиностанция и кафетерия.',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/attachments/machines/'.$machines[0]->image ,
              'keywords' => 'Кафемашини, NESCAFÉ® Alegria 8/120, NESCAFÉ® Alegria 10/60, NESCAFÉ® Alegria 8/60, NESCAFÉ® Alegria 6/30'];
    return view('machines.index', compact('machines', 'meta'));
  }

  public function show($slug) {
    $machine = Machine::findBySlug($slug);;
    $prev = DB::table('machines')->where('id', '<', $machine->id)->orderBy('id', 'desc')->first();
    $next = DB::table('machines')->where('id', '>', $machine->id)->orderBy('id', 'asc')->first();

    $meta = [ 'title' => $machine->name,
              'description' => strip_tags(html_entity_decode($machine->meta_description)),
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/attachments/machines/'.$machine->image ,
              'keywords' => $machine->keywords];

    return view('machines.show', compact('machine', 'prev', 'next', 'meta'));
  }
}
