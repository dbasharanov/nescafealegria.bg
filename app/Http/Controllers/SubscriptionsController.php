<?php namespace NescafeAlegria\Http\Controllers;

use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Controller;
use NescafeAlegria\Subscription;
use NescafeAlegria\Consumer;

use Illuminate\Http\Request;

class SubscriptionsController extends Controller {

	public function thankyou() {
		$meta = [ 'title' => 'Благодарим Ви, че се абонирахте за нашите новини и предложения',
							'description' => 'Благодарим Ви, че се абонирахте за нашите новини и предложения!',
							'image' => 'http://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
							'keywords' => 'заявка, кафемашина, решение, бизнес' ];
		return view('subscriptions.thankyou', compact('meta'));
	}

	public function store(Request $request)
	{
		$subscription = Subscription::where('email', '=', $request->get('email'))->first();
		if($subscription===null) {
			$subscription = Subscription::create($request->all());
		}
		elseif ($subscription->subscribed == 0) {
			$subscription->subscribe();
		}
		return redirect('subscriptions/thankyou');
		//return redirect()->back()->with(['status' => 'success', 'message' => "Благодарим Ви, че се абонирахте за нашите новини и предложения."]);
	}

	public function unsubscribe(Request $request, $consumer_id = false)
	{
		if($consumer_id) {
			$consumer = Consumer::where('consumer_id', '=', $consumer_id)->first();
			$subscription = Subscription::where('email', '=', $consumer->email_value)->first();
			$subscription->unsubscribe();
			return view('subscriptions.unsubscribe');
		}
		elseif($request->get('email')) {
			$subscription = Subscription::where('email', '=', $request->get('email'))->first();
			if($subscription) {
				$subscription->unsubscribe();
			}
			return view('subscriptions.unsubscribe');
		}
		return view('subscriptions.index');
	}

}
