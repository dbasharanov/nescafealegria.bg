<?php namespace NescafeAlegria\Http\Controllers;
use NescafeAlegria\Http\Requests;
use NescafeAlegria\Http\Controllers\Controller;
use NescafeAlegria\ContactForm;
use NescafeAlegria\Subscription;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ContactsController extends Controller {
	/**
	 * Shows the contact form
	 * @return Response
	 */
	public function index() {
		$meta = [ 'title' => 'Контакти',
				'description' => 'Можете да се свържете с нас на имейл адрес: Nestle.Professional@bg.nestle.com или да ни се обадите на телефон: 0 700 16 606',
				'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
				'keywords' => 'Контакти,имейл адрес, телефон '
				];
		return view('contacts.index', compact('meta'));
	}

	public function store(Request $request)
	{

		$validator = Validator::make($request->all(), ['file' => 'max:2048',]);
		if ($validator->fails()) {
			 return redirect()->back()->withErrors($validator)->withInput();
		}
		$contact = ContactForm::create($request->only(['first_name','last_name','phone_number','email','question_type', 'city', 'file', 'message']));
		$data = $request->only(['first_name','last_name','phone_number','email','question_type', 'city', 'file', 'message']);
		$subscription = Subscription::where('email', '=',($request->only(['email'])))->first();
		if( $request['accept_subscription'] == true) {
			if ($subscription===null) {
				$subscriber = $request->only(['email']);
				$subscriber['source'] = 1;
				$subscription = Subscription::create($subscriber);
			}
			else {
				$subscription->subscribe();
			}
		}
		else {
			if ($subscription!==null) {
				$subscription->unsubscribe();
			}
		}
		$arr = array(0 => 'Запитване за продукт', 1 => 'Запитване за продукт', 2 => 'Запитване за машина', 3 => 'Контакт с търговски представител', 4 => 'Друго');
		\Mail::send(['emails.contact_form_html', 'emails.order_text'], ['first_name' => $data['first_name'],
					 'last_name' => $data['last_name'],
					 'phone_number' => $data['phone_number'],
					 'email' => $data['email'],
					 'question_type' => $arr[$data['question_type']],
					 'city' => $data['city'],
					 'file' => $data['file'],
					 'text' => $data['message']], function($message) {
			$message->to('nestle.professional@bg.nestle.com')->subject('Контактна форма');
		});

		return redirect()->back()->with(['status' => 'success', 'message' => "Благодарим Ви! Вашето запитване е изпратено успешно. Ще Ви отговорим при първа възможност. Междувременно се надяваме, че с удоволствие ще разгледате нашия уебсайт."]);
	}
}
