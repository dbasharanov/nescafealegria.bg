<?php namespace NescafeAlegria\Http\Controllers;

class InformationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function tos()
  {
    $meta = [ 'title' => 'Правила и условия',
              'description' => 'Политика за ползване на Уеб Сайт',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'Допустима употреба, Защита на данните, Интелектуална собственост, Отговорност'
        ];
    return view('information.tos', compact('meta'));
  }

  public function cookies()
  {
    $meta = [ 'title' => 'Политика за използване на бисквитки',
              'description' => 'Политика относно "бисквитките" (cookies) на "Нестле Бъглария" АД',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'бисквитките, cookies,НЕСТЛЕ БЪЛГАРИЯ'
    ];
    return view('information.cookies', compact('meta'));
  }

  public function security()
  {
    $meta = [ 'title' => 'Сигурност и дискретност',
              'description' => 'Политика за обработване на лични данни и друга поверителна информация',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'лични данни, информация, Вашите права, НЕСТЛЕ БЪЛГАРИЯ'
    ];
    return view('information.security', compact('meta'));
  }

  public function general_policy()
  {
    $meta = [ 'title' => 'Политика на “Нестле България“ АД за поверителност на данните',
              'description' => 'Политика за обработване на лични данни и друга поверителна информация',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'лични данни, информация, Вашите права, НЕСТЛЕ БЪЛГАРИЯ'
    ];
    return view('information.general_policy', compact('meta'));
  }

}
