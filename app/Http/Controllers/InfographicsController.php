<?php namespace NescafeAlegria\Http\Controllers;

class InfographicsController extends Controller {

	public function delivery() {
    $meta = [ 'title' => 'УСЛОВИЯ ЗА ДОСТАВКА',
              'description' => 'Създайте решението за Вашия бизнес',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'заявка, кафемашина, инсталиране, монтаж'
    ];
		return view('infographics.delivery', compact('meta'));
	}

	public function plan() {
    $meta = [ 'title' => 'NESCAFÉ® PLAN',
              'description' => 'Като кафето на хората NESCAFÉ® носи специална отговорност да подсигури достъпността на кафето със страхотен вкус за всички',
              'image' => 'https://'.$_SERVER['SERVER_NAME'].'/images/deliveries/nescafe-alegria-logo.png' ,
              'keywords' => 'план, кафемашина, ЗЕЛЕНО КАФЕ, монтаж'
            ];
		return view('infographics.plan', compact('meta'));
	}
}
