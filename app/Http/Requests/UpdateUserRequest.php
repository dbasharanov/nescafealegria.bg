<?php namespace NescafeAlegria\Http\Requests;

use NescafeAlegria\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;

class UpdateUserRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::user()->isAdministrator();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'name' => 'required|min:2',
			'email' => "required|email|unique:users,email,{$this->segment(3)},id",
			'password' => 'min:6'
		];
	}

}
