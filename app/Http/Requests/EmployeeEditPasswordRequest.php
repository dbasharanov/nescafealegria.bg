<?php namespace NescafeAlegria\Http\Requests;

use NescafeAlegria\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use NescafeAlegria\Cycle;

class EmployeeEditPasswordRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		#return Cycle::where('id', $cycleId)
		#			->where('trading_person_name', Auth::user()->name)->exists();
		return true
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'password' => 'required|min:6',
		];
	}

}
