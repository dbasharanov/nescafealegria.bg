<?php namespace NescafeAlegria\Http\Requests;

use Illuminate\Support\Facades\Auth;
use NescafeAlegria\Http\Requests\Request;

class AdminEditCycleRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return Auth::user()->isAdministrator();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [];
	}

}
