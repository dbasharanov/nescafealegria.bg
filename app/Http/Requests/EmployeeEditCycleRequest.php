<?php namespace NescafeAlegria\Http\Requests;

use NescafeAlegria\Http\Requests\Request;
use Illuminate\Support\Facades\Auth;
use NescafeAlegria\Cycle;

class EmployeeEditCycleRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		$cycleId = $this->route('own_cycles');
		return Cycle::owned()->where('id', $cycleId)->exists();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [];
	}
}
