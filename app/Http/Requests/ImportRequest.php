<?php namespace NescafeAlegria\Http\Requests;

use Illuminate\Support\Facades\Auth;
use NescafeAlegria\Http\Requests\Request;

class ImportRequest extends Request {

	public function authorize()
	{
		return Auth::user()->isAdministrator();
	}

	public function rules()
	{
		return [
			#'file' => 'mimes:xls,xlsx,csv,txt'
		];
	}
}
