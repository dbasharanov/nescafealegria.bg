<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

// Public Website
Route::get('/', 'HomeController@index');
Route::get('/configuration/step-1', 'ConfigurationController@step_1');
Route::get('/configuration/step-2', 'ConfigurationController@step_2');
Route::get('/configuration/step-3', 'ConfigurationController@step_3');
Route::get('/configuration/result/{id}', 'ConfigurationController@result');

Route::resource('contacts', 'ContactsController', ['only' => ['index', 'store']]);
Route::resource('order', 'OrderController', ['only' => ['index', 'store']]);
Route::get('/order/thankyou', 'OrderController@thankyou');
Route::resource('subscriptions', 'SubscriptionsController', ['only' => ['store']]);
Route::get('/subscriptions/thankyou', 'SubscriptionsController@thankyou');
Route::get('/unsubscribe', 'SubscriptionsController@unsubscribe');
Route::get('/unsubscribe/{id}', 'SubscriptionsController@unsubscribe');
Route::post('/unsubscribe', ['as' => 'subscriptions.unsubscribe', 'uses' => 'SubscriptionsController@unsubscribe'] );
Route::resource('benefits', 'BenefitsController', ['only' => ['index']]);
Route::resource('products', 'ProductsController', ['only' => ['index','show']]);
Route::resource('beverages', 'BeveragesController', ['only' => ['index','show']]);
Route::resource('deliveries', 'DeliveriesController', ['only' => ['index']]);
Route::get('beverages/category/{id}', 'BeveragesController@category');
Route::get('delivery', 'InfographicsController@delivery');
Route::get('nescafe-plan', 'InfographicsController@plan');
Route::resource('search', 'SearchController', ['only' => ['index','show']]);
Route::resource('businesses', 'BusinessesController', ['only' => ['show']]);
Route::get('promotions', 'HomeController@promotions');
Route::get('promotions/{slug}', 'HomeController@promotion');

/*Route::get('/business/petrolstation', 'BusinessesController@petrolstation');
Route::get('/business/hotel', 'BusinessesController@hotel');
Route::get('/business/coffeeshop', 'BusinessesController@coffeeshop');
Route::get('/business/shop', 'BusinessesController@shop');
Route::get('/business/office', 'BusinessesController@office');*/

Route::get('/information/tos', 'InformationController@tos');
Route::get('/information/cookies', 'InformationController@cookies');
Route::get('/information/security', 'InformationController@security');
Route::get('/information/general-policy', 'InformationController@general_policy');

Route::get('/sitemap.xml', 'SitemapController@index');
Route::get('/sitemap/beverages', 'SitemapController@beverages');
Route::get('/sitemap/machines', 'SitemapController@machines');
Route::get('/sitemap/products', 'SitemapController@products');

Route::resource('machines', 'MachinesController', ['only' => ['index','show']]);
// Authentication routes
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

Route::get('/export', ['as' => 'export', 'uses' => 'ExportsController@index']);
Route::post('exports/orders', ['as' => 'exports.orders', 'uses' => 'ExportsController@orders']);

// Employees Intranet
Route::group(['prefix' => 'admin_rlAPkBI5RRPqmty1pVu4', 'namespace' => 'Admin', 'middleware' => 'auth'], function() {
	// Only admin group has access to these routes.
	Route::group(['middleware' => ['auth', 'acl'], 'is' => 'administrator'], function() {
		Route::resource('users', 'UsersController', ['only' => ['index', 'create', 'destroy', 'edit', 'update', 'store']]);

		Route::get('orders/export', ['as' => 'admin_rlAPkBI5RRPqmty1pVu4.orders.export', 'uses' => 'OrdersController@export']);

		Route::post('orders/import', ['as' => 'admin_rlAPkBI5RRPqmty1pVu4.orders.import', 'uses' => 'OrdersController@import']);

		Route::get('cycles/export', ['as' => 'admin_rlAPkBI5RRPqmty1pVu4.cycles.export', 'uses' => 'CyclesController@export']);
		Route::resource('cycles', 'CyclesController', ['except' => 'index']);
		Route::post('cycles/import', ['as' => 'admin_rlAPkBI5RRPqmty1pVu4.cycles.import', 'uses' => 'CyclesController@import']);
		Route::post('verifications/import', ['as' => 'admin_rlAPkBI5RRPqmty1pVu4.verifications.import', 'uses' => 'VerificationsController@import']);
	});

	// every employee has access to these routes.
	Route::get('/', 'DashboardController@index');
	Route::resource('visits', 'VisitsController', ['only' => 'index']);
	Route::resource('own_cycles', 'OwnCyclesController', ['only' => ['edit', 'update']]);
	Route::resource('cycles', 'CyclesController');
	Route::get('cycles/disable/{id}',['as' => 'admin_rlAPkBI5RRPqmty1pVu4.cycles.disable', 'uses' => 'CyclesController@disable']);
	Route::get('users/change_password', ['as' => 'admin_rlAPkBI5RRPqmty1pVu4.users.change_password', 'uses' => 'UsersController@change_password']);
	Route::post('users/change_password', ['as' => 'admin_rlAPkBI5RRPqmty1pVu4.users.update_password', 'uses' => 'UsersController@update_password']);
	Route::resource('schedules', 'SchedulesController');
	Route::post('schedules/toggle/{id}', ['as' => 'admin_rlAPkBI5RRPqmty1pVu4.schedules.toggle', 'uses' => 'SchedulesController@toggle']);
	Route::resource('orders', 'OrdersController');
	Route::resource('objects', 'ObjectsController');
});

// Editor's Panel
