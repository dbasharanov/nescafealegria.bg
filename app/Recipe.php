<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;

class Recipe extends \SleepingOwl\Models\SleepingOwlModel {

  // 

  protected $fillable = ['name','beverages'];

  public function beverages()
  {
    return $this->belongsToMany('NescafeAlegria\Beverage');
  }
  public static function getList()
  {
    return static::lists('name', 'id');
  }

  public function setBeveragesAttribute($beverages)
  {
    $this->beverages()->detach();
    if ( ! $beverages) return;

    if ( ! $this->exists) $this->save();

    $this->beverages()->attach($beverages);
  }

}
