<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Beverage extends \SleepingOwl\Models\SleepingOwlModel implements ModelWithImageFieldsInterface, SluggableInterface {

	//
  use ModelWithImageOrFileFieldsTrait;
  use SluggableTrait;

  protected $fillable = ['name', 'slug', 'summary', 'description', 'position', 'products', 'recipes', 'image','category_id','meta_description','keywords'];
  protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug',];

  public function getImageFields()
  {
      return [
          'image' => ['attachments/beverages/', function($directory, $originalName, $extension)
            {
              return $this->getSlug() .'.' . $this->id . '.'.$extension;
            }]
      ];
  }

  public function products() {
    return $this->belongsToMany('NescafeAlegria\Product');
  }
  public function recipes() {
    return $this->belongsToMany('NescafeAlegria\Recipe');
  }

  public static function getList() {
    return static::lists('name', 'id');
  }

  public function setProductsAttribute($products) {  
    $this->products()->detach();
    if ( ! $products) return;

    if ( ! $this->exists) $this->save();
    
    $this->products()->attach($products);
  }

  public function setRecipesAttribute($recipes) {  
    $this->recipes()->detach();
    if ( ! $recipes) return;

    if ( ! $this->exists) $this->save();
    
    $this->recipes()->attach($recipes);
  }

}
