<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Category extends \SleepingOwl\Models\SleepingOwlModel implements SluggableInterface {
 
  use SluggableTrait;

  protected $fillable = ['name','summary', 'is_active'];
  protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug',];

  public function beverages() {
    return $this->hasMany('NescafeAlegria\Beverage');
  }
  public static function getList() {
    return static::lists('name', 'id');
  }

}