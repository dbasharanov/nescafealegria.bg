<?php namespace NescafeAlegria\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel {

	/**
	 * The Artisan commands provided by your application.
	 *
	 * @var array
	 */
	protected $commands = [
		'NescafeAlegria\Console\Commands\Inspire',
		'NescafeAlegria\Console\Commands\GenerateVisits',
		'NescafeAlegria\Console\Commands\GenerateSchedules'
	];

	/**
	 * Define the application's command schedule.
	 *
	 * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
	 * @return void
	 */
	protected function schedule(Schedule $schedule)
	{
		$schedule->command('visits:generate')
				 ->daily();
		$schedule->command('schedules:generate')
				 ->daily();
				 //->cron('* * * * * *');

	}

}
