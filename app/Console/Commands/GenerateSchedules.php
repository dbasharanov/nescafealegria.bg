<?php namespace NescafeAlegria\Console\Commands;

use NescafeAlegria\User as UserModel;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GenerateSchedules extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'schedules:generate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate weekly schedule for all trading persons';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$users = UserModel::all();
		foreach ($users as $user) {
			$this->comment("Generating weekly schedule for user " .$user->name);
			$user->generate_schedule();
		}
		$this->comment(PHP_EOL."All done".PHP_EOL);

	}

}
