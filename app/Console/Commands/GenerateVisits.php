<?php namespace NescafeAlegria\Console\Commands;

use NescafeAlegria\User as UserModel;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GenerateVisits extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'visits:generate';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Generate daily vistis for all trading persons';

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$users = UserModel::all();
		foreach ($users as $user) {
			$this->comment("Generating daily visits for user " .$user->name);
			$user->generate_visits();
		}
		$this->comment(PHP_EOL."All done".PHP_EOL);
	}

}
