<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use NescafeAlegria\CanBeImportedTrait;
use NescafeAlegria\Observers\CycleObserver;
use Auth;
use Carbon\Carbon;

class Cycle extends Model {
	use CanBeImportedTrait;
	use CanBeFilteredByUserTrait;

	public static function boot()
	{
		parent::boot();

		parent::observe(new CycleObserver);
	}

	protected $fillable = [
		'operator_name',
		'poc_client_number',
		'poc_responsible_name',
		'poc_code',
		'poc_name',
		'contact_name',
		'address',
		'city',
		'location',
		'phone',
		'location_hint',
		'bmb_number',
		'machine_sn',
		'machine_model',
		'operator_code',
		'install_date',
		'trading_person_name',
		'start_date',
		'period',
		'edited'
	];

	private static $mapping = [
		'ime_na_operator' => 'operator_name',
		'ros_klient' => 'poc_responsible_name',
		'kod_na_poc' => 'poc_code',
		'ime_na_poc' => 'poc_name',
		'litse_za_kontakt' => 'contact_name',
		'adres' => 'address',
		'grad' => 'city',
		'geografsko_mestopolozhenie' => 'location',
		'nomer_telefon' => 'phone',
		'podskazvane_na_mestopolozhenie' => 'location_hint',
		'mashina' => 'bmb_number',
		'serien_nomer' => 'machine_sn',
		'konfiguratsiya_na_selektori' => 'machine_model',
		'kontrolen_nomer_na_operator' => 'operator_code',
		'zachislena_na' => 'install_date',
		'trgovski_predstavitel' => 'trading_person_name',
		'nachalo_na_tsikbla' => 'start_date',
		'tsikbl_na_poseshchenie' => 'period',
		'ros_klient_no' => 'poc_client_number',
		'edited' => 'edited',
	];

	public static $originalNames = [
		'operator_name' => 'Име на Оператор',
		'poc_client_number' => 'POC Клиент No',
		'poc_responsible_name' => 'POC Клиен',
		'poc_code' => 'Код на POC',
		'poc_name' => 'Име на POC',
		'contact_name' => 'Лице за контакт',
		'address' => 'Адрес',
		'city' => 'Град',
		'location' => 'Географско местоположение',
		'phone' => 'Телефон',
		'location_hint' => 'Подсказване на Местоположение',
		'bmb_number' => 'Машина',
		'machine_sn' => 'Сериен номер',
		'machine_model' => 'Конфигурация на Селектори',
		'operator_code' => 'Контролен номер на Оператор',
		'install_date' => 'Зачислена на',
		'trading_person_name' => 'Търговски представител',
		'start_date' => 'НАЧАЛО НА ЦИКЪЛА',
		'period' => 'ЦИКЪЛ НА ПОСЕЩЕНИЕ',
		'edited' => 'Променен'
	];

	public function generate_future_visits($old_start_date, $old_period)
	{
		if($old_period == 0)
			return;
		$today = Carbon::today()->toDateString();
		$new_start_date = new Carbon($this->start_date);
		$new_period = $this->period;
		#$edge_date = (new Carbon($new_start_date))->addDays($new_period);
		$edge_date = new Carbon($new_start_date);
		Visit::whereBmbNumber($this->bmb_number)->whereTradingPersonName($this->trading_person_name)->where("calendar_date", ">", $today)->delete();
		#$new_date = (new Carbon($old_start_date))->addDays($old_period);
		$new_date = new Carbon($old_start_date);

		while($edge_date->gt($new_date))
		{
			if($new_date->gt(Carbon::today()))
			{
				$visit = Visit::create([
					'trading_person_name' => $this->trading_person_name,
					'poc_name' => $this->poc_name,
					'bmb_number' => $this->bmb_number,
					'calendar_date' => $new_date,
					'start_date' => $old_start_date,
					'period' => $old_period,
					'machine_model' => $this->machine_model,
					'machine_sn' => $this->machine_sn]);
			}
			$new_date = $new_date->addDays($old_period);
		}
	}

	public function checkDateForVisit($date)
	{
		$start_date = new Carbon($this->start_date);
		#$visit_offset = $start_date->addDays($this->period)->diffInDays($date) % $this->period;
		$visit_offset = $start_date->diffInDays($date) % $this->period;
		if($visit_offset == 0)
		{
			$row = new Cycle;
			$row->id = $this->id;
			$row->start_date = $date;
			$row->operator_name = $this->operator_name;
			$row->period = $this->period;
			$row->trading_person_name = $this->trading_person_name;
			$row->poc_name = $this->poc_name;
			$row->address = $this->address;
			$row->bmb_number = $this->bmb_number;
			$row->location = $this->location;
			$row->city = $this->city;
			$row->machine_sn = $this->machine_sn;
			$row->machine_model = $this->machine_model;
			return $row;
		}
		return false;
	}

	public static function scopeOwned($query)
	{
		$subjects = Auth::user()->getSubjectsNames();
		array_push($subjects, "");
		return $query->whereIn('trading_person_name', $subjects);
	}

	public static function scopeActive($query)
	{
		return $query->where('period', '>', 0);
	}

	public function setStartDateAttribute($value)
	{
		$this->attributes['start_date'] = new Carbon($value);
	}

	public function setPeriodAttribute($value)
	{
		$this->attributes['period'] = (int)$value;
	}

	public function setTradingPersonNameAttribute($value)
	{
		if($value == NULL)
			$this->attributes['trading_person_name'] = "";
		else
			$this->attributes['trading_person_name'] = $value;
	}

	public function getStartDateAttribute($value)
	{
		return Carbon::parse($this->attributes['start_date'])->format('d.m.Y');
	}

	public function getInstallDateAttribute($value)
	{
		return Carbon::parse($this->attributes['install_date'])->format('d.m.Y');
	}
}
