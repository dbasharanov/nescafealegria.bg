<?php // Code within app\Helpers\Helper.php

namespace NescafeAlegria\Helpers;

class Helper
{
    public static function get_url()
    {
        $path =preg_replace('/\\?.*/', '', $_SERVER['REQUEST_URI']);
        return strip_tags("https://$_SERVER[HTTP_HOST]$path");
        #return "http://$_SERVER[HTTP_HOST]$_SERVER['REQUEST_URI']";
    }

    public static function cups_per_day($string)
    {
    	if($string == '0')
    		return '0.00';
    	elseif(strlen($string) < 4)
    		return $string . '0';
    	else
    		return substr($string, 0, 4);
    }
}