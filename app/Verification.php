<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use NescafeAlegria\CanBeImportedTrait;
use NescafeAlegria\Observers\VerificationObserver;

class Verification extends Model {
	use CanBeImportedTrait;

	public static function boot()
	{
		parent::boot();

		parent::observe(new VerificationObserver);
	}

	protected $fillable = [
		'year_month',
		'operator_name',
		'calendar_date',
		'poc_name',
		'bmb_number',
		'trading_person_name',
		'audit_type',
	];

	private static $mapping = [
	'vd_calyearmonthyyyymm' => 'year_month',
	'operator_name' => 'operator_name',
	'calendar_date' => 'calendar_date',
	'poc_name' => 'poc_name',
	'machine_bmb_number' => 'bmb_number',
	'bmb_user_ofu_name' => 'trading_person_name',
	'audit_procedure_name' => 'audit_type',
	'vm_number_of_audits' => '',
	];
}
