<?php namespace NescafeAlegria;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Kodeine\Acl\Traits\HasRole;
use Carbon\Carbon;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword, HasRole;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password', 'user_id', 'operator'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];

	public function generate_visits()
	{
		$clients = Cycle::whereTradingPersonName($this->name)->active()->get();
		$today = Carbon::today();
		foreach ($clients as $client)
		{
			$start_date = new Carbon($client->start_date);
			$edge_date = (new Carbon($start_date))->addDays($client->period);
			// echo $client->start_date."|".$client->period."|".$edge_date."|".$today."|".$edge_date->lte($today)."|".$today->diffInDays($start_date)."|".(($today->diffInDays($start_date)) % $client->period)."\n";
			if(($today->diffInDays($start_date) % $client->period) == 0)
			{
				$visit = Visit::create([
					'trading_person_name' => $this->name,
					'poc_name' => $client->poc_name,
					'bmb_number' => $client->bmb_number,
					'calendar_date' => Carbon::today(),
					'start_date' => $client->start_date,
					'period' => $client->period,
					'machine_model' => $client->machine_model,
					'machine_sn' => $client->machine_sn]);
			}
		}
	}

	public function generate_schedule($period_in_days = 2)
	{
		$clients = Cycle::whereTradingPersonName($this->name)->active()->get();
		$today = Carbon::today();

		foreach ($clients as $client)
		{
			$start_date = new Carbon($client->start_date);
			#$visit_offset = $start_date->addDays($client->period)->diffInDays($today) % $client->period;
			$visit_offset = $start_date->diffInDays($today) % $client->period;

			if(($visit_offset + $period_in_days) > $client->period )
			{
				#echo "1";
				$days_from_now = $client->period - $visit_offset;
				$visit = Schedule::create([
					'trading_person_name' => $this->name,
					'poc_name' => $client->poc_name,
					'calendar_date' => Carbon::today()->addDays($days_from_now),
					'machine_sn' => $client->machine_sn,
					'machine_model' => $client->machine_model,
					'visited' => false,
					'period' => $client->period,
					'location' => $client->city,
					'address' => $client->address,
					'bmb_number' => $client->bmb_number,
				]);
			}
			for($i=0; $i<$period_in_days; $i++)
			{
				#echo "2";
				$check_for = Carbon::today();
				$check_for = $check_for->addDays($i);
				#echo $check_for;
				if($start_date->eq($check_for))
				{
					#echo "3";
					#dd(Schedule::where('bmb_number', '=', $client->bmb_number)->where('calendar_date', '=', $check_for->format('Y-m-d'))->exists());
					if(!Schedule::where('bmb_number', '=', $client->bmb_number)->where('calendar_date', '=', $check_for->format('Y-m-d'))->exists()) {
						#echo "4";
						$visit = Schedule::create([
							'trading_person_name' => $this->name,
							'poc_name' => $client->poc_name,
							'calendar_date' => $check_for,
							'machine_sn' => $client->machine_sn,
							'machine_model' => $client->machine_model,
							'visited' => false,
							'period' => $client->period,
							'location' => $client->city,
							'address' => $client->address,
							'bmb_number' => $client->bmb_number,
						]);
					}
				}
			}
			#echo "\n";
		}
	}

	public function isSubject($user_id)
	{
		return (0 == $user_id || $user_id == $this->id || $this->users()->where('id', $user_id)->count() > 0 || $this->isAdministrator());
	}

	public function getAllUserNames()
	{
		$allUsers = $this->users->lists('name');
		array_push($allUsers, $this->name);
		return $allUsers;
	}

	public function getSubjectsNames()
	{
		if($this->isAdministrator())
		{
			return \DB::table('users')->lists('name', 'id');
		}

		$my_subjects = \DB::table('users')->select('name', 'id')->where('user_id', $this->id);
		return \DB::table('users')->select('name', 'id')->where('id', $this->id)->union($my_subjects)->lists('name', 'id');
	}

	public function getOperatorsNames()
	{
		if($this->isThirdPartyEmployee())
		{
			return [$this->operator =>$this->operator];
		}
		$operators = ['Всички' => 'Всички'] + \DB::table('cycles')->select('operator_name')->where('operator_name', '<>', '')->groupBy('operator_name')->lists('operator_name', 'operator_name');
		return $operators;
	}

	public function users()
	{
		return $this->hasMany('NescafeAlegria\User');
	}

	public function isSupervisor()
	{
		return $this->users()->count() > 0;
	}

	public function isThirdPartyEmployee()
	{
		return $this->roles()->where('role_id', '=', 4)->count();
	}

	public function supervisor()
	{
		return $this->belongsTo('NescafeAlegria\User', 'user_id');
	}


	public function setUserIdAttribute($value)
	{
		$this->attributes['user_id'] = ($value == 0 ? null : $value);
	}
}
