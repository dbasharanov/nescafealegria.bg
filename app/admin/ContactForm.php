<?php

Admin::model(NescafeAlegria\ContactForm::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::string('first_name', 'Name');
  Column::string('last_name', 'Name');
  Column::string('created_at', 'Date');
})->form(function ()
{
  FormItem::select('question_type', 'Question')->list([     0  => 'Запитване за продукт', 
                                                            1  => 'Запитване за машина', 
                                                            2  => 'Контакт с търговски представител', 
                                                            3 => 'Друго']);
  FormItem::text('first_name', 'First Name');
  FormItem::text('last_name', 'Last Name');
  FormItem::text('phone_number', 'Phone Number');
  FormItem::text('email', 'Email');
  FormItem::text('city', 'City');
  FormItem::file('file', 'File');
  FormItem::ckeditor('message', 'Message');

});