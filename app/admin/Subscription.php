<?php

Admin::model(NescafeAlegria\Subscription::class)->title('Subscription')->with()->filters(function ()
{
    ModelItem::filter('subscribed')->title('subscribed');
})->columns(function ()
{
  Column::string('email', 'Email');
  Column::string('subscribed', 'Subscribed')->append(Column::filter('subscribed')->value('subscribed'));
  Column::string('updated_at', 'Last Update');
})->form(function ()
{
	FormItem::text('email', 'Email');
});