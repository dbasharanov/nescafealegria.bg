<?php

/*
 * Describe your menu here.
 *
 * There is some simple examples what you can use:
 *
 * 		Admin::menu()->url('/')->label('Start page')->icon('fa-dashboard')->uses('\AdminController@getIndex');
 * 		Admin::menu(User::class)->icon('fa-user');
 * 		Admin::menu()->label('Menu with subitems')->icon('fa-book')->items(function ()
 * 		{
 * 			Admin::menu(\Foo\Bar::class)->icon('fa-sitemap');
 * 			Admin::menu('\Foo\Baz')->label('Overwrite model title');
 * 			Admin::menu()->url('my-page')->label('My custom page')->uses('\MyController@getMyPage');
 * 		});
 */

//Admin::menu()->url('/')->label('Start Page')->icon('fa-dashboard');
//Admin::menu()->url('/')->label('Start Page')->icon('fa-dashboard');
Admin::menu(\NescafeAlegria\OrderForm::class)->label('Orders')->icon('fa-credit-card');
Admin::menu(\NescafeAlegria\ContactForm::class)->label('Contacts')->icon('fa-envelope');
Admin::menu(\NescafeAlegria\Subscription::class)->label('Subscriptions')->icon('fa-child');
Admin::menu(\NescafeAlegria\Beverage::class)->label('Beverages')->icon('fa-coffee');
Admin::menu(\NescafeAlegria\Recipe::class)->label('Recipes')->icon('fa-star');
Admin::menu(\NescafeAlegria\Machine::class)->label('Machines')->icon('fa-rocket');
Admin::menu(\NescafeAlegria\Product::class)->label('Products')->icon('fa-cubes');
Admin::menu(\NescafeAlegria\Business::class)->label('Business')->icon('fa-briefcase');
Admin::menu(\NescafeAlegria\Testimonial::class)->label('Testimonials')->icon('fa-comments-o');
Admin::menu(\NescafeAlegria\Slide::class)->label('Slides')->icon('fa-picture-o');
Admin::menu(\NescafeAlegria\Notice::class)->label('Promotions')->icon('fa-list');
Admin::menu()->url(route('export'))->icon('fa-wrench')->label('Export');
