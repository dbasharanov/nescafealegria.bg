<?php

Admin::model(NescafeAlegria\Recipe::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::string('name', 'Name');
})->form(function ()
{
  FormItem::text('name', 'Name');
  FormItem::multiSelect('beverages', 'Beverages')->list('NescafeAlegria\Beverage')->value('beverages.beverage_id');
});