<?php

Admin::model(\NescafeAlegria\Category::class)->title('Category')->with()->filters(function ()
{

})->columns(function ()
{
	Column::string('name', 'Name');
	Column::string('summary', 'Summary');
  Column::count('beverages');
})->form(function ()
{
  FormItem::text('name', 'Name');
  FormItem::ckeditor('summary', 'Summary');
  FormItem::checkbox('is_active', 'Approved');
});