<?php

Admin::model(NescafeAlegria\Slide::class)->title('Slide')->with()->filters(function ()
{

})->columns(function ()
{
  Column::image('image', 'Image');
  Column::image('bullet', 'Small Image');
  Column::string('title', 'Title');
  Column::string('description', 'Description');
})->form(function ()
{
  FormItem::image('image', 'Image - recommended 820x640');
  FormItem::image('bullet', 'Small Image - recommended 60x60');
  FormItem::text('title', 'Title');
  FormItem::text('description', 'Description');
  FormItem::text('slug', 'Link');
  FormItem::text('position', 'Position');
  });