<?php

Admin::model(NescafeAlegria\Product::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::image('image', 'Image');
  Column::string('name', 'Name');
  Column::string('summary', 'Summary');
  //Column::lists('beverages.name', 'Beverages');
})->form(function ()
{
	FormItem::text('name', 'Name');
  //FormItem::text('slug', 'Friendly URL');
  FormItem::image('image', 'Image - recommended 560x560');
  FormItem::text('summary', 'Summary');
  FormItem::ckeditor('description', 'Description');
  FormItem::multiSelect('beverages', 'Beverages')->list('NescafeAlegria\Beverage')->value('beverages.beverage_id');
  FormItem::text('meta_description', 'Meta Description (use a maximum of 155 characters)');
  FormItem::text('keywords', 'Keywords');
});