<?php

Admin::model(NescafeAlegria\Beverage::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::image('image', 'Image');
  Column::string('name', 'Name');
  Column::string('summary', 'Summary');
  Column::lists('products.name', 'Products');
})->form(function ()
{
	FormItem::text('name', 'Name');
  FormItem::text('slug', 'Friendly URL');
  FormItem::image('image', 'Image - recommended 560x560');
  FormItem::text('summary', 'Summary');
  FormItem::text('position', 'position');
	FormItem::ckeditor('description', 'Description');
  FormItem::multiSelect('products', 'Products')->list('NescafeAlegria\Product')->value('products.product_id');
  FormItem::multiSelect('recipes', 'Recipes')->list('NescafeAlegria\Recipe')->value('recipes.recipe_id');
  FormItem::select('category_id', 'CategoryId')->list('NescafeAlegria\Category')->nullable();
  FormItem::text('meta_description', 'Meta Description (use a maximum of 155 characters)');
  FormItem::text('keywords', 'Keywords');
});