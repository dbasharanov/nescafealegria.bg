<?php

Admin::model(NescafeAlegria\Testimonial::class)->title('Testimonial')->with()->filters(function ()
{

})->columns(function ()
{
  Column::image('image', 'Image');
  Column::string('name', 'Name');
  Column::string('description', 'Description');
  Column::count('businesses');
})->form(function ()
{
  FormItem::image('image', 'Image - recommended 200x200');
  FormItem::text('name', 'Name');
  FormItem::text('position', 'Position');
  FormItem::text('company', 'Company');
  FormItem::text('type', 'Type');
  FormItem::ckeditor('description', 'Description');
  FormItem::select('business_id', 'BusinessId')->list('NescafeAlegria\Business')->nullable();
});