<?php

Admin::model(NescafeAlegria\Machine::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::image('image', 'Image');
	Column::string('name', 'Name');
	Column::string('slogan', 'Slogan');
	//Column::string('description', 'Description');
})->form(function ()
{
	FormItem::text('name', 'Name');
  //FormItem::text('slug', 'Friendly URL');
  FormItem::image('image', 'Image - recommended 480x525');
  FormItem::text('slogan', 'Slogan');
	FormItem::ckeditor('summary', 'Summary');
	FormItem::text('description', 'Description');
  FormItem::text('meta_description', 'Meta Description (use a maximum of 155 characters)');
  FormItem::text('keywords', 'Keywords');
  FormItem::text('position', 'Position');
});