<?php

Admin::model(NescafeAlegria\Business::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::image('image', 'Image');
  Column::string('name', 'Name');
  Column::string('description', 'Description');
})->form(function ()
{
  FormItem::text('name', 'Name');
  //FormItem::text('slug', 'Slug');
  FormItem::image('image', 'Image - recommended 1350x900');
  FormItem::ckeditor('description', 'Description');
  FormItem::text('meta_description', 'Meta Description (use a maximum of 155 characters)');
  FormItem::text('keywords', 'Keywords');
  
});