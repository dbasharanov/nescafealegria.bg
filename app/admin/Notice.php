<?php

Admin::model(NescafeAlegria\Notice::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::image('image', 'Image');
  Column::string('title', 'title');
  Column::string('slug', 'url');
  //Column::string('description', 'Description');
})->form(function ()
{
  FormItem::text('title', 'title');
  FormItem::text('slug', 'Friendly URL');
  FormItem::image('image', 'Image - recommended 1170x400');
  FormItem::image('banner', 'Banner - recommended 970x90');
  FormItem::text('summary', 'Summary');
  FormItem::ckeditor('description', 'Description');
  FormItem::checkbox('approved', 'Approved');
  FormItem::text('meta_description', 'Meta Description (use a maximum of 155 characters)');
  FormItem::text('keywords', 'Keywords');
});