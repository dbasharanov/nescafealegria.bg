<?php

Admin::model(NescafeAlegria\OrderForm::class)->title('')->with()->filters(function ()
{

})->columns(function ()
{
  Column::string('first_name', 'Name');
  Column::string('last_name', 'Name');
  Column::string('created_at', 'Date');
  Column::string('order_id', 'Order Number');
  //Column::lists('beverages.name', 'Beverages');
})->form(function ()
{
  FormItem::text('order_id', 'Order Number');
  FormItem::text('first_name', 'First Name');
  FormItem::text('last_name', 'Last Name');
  FormItem::text('phone_number', 'Phone Number');
  FormItem::text('email', 'email');
  FormItem::text('organization_name', 'Ogranization');
  FormItem::text('city', 'City');
  FormItem::select('business_type', 'Business')->list(['coffeeshop' => 'Кафетерия', 
                                                       'hotel'      => 'Хотел',
                                                       'shop'       => 'Магазин',
                                                       'office'     => 'Офис',
                                                       'petrol'     => 'Бензиностанция']);
  FormItem::text('recipe_name', 'Recipe');
  FormItem::text('beverages', 'Beverages');
  FormItem::text('employees', 'Employees');
  FormItem::ckeditor('message', 'Message');

});