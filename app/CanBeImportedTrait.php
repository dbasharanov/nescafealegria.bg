<?php namespace NescafeAlegria;
use Exception;
trait CanBeImportedTrait {
	public static function createFromExcel($data = [])
	{
		$normalized_data = [];
		foreach($data as $key => $value) {
			if(array_key_exists($key, static::$mapping))
			{
				if($key == 'calendar_date') {
					$value =  \Carbon\Carbon::createFromFormat('m/d/Y', $value);
				}
				if($key == 'edited') {
					$value = 0;
				}
				$normalized_data[static::$mapping[$key]] = $value;
			}
			else
			{
				$normalized_data[$key] = $value;
			}
		}
		# dd($normalized_data);
		$instance = static::create($normalized_data);
		return $instance;
	}

	public static function getOriginalNames()
	{
		$generatedNames = [];
		foreach(static::getFillableColumns() as $attribute)
		{
			array_push($generatedNames, static::$originalNames[$attribute]);
		}
		return $generatedNames;
	}

	public static function getFillableColumns()
	{
		return (new static)->fillable;
	}

	public function scopeFillableColumns($query)
	{
		return $query->select($this->fillable);
	}
}
