<?php namespace NescafeAlegria\Observers;

use NescafeAlegria\Consumer;

class ConsumerObserver {
	public function saving($consumer)
	{
		$consumer->consumer_id = '10966342_'.md5($consumer->email_value.'BG'.$consumer->created_at);
	}
}