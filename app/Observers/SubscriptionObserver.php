<?php namespace NescafeAlegria\Observers;

use NescafeAlegria\Subscription;
use NescafeAlegria\Consumer;

class SubscriptionObserver {
	public function created($subscription)
	{
		$consumer = Consumer::where('email_value', '=', $subscription->email)->first();
		if($consumer===null) {
			$consumer = Consumer::create(['email_value' => $subscription->email]);
		}
	}
}