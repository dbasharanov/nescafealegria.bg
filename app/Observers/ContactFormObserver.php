<?php namespace NescafeAlegria\Observers;

use NescafeAlegria\ContactForm;
use NescafeAlegria\Consumer;

class ContactFormObserver {
	public function created($contact_form)
	{
		$consumer = Consumer::where('email_value', '=', $contact_form->email)->first();
		if($consumer===null) {
			$consumer = Consumer::create(['email_value' => $contact_form->email,
										  'first_name' => $contact_form->first_name,
										  'last_name' => $contact_form->last_name]);
		}
		else {
			$consumer->first_name = $contact_form->first_name;
			$consumer->last_name = $contact_form->last_name;
			$consumer->save();
		}
	}
}