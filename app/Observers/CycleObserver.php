<?php namespace NescafeAlegria\Observers;

use NescafeAlegria\Cycle;
use Carbon\Carbon;

class CycleObserver {
	public function saving($cycle)
	{
		if($cycle->bmb_number == NULL)
			$cycle->bmb_number = uniqid('g', false);
	}
	public function updating($cycle)
	{
		$start_date = new Carbon($cycle['original']['start_date']);
		if($start_date->gt(Carbon::today()))
			$cycle->generate_future_visits($cycle['original']['start_date'], $cycle['original']['period']);
		$cycle->edited = 1;
	}
}
