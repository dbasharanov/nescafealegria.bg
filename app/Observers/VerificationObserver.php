<?php namespace NescafeAlegria\Observers;

use NescafeAlegria\Visit;

class VerificationObserver {
	public function created($verification)
	{
		return Visit::findAndMarkAsVisited($verification);
	}
}
