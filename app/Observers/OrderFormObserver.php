<?php namespace NescafeAlegria\Observers;

use NescafeAlegria\OrderForm;
use NescafeAlegria\Consumer;

class OrderFormObserver {
	public function created($order_form)
	{
		$consumer = Consumer::where('email_value', '=', $order_form->email)->first();
		if($consumer===null) {
			$consumer = Consumer::create(['email_value' => $order_form->email,
										  'first_name' => $order_form->first_name,
										  'last_name' => $order_form->last_name]);
		}
		else {
			$consumer->first_name = $order_form->first_name;
			$consumer->last_name = $order_form->last_name;
			$consumer->save();
		}
	}
}