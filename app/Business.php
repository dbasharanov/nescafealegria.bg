<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use SleepingOwl\Models\Interfaces\ModelWithImageFieldsInterface;
use SleepingOwl\Models\Traits\ModelWithImageOrFileFieldsTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Business extends \SleepingOwl\Models\SleepingOwlModel implements ModelWithImageFieldsInterface, SluggableInterface {

  //
  use ModelWithImageOrFileFieldsTrait;
  use SluggableTrait;

  protected $fillable = ['name', 'slug', 'description','image','meta_description','keywords'];
  protected $sluggable = ['build_from' => 'name', 'save_to' => 'slug',];

  public function getImageFields()
  {
      return [
          'image' => ['attachments/businesses/', function($directory, $originalName, $extension)
            {
              return $this->getSlug() .'.'.$extension;
            }]
      ];
  }

  public function testimonials() {
    return $this->hasMany('NescafeAlegria\Testimonial');
  }

  public static function getList() {
    return static::lists('name', 'id');
  }

}
