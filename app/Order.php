<?php namespace NescafeAlegria;

use Illuminate\Database\Eloquent\Model;
use NescafeAlegria\CanBeImportedTrait;
use Carbon\Carbon;

class Order extends Model {
	use CanBeImportedTrait;
	use CanBeFilteredByUserTrait;

	protected $fillable = [
		'market',
		'year_month',
		'operator_name',
		'poc_responsible_name',
		'poc_name',
		'city',
		'address',
		'channel_level_3_name',
		'machine_model',
		'machine_sn',
		'bmb_number',
		'machine_status',
		'recipe_name',
		'calendar_date',
		'ofu_name',
		'days',
		'cups_served',
		'cups_per_day',
		'machine_config_code',
		'poc_code',
	];

	private static $mapping = [
		'market_name' => 'market',
		'poc_responsible_name' => 'poc_responsible_name',
		'calendar_date' => 'calendar_date',
		'poc_name' => 'poc_name',
		'poc_code' => 'poc_code',
		'calyearmonth' => 'year_month',
		'operator_name' => 'operator_name',
		'poc_city' => 'city',
		'poc_address' => 'address',
		'poc_localization_channel_level_3_name' => 'channel_level_3_name',
		'ofu_name' => 'ofu_name',
		'machine_bmb_number' => 'bmb_number',
		'machine_sap_serial_number' => 'machine_sn',
		'machine_model_name' => 'machine_model',
		'last_machine_status' => 'machine_status',
		'serving_recipe_name' => 'recipe_name',
		'number_of_days' => 'days',
		'number_of_cups_served' => 'cups_served',
		'average_cups' => 'cups_per_day',
		'machine_configuration_code' => 'machine_config_code'
	];

	public static $originalNames = [
		'poc_responsible_name' => 'POC Responsible Name',
		'calendar_date' => 'Calendar Date',
		'poc_name' => 'POC Name',
		'poc_code' => 'POC Code',
		'market' => 'Market Name',
		'year_month' => 'Cal.Year/Month',
		'operator_name' => 'Operator Name',
		'city' => 'POC City',
		'address' => 'POC Address',
		'channel_level_3_name' => 'POC Localization Channel Level 3 Name',
		'bmb_number' => 'Machine BMB Number',
		'machine_sn' => 'Machine SAP Serial Number',
		'machine_model' => 'Machine Model Name',
		'machine_status' => 'Last Machine Status',
		'recipe_name' => 'Serving Recipe Name',
		'days' => 'Number of Days',
		'cups_served' => 'Number of cups Served',
		'cups_per_day' => 'Average cups',
		'machine_config_code' => 'Machine Configuration Code',
		'ofu_name' => 'OFU Name'
	];

	public function getCalendarDateAttribute($value)
	{
		return Carbon::parse($this->attributes['calendar_date'])->format('d.m.Y');
	}
}
