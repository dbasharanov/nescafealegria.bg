<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('businesses', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('slug')->nullable();
			$table->string('summary');
			$table->text('description');
			$table->string('image');
			$table->timestamps();
		});

		Schema::table('testimonials', function(Blueprint $table)
		{
			$table->integer('business_id');
		});

	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('businesses');
		
		Schema::table('testimonials', function(Blueprint $table)
		{
			$table->dropColumn('business_id');
		});
	}

}
