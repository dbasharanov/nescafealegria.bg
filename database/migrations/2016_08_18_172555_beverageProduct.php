<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BeverageProduct extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		//
		Schema::create('beverage_product', function(Blueprint $table)
		{
			$table->integer('beverage_id')->unsigned()->index();
	    $table->foreign('beverage_id')->references('id')->on('beverages')->onDelete('cascade');
	    $table->integer('product_id')->unsigned()->index();
	    $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		//
	}

}
