<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NullableTradingPerson extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
	    DB::statement('ALTER TABLE `cycles` MODIFY `trading_person_name` VARCHAR(255) NULL;');
	    DB::statement('UPDATE `cycles` SET `trading_person_name` = NULL WHERE `trading_person_name` = "";');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
	    DB::statement('UPDATE `cycles` SET `trading_person_name` = "" WHERE `trading_person_name` IS NULL;');
	    DB::statement('ALTER TABLE `cycles` MODIFY `trading_person_name` VARCHAR(255) NOT NULL;');
	}

}
