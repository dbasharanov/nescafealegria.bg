<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBusnessTypeToOrderForm extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('order_forms', function(Blueprint $table)
		{
			$table->string('recipe_name');
			$table->string('business_type');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('order_forms', function(Blueprint $table)
		{
			$table->dropColumn('recipe_name');
			$table->dropColumn('business_type');
		});
	}

}
