<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsumersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('consumers', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('consumer_id')->unique();
			$table->string('initial_source_app_code');
			$table->string('email_value');
			$table->string('mobile_value');
			$table->string('phone_value');
			$table->string('address_line_1');
			$table->string('address_line_2');
			$table->string('address_line_3');
			$table->string('address_line_4');
			$table->string('city');
			$table->string('zipcode');
			$table->string('region');
			$table->string('country_code');
			$table->string('title');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('additional_name_1');
			$table->string('additional_name_2');
			$table->string('salutation');
			$table->string('marital_status');
			$table->string('education');
			$table->string('prefered_language_code');
			$table->string('occupation');
			$table->string('sex');
			$table->string('company_name');
			$table->boolean('is_deleted');
			$table->string('household_owns_cats');
			$table->string('household_owns_dogs');
			$table->string('household_cats_count');
			$table->string('household_dogs_count');
			$table->string('household_other_pets_count');
			$table->string('household_residents_count');
			$table->string('household_adult_count');
			$table->string('household_children_count');
			$table->string('household_owns_pet');
			$table->string('is_contact_disallowed');
			$table->string('last_update');
			$table->string('creation_date');
			$table->string('is_email_valid');
			$table->string('is_postal_address_valid');
			$table->string('is_phone_valid');
			$table->string('is_mobile_valid');
			$table->string('do_not_contact_email');
			$table->string('do_not_contact_phone');
			$table->string('do_not_contact_mobile');
			$table->string('do_not_contact_address');
			$table->string('nutrition_cooking_like');
			$table->string('nutrition_cooking_dislike');
			$table->string('nutrition_sensitive_risk');
			$table->string('sfmc_entry_date');
			$table->string('sfmc_last_modified_date');
			$table->string('consumer_type');
			$table->string('birthdate_reliability');
			$table->string('nutrition_cooking_style');
			$table->string('nutrition_diet');
			$table->string('birthdate');
			$table->string('nutrition_cooking_frequency');
			$table->string('nutrition_cooking_level');
			$table->string('market_code');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('consumers');
	}

}
