<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSummaryToProducts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->string('summary');
			$table->decimal('position');

		});
		Schema::table('beverages', function(Blueprint $table)
		{
			$table->string('summary');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function(Blueprint $table)
		{
			$table->dropColumn('summary');
			$table->dropColumn('position');
		});
		Schema::table('beverages', function(Blueprint $table)
		{
			$table->dropColumn('summary');
		});
	}

}
