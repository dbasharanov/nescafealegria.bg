<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndecesToOrders extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->index('poc_name');
			$table->index('bmb_number');
			$table->index('machine_sn');
			$table->index('operator_name');
			$table->index('recipe_name');
			$table->index('calendar_date');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropIndex('poc_name');
			$table->dropIndex('bmb_number');
			$table->dropIndex('machine_sn');
			$table->dropIndex('operator_name');
			$table->dropIndex('recipe_name');
			$table->dropIndex('calendar_date');
		});
	}

}
