<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('visits', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('trading_person_name');
			$table->string('poc_name');
			$table->integer('bmb_number');
			$table->date('calendar_date');
			$table->string('audit_type');
			$table->date('start_date');
			$table->integer('period');
			$table->boolean('visited')->default(false);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('visits');
	}

}
