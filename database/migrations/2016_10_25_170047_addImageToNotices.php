<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImageToNotices extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('notices', function(Blueprint $table)
		{
			$table->string('image');
			$table->text('description');
			$table->string('slug')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('notices', function(Blueprint $table)
		{
			$table->dropColumn('image');
			$table->dropColumn('description');
			$table->dropColumn('slug');
		});
	}

}
