<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\Model;
use NescafeAlegria\Cycle;

class RemoveDuplicatesBmbNumber extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		$clients = Cycle::where('bmb_number', '=', 0)->get();
		foreach ($clients as $client) {
			$client->bmb_number = uniqid('g', false);
			$client->save();
		}

		Schema::table('cycles', function(Blueprint $table)
		{
			$table->unique('bmb_number');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cycles', function(Blueprint $table)
		{
			$table->dropUnique('cycles_bmb_number_unique');
		});

		$clients = Cycle::where('bmb_number', 'LIKE', '%g%')->get();
		foreach ($clients as $client) {
			$client->bmb_number = 0;
			$client->save();
		}
	}

}
