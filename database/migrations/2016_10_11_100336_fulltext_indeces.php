<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FulltextIndeces extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		DB::statement('ALTER TABLE machines ADD FULLTEXT search(name, description)');
		DB::statement('ALTER TABLE businesses ADD FULLTEXT search(name, description)');
		DB::statement('ALTER TABLE beverages ADD FULLTEXT search(name, description)');
		DB::statement('ALTER TABLE products ADD FULLTEXT search(name, description)');
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('products', function($table) {
			$table->dropIndex('search');
		});
		Schema::table('beverages', function($table) {
			$table->dropIndex('search');
		});
		Schema::table('businesses', function($table) {
			$table->dropIndex('search');
		});
		Schema::table('machines', function($table) {
			$table->dropIndex('search');
		});
	}
}
