<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBmbNumberType extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cycles', function(Blueprint $table)
		{
			$table->string('bmb_number', 14)->change();
		});
		Schema::table('visits', function(Blueprint $table)
		{
			$table->string('bmb_number', 14)->change();
		});

		Schema::table('verifications', function(Blueprint $table)
		{
			$table->string('bmb_number', 14)->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cycles', function(Blueprint $table)
		{
			$table->integer('bmb_number')->change();
		});

		Schema::table('visits', function(Blueprint $table)
		{
			$table->integer('bmb_number')->change();
		});

		Schema::table('verifications', function(Blueprint $table)
		{
			$table->integer('bmb_number')->change();
		});
	}

}
