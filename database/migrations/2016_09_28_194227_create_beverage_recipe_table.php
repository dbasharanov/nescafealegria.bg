<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeverageRecipeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('beverage_recipe', function(Blueprint $table)
		{
			$table->integer('beverage_id')->unsigned()->index();
	    $table->foreign('beverage_id')->references('id')->on('beverages')->onDelete('cascade');
	    $table->integer('recipe_id')->unsigned()->index();
	    $table->foreign('recipe_id')->references('id')->on('recipes')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('beverage_recipe');
	}

}
