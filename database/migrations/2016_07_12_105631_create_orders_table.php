<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('poc_responsible_name');
			$table->string('poc_name');
			$table->string('poc_code');
			$table->string('market');
			$table->string('year_month');
			$table->string('operator_name');
			$table->string('city');
			$table->string('address');
			$table->string('channel_level_3_name');
			$table->string('poc_group_name');
			$table->integer('bmb_number');
			$table->string('machine_sn');
			$table->string('machine_model');
			$table->string('machine_status');
			$table->string('recipe_name');
			$table->integer('days');
			$table->integer('cups_served');
			$table->integer('cups_per_day');
			$table->date('calendar_date');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
