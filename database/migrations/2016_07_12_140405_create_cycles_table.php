<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCyclesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cycles', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('operator_name');
			$table->string('poc_responsible_name');
			$table->string('poc_code');
			$table->string('poc_name');
			$table->string('contact_name')->nullable();
			$table->string('address');
			$table->string('city');
			$table->string('location');
			$table->string('phone')->nullable();
			$table->string('location_hint')->nullable();
			$table->integer('bmb_number');
			$table->string('machine_sn');
			$table->string('machine_model');
			$table->string('operator_code')->nullable();
			$table->date('install_date');
			$table->string('trading_person');
			$table->date('start_date')->default('0000-00-00');
			$table->integer('period')->default(0);
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cycles');
	}

}
