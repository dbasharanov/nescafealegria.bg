<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetaDescription extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('beverages', function(Blueprint $table)
		{
			$table->string('meta_description');
		});
		Schema::table('businesses', function(Blueprint $table)
		{
			$table->string('meta_description');
		});
		Schema::table('products', function(Blueprint $table)
		{
			$table->string('meta_description');
		});
		Schema::table('machines', function(Blueprint $table)
		{
			$table->string('meta_description');
		});
		Schema::table('notices', function(Blueprint $table)
		{
			$table->string('meta_description');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('beverages', function(Blueprint $table)
		{
			$table->dropColumn('meta_description');
		});
		Schema::table('businesses', function(Blueprint $table)
		{
			$table->dropColumn('meta_description');
		});
		Schema::table('products', function(Blueprint $table)
		{
			$table->dropColumn('meta_description');
		});
		Schema::table('machines', function(Blueprint $table)
		{
			$table->dropColumn('meta_description');
		});
		Schema::table('notices', function(Blueprint $table)
		{
			$table->dropColumn('meta_description');
		});
	}

}
