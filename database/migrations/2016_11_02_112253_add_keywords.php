<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddKeywords extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('beverages', function(Blueprint $table)
		{
			$table->string('keywords');
		});
		Schema::table('businesses', function(Blueprint $table)
		{
			$table->string('keywords');
		});
		Schema::table('products', function(Blueprint $table)
		{
			$table->string('keywords');
		});
		Schema::table('machines', function(Blueprint $table)
		{
			$table->string('keywords');
		});
		Schema::table('notices', function(Blueprint $table)
		{
			$table->string('keywords');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('beverages', function(Blueprint $table)
		{
			$table->dropColumn('keywords');
		});
		Schema::table('businesses', function(Blueprint $table)
		{
			$table->dropColumn('keywords');
		});
		Schema::table('products', function(Blueprint $table)
		{
			$table->dropColumn('keywords');
		});
		Schema::table('machines', function(Blueprint $table)
		{
			$table->dropColumn('keywords');
		});
		Schema::table('notices', function(Blueprint $table)
		{
			$table->dropColumn('keywords');
		});
	}

}
