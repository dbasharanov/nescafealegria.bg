<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDefaultMarketCodeToConsumers extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('consumers', function(Blueprint $table)
		{
			$table->string('market_code')->default('10301')->change();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('consumers', function(Blueprint $table)
		{
			$table->string('market_code')->default(NULL)->change();
		});
	}

}
