<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NewOrdersColumns extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropColumn('poc_group_name');
			$table->dropColumn('cups_per_day');
		});
		Schema::table('orders', function(Blueprint $table)
		{
			$table->string('ofu_name')->nullable()->default(null);
			$table->string('machine_config_code');
			$table->string('cups_per_day');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('orders', function(Blueprint $table)
		{
			$table->dropColumn('ofu_name');
			$table->dropColumn('cups_per_day');
			$table->dropColumn('machine_config_code');
		});
		Schema::table('orders', function(Blueprint $table)
		{
			$table->string('poc_group_name');
			$table->integer('cups_per_day');
		});
	}

}
