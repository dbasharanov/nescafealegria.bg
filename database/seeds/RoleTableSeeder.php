<?php 

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Kodeine\Acl\Models\Eloquent\Role;

class RoleTableSeeder extends Seeder {

	public function run()
	{
		$admin = new Role;
		$admin->name = 'Administrator';
		$admin->slug = 'administrator';
		$admin->description = 'full access';
		$admin->save();

		$employee = new Role;
		$employee->name = 'Employee';
		$employee->slug = 'employee';
		$employee->description = 'only reports';
		$employee->save();

		$employee = new Role;
		$employee->name = 'Third Party Employee';
		$employee->slug = 'third_party_employee';
		$employee->description = 'only reports for the third party';
		$employee->save();

		//$supervisor = new Role;
		//$supervisor->name = 'Supervisor';
		//$supervisor->slug = 'supervisor';
		//$supervisor->description = 'reports for all his employees';
		//$supervisor->save();
	}

}
