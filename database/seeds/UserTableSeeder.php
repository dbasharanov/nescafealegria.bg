<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use NescafeAlegria\User;
use Kodeine\Acl\Models\Eloquent\Role;

class UserTableSeeder extends Seeder {

	public function run()
	{
		$admin = new User;
		$admin->name = 'Admin';
		$admin->email = 'admin@example.com';
		$password = str_random(12);
		$admin->password = Hash::make($password);
		$admin->save();

		$adminRole = Role::where('slug', '=', 'administrator')->firstOrFail();

		$admin->assignRole($adminRole);

		$this->command->info('Admin user created');

		$this->command->info('email: admin@example.com');
		$this->command->info('password: ' . $password);
	}

}
