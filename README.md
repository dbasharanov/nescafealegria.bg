# Environment Setup

Install homestead as described on https://laravel.com/docs/5.2/homestead except the step when you install the vagrant box - this project requires php version 5.6
so we need to install older version of laravel homestead.

This is done with the command:

```
vagrant box add laravel/homestead --box-version 0.3.3
```

# Project Initialization

after Homestead is up and ready

clone the project:

```
git clone git@bitbucket.org:xeniumdevelopers/nescafe_alegria.git
```
Change Homestead.yml

Reprovision the homestead with that command:

```
vagrant provision
```
SSH to homestead with: vagrant ssh

run the following commands:

```
composer install
npm install
bower install
```

Run 'gulp' and if you want run "gulp --watch"
With that command gulp will watch for changes to assets and after every change it will recompile the assets.

# Project Setup
Copy .env_example to .env, and then enter the correct information.
You can get the database info from keepass.

# Cron tabs
Place this in crontab -e

```
* * * * * php /path/to/artisan schedule:run >> /dev/null 2>&1
```

# Deployment

Install rocketeer
```
$ vagrant ssh
$ wget http://rocketeer.autopergamene.eu/versions/rocketeer.phar
$ chmod +x rocketeer.phar
$ mv rocketeer.phar /usr/local/bin/rocketeer
```

Go to project root and deploy
```
rocketeer deploy
```