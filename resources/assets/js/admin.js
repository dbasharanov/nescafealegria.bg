if (self == top) {
  document.documentElement.style.display = 'block';
} else {
  top.location = self.location;
}

$.ajaxSetup({headers: {
  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}});

$(document).ready(function() {
  $(document).foundation();

  $('.visited-checkbox').change(function(e) {
    $(this).attr('disabled', 'disabled');
    var _this = $(this);
    $.post(_this.data('url'), function(data) {

    }).fail(function(data) {
      _this.prop('checked', !_this.prop('checked'));
    }).always(function(data) {
      _this.removeAttr('disabled');
    });
  });

  $('.with-datepicker').fdatepicker({
    format: 'dd.mm.yyyy',
    language: 'bg',
    weekStart: 1,
    onRender: function (date) {
      from_date = new Date(this.element.data('fromDate'));
      to_date = new Date(this.element.data('toDate'));
      return (typeof from_date !== 'undefined' && date.valueOf() < from_date.valueOf()) ||
             (typeof to_date !== 'undefined' && date.valueOf() > to_date.valueOf()) ? 'disabled' : '';
    }
  });

  $('#role').on('change', function() {
    if($(this).val() == 4) {
      $('#operator_container').show();
    } else {
      $('#operator_container').hide();
    }
  });

  $('.with-unlimited-datepicker').fdatepicker({
    format: 'dd.mm.yyyy',
    language: 'bg',
    weekStart: 1,
    onRender: function (date) {
      from_date = new Date(this.element.data('fromDate'));
      to_date = new Date(this.element.data('toDate'));
    }
  });

  $('#autosubmit input[type=file]').change(function() {
    var message = "Сигурни ли сте че искате да импортнете селектирания файл?";
    if($('#truncate').is(':checked')) {
      message = 'Всички редове от таблицата ще бъдат изтрити и след това ще се импортнат от файла.\nСигурни ли сте че искате да направите това?';
    }
    if (window.confirm(message)) {
      $('form#autosubmit').submit();
    };
  });

  $('#autosubmit').submit(function() {
    $('#spinner-container').css('display', 'flex');
    return true;
  });
});
