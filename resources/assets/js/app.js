if (self == top) {
  document.documentElement.style.display = 'block';
} else {
  top.location = self.location;
}

$(document).ready(function() {
  $(document).foundation();

  if(document.referrer.match(/nescafealegria/g)) {
    $('.arrow-back').attr('href', document.referrer);
  }
 if($('video').attr('id') == 'homepage-video') {
  document.getElementById('homepage-video').play();
 }
  //console.log(document.referrer);

  toggleMenu();
  calculator();
  autoFillLabel();

  $('.video-overlay').click(function () {
    src = "https://www.youtube.com/embed/sS1TYy3jb2A";
    $('#modalVideo iframe').attr('src', src);
  });
  $('#modalVideo button').click(function () {
    $('#modalVideo iframe').removeAttr('src');
    $('body').removeClass('is-reveal-open');
  });

  $('.search-trigger').click(function(){
    if ($('#query').val() != '') {
      $('.search-form').submit();
    } else {
      if ($('#mobile-query').val() != '') {
        $('.mobile-search-form').submit();
      }
      $('.search-form').toggle();
      $('#query').focus();
    }
  });


  $('span#beverages-tab').click(function() {
    $(this).show();
  });

  $('#beverages').click(function() {
    $('.specifications').addClass('hide');
    $('.beverages-list').removeClass('hide');
    $('.tab').removeClass('selected');
    $(this).addClass('selected');

    calculateNavPosition();
  });

  $('#specifications').click(function() {
    $('.specifications').removeClass('hide');
    $('.beverages-list').addClass('hide');
    $('.tab').removeClass('selected');
    $(this).addClass('selected');

    calculateNavPosition();
  });

  var mq = window.matchMedia( "(min-width: 1024px)" );
  if (mq.matches) {
    // window width is at least 500px
    $('.business-block').height(window.innerHeight);
  }

  footerHeight = $('footer').innerHeight();
  $('.off-canvas-wrap').css('padding-bottom', footerHeight);
  fixBottomNav();
  if($('.selected-box').is(':visible')){
    autoFillFormLabel();
  }
  var timeoutId;
  $('.benefit-container').bind('touchstart touchend', function(e) {
    if (!timeoutId) {
      timeoutId = window.setTimeout(function() {
        timeoutId = null; // EDIT: added this line
        e.preventDefault();
        $(this).child('p').css({opacity: 1});
      }, 1000);
    } else {
      window.clearTimeout(timeoutId);
      timeoutId = null;
      $(this).child('p').css({opacity: 0});
    }
  });

  $("#scroll-to-top").click(function() {
    $("html, body").animate({ scrollTop: 0 }, "slow");
    return false;
  });
});


  $('ul.business-types li').click(function() {
     updateCookie('nestle_cookie', [$(this).attr('id')], 0.08);
  });

function toggleMenu() {
  var container = $('.dropdown-menu');
  $(document).mouseup(function(e) {
    if ($('#menu-trigger').is(e.target)) {
      $('#menu-trigger').toggleClass('active');
      $('.dropdown-menu').toggle();
    }
  });
}

function calculator() {
  if( $('#step-1').length ) {
    step_1();
  }
  if( $('#step-2').length ) {
    step_2();
  }
  if( $('#step-3').length ) {
    step_3();
  }
}

function step_1() {
  value = getCookie('nestle_cookie').split(",");
  if (value != '') {
    $('#' + value[0]).addClass('selected');
  }
  $('ul.business-types li').click(function() {
    str = $( this ).text();
    id = $(this).attr('id')
    $('ul.business-types li').removeClass('selected');
    $(this).addClass('selected');
    $('.item.business').show().empty().html( str );
    $('#go-to-step-2').removeClass('disabled');

    updateCookie('nestle_cookie', [id], 0.08);
  });

}

function step_2() {

  value = getCookie('nestle_cookie').split(",");
  if (value != '') {
    if (value[0] == 'office') {
      $('#clients').empty().html( 'служители' );
    }
    if(!value[2]) {
      updateCookie('nestle_cookie', ['', '', '20 до 50'], 0.08);
    }
    $('.item.people').empty().show().html( $( '.checkbox span.checked' ).text() );
    employees = value[2] != 'над 50' ? 0 : 1 ;
    $('.checkbox span').removeClass('checked');
    $('#' + employees).find('span').addClass('checked');

    $('input#beverages').val(value[1]);
  }

  $('.checkbox').click(function() {
    $('.checkbox span').removeClass('checked');
    $(this).find('span').addClass('checked');
    var str = $( '.checkbox span.checked' ).text();
    $('.item.people').empty().show().html( str );
    $('#go-to-step-3').removeClass('disabled');

    updateCookie('nestle_cookie', ['', '', str], 0.08);
  });
}

function step_3() {
  $('.item.business').show();
  $('.item.cups').show();
  $('.item.people').show();
  $('#img-machine').show();
  $('#configurations-select').show();
  value = getCookie('nestle_cookie').split(",");
  val = value[1] ? value[1] : 30;
  configSlider(val);

  $('input[type=range]').bind( "mouseup touchend", function(e){
    value = $(this).val();
    configSlider(value);
  });
}


function configSlider(value) {
  sliderWidth = $('.slider-wrapper').width();
  document.getElementById('range').innerHTML=value;
  $('input[type=range]').val(value);
  updateCookie('nestle_cookie', ["", value, ""], 0.08);
  configMachine();
  $('#range').css( 'left', function() {
    percent = ((value - 20) / 80)
    if (percent == 1) {
      $('.item.cups').empty().show().html( 'над 100 чаши' );
      return sliderWidth - 24;
    } else {
      $('.item.cups').empty().show().html( value + ' ' + 'чаши' );
      return (sliderWidth * percent) - ((24*percent));
    }
  });
}

function configMachine() {
  cookie = getCookie('nestle_cookie').split(",");
  if (cookie != '') {
    busness = cookie[0];
    value = cookie[1];
  }
  switch (busness) {
    case 'petrol':
    case 'shop':
      if (value < 30) {
        number = 1;
        menu = 'G2';
      }
      if (value >= 30 && value < 100) {
        number = 2
        menu = 'G8';
      }
      if (value >= 100) {
        number = 3;
        menu = 'AE7';
      }
      break;
    case
     'hotel':
      if (value < 100) {
        number = 1
        menu = 'M2';
      } else {
        number = 4;
        menu = 'M3';
      }
      break;
    case
     'office':
      if (value < 30) {
        number = 1;
        menu = 'A1';
      }
      if (value >= 30 && value < 100) {
        number = 1
        menu = 'A13';
      }
      if (value >= 100) {
        number = 2;
        menu = 'G8';
      }
      break;
    case
     'coffeeshop':
      if (value < 30) {
        number = 1;
        menu = 'C26';
      } else {
        number = 2;
        menu = 'C9';
      }
      break;
   default:
      number = 2;
      menu = 'C9';
      break;
  }
  updateCookie('nestle_cookie', ["", "", "", number, menu], 0.08);
  //document.getElementById('img-machine').src = '/images/machines/' + number + '.png';
  if($("#configurations-select").length != 0) {
    document.getElementById('configurations-select').href = '/configuration/result/' + number + '?menu=' + menu;
  }
  autoFillLabel();
}


function autoFillLabel() {
  value = getCookie('nestle_cookie').split(",");
  if (value != '') {
    if(value[0] && value[1] && value[2]) {
      $('#no-cookie-box').hide();
      $('#configurations-select').show();
    }
    $('#cookie-box').show();
    var map = {'office': 'Офис','hotel': 'Хотел','coffeeshop': 'Кафетерия', 'shop': 'Магазин','petrol': 'Бензиностанция'}
    var names = {1: '6/30', 2: '8/60',3: '10/60', 4: '8/120'}
    $('.item.business').show().empty().html( map[value[0]] );
    $('.item.people').empty().html( value[2] );
    $('.item.cups').empty().html( value[1] + ' чаши');
    $("#machine-id input").val(value[3]);
    $("#recipe-name input").val(value[4]);
    $('#machine-name').text( 'NESCAFÉ Alegria ® ' + names[value[3]] );
    if($("#img-machine").length != 0) {
      document.getElementById('img-machine').src = '/images/machines/' + value[3] + '.png';
    }
  } else {
    $('#no-cookie-box').show();
  }

}
function autoFillFormLabel() {
  value = getCookie('nestle_cookie').split(",");
  if (value != '') {
    if (value[0] == 'office') {
      $('#employees label').empty().html( 'служители' );
    }
    $('input#beverages').val(value[1]);
    var map = {'office': 'Офис','hotel': 'Хотел','coffeeshop': 'Кафетерия', 'shop': 'Магазин','petrol': 'Бензиностанция'}
    employees = value[2] != 'над 50' ? 0 : 1 ;

    $('select#business_type').val(value[0]);
    $('select#employees').val(employees);
    $('.item.business').empty().show().html( map[value[0]] );
    cups = value[1] ? value[1] : '30';
    $('.item.cups').empty().show().html( cups + " чаши" );
    people = value[2] ? value[2] : '20 до 50';
    $('.item.people').empty().show().html( people );
    configMachine();
  }
   $('select#employees').change(function () {
    var str = '';
    $( 'select#employees option:selected' ).each(function() {
      str += $( this ).text() + " ";
      updateCookie('nestle_cookie', ["", "", str, ""], 0.08);
      configMachine();
    });
    $( ".item.people" ).text( str );
  });
  $('input#beverages').change(function () {
    var str = '';
    str += $('input#beverages').val() + " ";
    if (str == parseInt(str)) {
      $( ".item.cups" ).text(parseInt(str) + ' чаши');
      updateCookie('nestle_cookie', ["", str, "", ""], 0.08);
      configMachine();
    }
  });
   $('select#business_type').change(function () {
    var str = '';
    $( 'select#business_type option:selected' ).each(function() {
      str += $( this ).text() + " ";
      updateCookie('nestle_cookie', [$( 'select#business_type option:selected' ).val(), "", "", ""], 0.08);
      configMachine();
    });
    $( ".item.business" ).text( str );
  });
}

function getCookie(cname) {
  var name = cname + "=";
  var ca = document.cookie.split(';');
  for(var i=0; i<ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0)==' ') c = c.substring(1);
      if (c.indexOf(name) == 0) {
        encrypted = c.substring(name.length, c.length);
        encrypted = CryptoJS.AES.decrypt(encrypted, "alegria").toString(CryptoJS.enc.Utf8);
        return encrypted;
        //return c.substring(name.length, c.length);
    }
  }
  return "";
}

function updateCookie(cname,value,exdays) {
  cookie = getCookie('nestle_cookie').split(",");;
  for (i = 0; i < cookie.length; i++) {
    value[i] = value[i] ? value[i] : cookie[i]
  }
  var now = new Date();
  var time = now.getTime();
  time += exdays * 24 * 60* 60 * 1000;
  now.setTime(time);
  //console.log(value);
  encrypted = CryptoJS.AES.encrypt(value.toString(), "alegria");
  domainName = window.location.hostname.match(/nescafe-alegria.app/g) ? '.nescafe-alegria.app;' : '.nescafealegria.bg;'
  document.cookie = cname + '=' +encrypted + ';' +
                    'path=/;' +
                    'domain='+ domainName +
                    'expires=' + now.toUTCString();
}

function fixBottomNav() {

  // get initial position of the elements
  var footerHeight = $('footer').outerHeight();
  var documentHeight =  $(document).height();
  var windowHeight = $(window).height();

  if((documentHeight < windowHeight + footerHeight) && (documentHeight != windowHeight) ) {
    setBottomNavPosition(windowHeight + footerHeight - documentHeight);
  }
  if(documentHeight == windowHeight) {
    //setBottomNavPosition(footerHeight);
  }

  // assign scroll event listener
  $(window).scroll(function() {
    calculateNavPosition();
  });
}

function calculateNavPosition() {
  var scrollBottom = $(document).height() - $(window).height() - $(window).scrollTop();
  var mq = window.matchMedia( "(min-width: 1024px)" );
  if (mq.matches) {
    // get current position
    var currentScroll = $(window).scrollTop();
    // apply position: fixed if you
    if (scrollBottom <= footerHeight ) {
      setBottomNavPosition(footerHeight - scrollBottom)
    }
    else {
      setBottomNavPosition(0);
    }
  }
}

function setBottomNavPosition(position) {
  $('#bottom-nav').css({bottom: position + 'px'});
}