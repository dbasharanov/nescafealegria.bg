<?php

return [

	'Administrator' => 'Администратор',
	'Employee' => 'Служител',
	'Third Party Employee' => 'Служител оператор',
	'Supervisor' => 'Супервайзър',
	'administrator' => 'Администратор',
	'employee' => 'Служител',
	'third_party_employee' => 'Служител оператор',
	'supervisor' => 'Супервайзър',

];
