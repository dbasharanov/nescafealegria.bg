@extends('app')

@section('content')
    <div class='configuration'>
        <div class='row steps show-for-large'>
          <div class='small-2 large-4 columns step text-left'><span class='selected'></span>вид бизнес</div>
          <div class='small-4 large-4 columns step text-center'><span class='selected' id='second-step'></span>Брой клиенти/служители</div>
          <div class='small-6 large-4 columns step text-right'><span id='third-step'>3</span>брой консумирани чаши</div>
        </div>
        <div class='row'>
          <div class='small-12 large-7 columns'>
            <div id='step-2'>
              <h1>Кафе решение за Вашия бизнес</h1>
              <p class='heading'><span class='hide-for-small-only'>2.</span><span class='show-for-small-only'>2/3&nbsp;</span>Посочете броя на Вашите <span id='clients'>посетители</span></p>
              <div class='clearfix column checkbox'>
                <div id='0' class='row first-row'>
                  <div class='column medium-4 small-4 hide-for-small-only'>
                    <span class='round'></span>20 до 50
                  </div>
                  <div class='column medium-6 small-12 people-config text-center '></div>
                  <div class='column small-12 text-center show-for-small-only'>
                    <span class='round'>20 до 50</span>
                  </div>
                </div>
              </div>
              <div class='clearfix column checkbox'>
                <div id='1' class='row'>
                  <div class='column medium-4 small-4 hide-for-small-only'>
                    <span></span> над 50
                  </div>
                  <div class='column medium-6 small-12 people-config text-center '></div>
                  <div class='column small-12 text-center show-for-small-only'>
                    <span>над 50</span>
                  </div>
                </div>
              </div>
              <div class='column small-12 medium-10'>
                <div class='columns small-4 medium-3 float-left' style='margin: 1rem 0;'>
                  <a class='black-arrow-back' href='/configuration/step-1'>Назад</a>
                </div>
                <div class='columns small-4 medium-3 float-right button success arrow' >
                  <a href='/configuration/step-3' class='disabled go-to-next-step' id='go-to-step-3'>Напред&nbsp;&gt;</a>
                </div>
              </div>
            </div>
          </div>
          <div class='small-12 large-5 columns'>
            <div class='choice'>
              <p class='box-label'>Вашето решение</p>
              <div class='small-6 float-left column'>
                <div class='item business'>Офис</div>
                <div class='item people'>20 до 50</div>
                <div class='item cups hide'>30 чаши</div>
              </div>
              <div class='small-6 float-left column'>
                <img src='/images/machines/1.png' id='img-machine'>
              </div>
              <div class='small-7'>
                <!-- <a id='configurations-select' class='button primary' href='#'>ВИЖ ПОВЕЧЕ</a> -->
              </div>
            </div>
          </div>
        </div>
    </div>    
@endsection