@extends('app')

@section('content')
  <div class='machine-container'>
    <div class='row  align-justify'>
      <div class='columns small-12 medium-5 img-wraper large-order-1' style='padding-bottom:45px;'>
        <img src='/images/machines/{{$machine->id}}big.png'>
      </div>
      <div class='columns small-12 medium-5 large-order-2'>
          <div class='heading clearfix' style='margin-bottom:4rem;'>
            {{$machine->name}}
          </div>
          <div class='choice' style='height:auto;'>
            <p class='box-label'>Вашето решение</p>
            <div class='small-12 text-left float-left column'>
              <div class='item business show'>Офис</div>
              <div class='item people show'>20 до 50</div>
              <div class='item cups show'>30 чаши</div>
            </div>
          </div>
          <div class='small-12'>
            @include('partials._order_button')
          </div>
        <div class='row'></div>
      </div>
    </div>
  </div>
  @if($recipe)
  <div class='row column'>
    <div class='tabs-container'>
      <div class='tab selected' id='beverages'>Меню напитки</div>
      <div class='tab' id='specifications'>Спецификации</div>
    </div>
    <div class='beverages-list row small-up-2 medium-up-4 large-up-6 text-center' style='padding-top:2rem;'>
        @foreach($recipe->beverages()->orderBy('position', 'asc')->get() as $index => $beverage)
          <div class='column'>
              <a href='/beverages/{!! $beverage->getSlug() !!}'>
                <img style='height: 80px;padding:5px;' src='/images/attachments/beverages/{{$beverage->image}}' alt='{{$beverage->name}}'>
                <p>{!! $beverage->name !!}</p>
              </a>
          </div>
        @endforeach
    </div>
    <div class='machine-container specifications hide'>
      <div class='features'>
        <p>{!! $machine->summary !!}</p>      
      </div>
    </div>
  </div>
  @endif
@endsection