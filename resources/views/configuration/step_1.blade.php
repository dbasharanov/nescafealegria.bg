@extends('app')

@section('content')
    <div class='configuration'>
        <div class='row steps show-for-large'>
          <div class='small-2 large-4 columns step text-left'><span class='selected'></span>вид бизнес</div>
          <div class='small-4 large-4 columns step text-center'><span id='second-step'>2</span>Брой клиенти/служители</div>
          <div class='small-6 large-4 columns step text-right'><span id='third-step'>3</span>брой консумирани чаши</div>
        </div>
        <div class='row'>
          <div class='small-12 large-7 float-left'>
            <div class='medium-11 small-12' id='step-1'>
              <div class='small-12 column'>
                <h1>Кафе решение за Вашия бизнес</h1>
                <p class='heading'><span class='hide-for-small-only'>1.</span><span class='show-for-small-only'>1/3&nbsp;</span>Посочете своя бизнес</p>
              </div>
              <ul class='menu business-types text-center icon-top'>
                <li id='shop'>
                  <a href='javascript:;'>
                    <div class='icon-circle'>
                      <i class='na-icons-grocery-shop'></i>
                    </div>
                    Магазин
                  </a>
                </li>
                <li id='hotel'>
                  <a href='javascript:;'>
                    <div class='icon-circle'>
                      <i class='na-icons-building'></i>
                    </div>
                    Хотел
                  </a>
                </li>
                <li id='coffeeshop'>
                  <a href='javascript:;'>
                    <div class='icon-circle'>
                      <i class='na-icons-cup'></i>
                    </div>
                    Кафетерия
                  </a>
                </li>
                <li id='office'>
                  <a href='javascript:;'>
                    <div class='icon-circle'>
                      <i class='na-icons-office-chair'></i>
                    </div>
                    Офис
                  </a>
                </li>
                <li id='petrol'>
                  <a href='javascript:;'>
                    <div class='icon-circle'>
                      <i class='na-icons-gas-station'></i>
                    </div>  
                    Бензиностанция
                  </a>
                </li>
              </ul>
              <div class='column small-12'>
                <div class='small-4 medium-3 float-right button success arrow' >
                  <a href='/configuration/step-2' class='disabled go-to-next-step' id='go-to-step-2'>Напред&nbsp;&gt;</a>
                </div>
              </div>
            </div>
          </div>
          <div class='small-12 large-5 columns'>
            <div class='choice'>
              <p class='box-label'>Вашето решение</p>
              <div class='small-6 float-left column'>
                <div class='item business'>Офис</div>
                <div class='item people hide'>20 до 50</div>
                <div class='item cups hide'>30 чаши</div>
              </div>
              <div class='small-6 float-left column hide'>
                <img src='/images/machines/1.png' id='img-machine'>
              </div>
              <div class='small-7'>
                <!-- <a id='configurations-select' class='button primary' href='#'>ВИЖ ПОВЕЧЕ</a> -->
              </div>
            </div>
          </div>
        </div>
    </div>    
@endsection