@extends('app')

@section('content')
    <div class='configuration'>
        <div class='row steps show-for-large'>
          <div class='small-2 large-4 columns step text-left'><span class='selected'></span>вид бизнес</div>
          <div class='small-4 large-4 columns step text-center'><span class='selected' id='second-step'></span>Брой клиенти/служители</div>
          <div class='small-6 large-4 columns step text-right'><span class='selected' id='third-step'></span>брой консумирани чаши</div>
        </div>
        <div class='row'>
          <div class='small-12 large-7 columns'>
            <div id='step-3'>
              <h1>Кафе решение за Вашия бизнес</h1>
              <p class='heading'><span class='hide-for-small-only'>3.</span><span class='show-for-small-only'>3/3&nbsp;</span>Колко кафе напитки се консумират на ден?</p>
              <div class='row' style='margin-bottom:45px;'>
                <div class='small-2 medium-1 column text-center'><img src='/images/coffee.svg'>20</div>
                <div class='small-8 medium-9 column slider-wrapper'>
                  <input type='range'  min='20' max='100' value='30' />
                  <span id='range'>30</span>
                </div>
                <div class='small-2 medium-1 column text-center'><img src='/images/cups.svg'>над 100</div>
              </div>
              <div class='columns small-4 medium-3 float-left' style='margin: 1rem 0;'>
                <a class='black-arrow-back' href='/configuration/step-2'>Назад</a>
              </div>
              <div class='columns small-4 medium-3 float-right button success arrow' >
                <a id='configurations-select' href='#' class='go-to-next-step'>Напред&nbsp;&gt;</a>
              </div>
            </div>  
          </div>
          <div class='small-12 large-5 columns'>
            <div class='choice'>
              <p class='box-label'>Вашето решение</p>
              <div class='small-12 float-left column'>
                <div class='item business'>Офис</div>
                <div class='item people'>20 до 50</div>
                <div class='item cups'>30 чаши</div>
              </div>
            </div>
          </div>
        </div>
    </div>    
@endsection