@extends('admin')

@section('content')
	<div class='column'>
		<h1>
			График
		</h1>
		@if(count($schedules) == 0)
			<p>Няма насрочени посещения</p>
		@else
			<table class='stack hover'>
				<thead>
					<tr>
						<th>#</th>
						<th>Дата</th>
						<th>ТП</th>
						<th>Град</th>
						<th>Адрес</th>
						<th>Обект</th>
						<th>Кафе Машина</th>
						<th>BMB Номер</th>
						<th>Посетен?</th>
					</tr>
				</thead>
				<tbody>
					@foreach($schedules as $key => $schedule)
					<tr class='callout'>
						<td>{{$key+1}}</td>
						<td>{{$schedule->calendar_date}}</td>
						<td>
							{{$schedule->trading_person_name}}
						</td>
						<td>{{$schedule->location}}</td>
						<td>{{$schedule->address}}</td>
						<td>{{$schedule->poc_name}}</td>
						<td>{{$schedule->machine_model}} | {{$schedule->machine_sn}}</td>
						<td>{{$schedule->bmb_number}}</td>
						<td>
							<div class="switch float-left" style='margin-bottom: 0;'>
								<input {{($schedule->visited)? 'checked' : ''}} data-url='{{route('admin_rlAPkBI5RRPqmty1pVu4.schedules.toggle', $schedule->id)}}' class="switch-input visited-checkbox" id="visited-checkbox-{{$key}}" type="checkbox" value='1' autocomplete='false' name="visited">
								<label class="switch-paddle" for="visited-checkbox-{{$key}}">
    								<span class="switch-active" aria-hidden="true">Да</span>
    								<span class="switch-inactive" aria-hidden="true">Не</span>
  	  	  	  	  	  	  	    </label>
						    </div>
						</td>
					</tr>
				@endforeach
				</tbody>
				<tfoot>
					<tr>
						<th>#</th>
						<th>Дата</th>
						<th>ТП</th>
						<th>Град</th>
						<th>Адрес</th>
						<th>Обект</th>
						<th>Кафе Машина</th>
						<th>BMB Номер</th>
						<th>Посетен?</th>
					</tr>
				</tfoot>
			</table>
		@endif
		<div class='row column align-center text-center'>
			{!! $schedules->render() !!}
		</div>
	</div>
@endsection
