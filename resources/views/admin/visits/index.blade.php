@extends('admin')

@section('content')
	<div class='column'>
		<h1>
			Чекирани обекти
			@if(Auth::user()->isAdministrator())
				{!! Form::open(['route' => 'admin_rlAPkBI5RRPqmty1pVu4.verifications.import', 'files' => true, 'class' => 'float-right', 'id' => 'autosubmit']) !!}
				<div class='small button file-upload'>
					импортиране
					{!! Form::file('file', ['class' => 'file-input']) !!}
				</div>
				{!! Form::close() !!}
			@endif
		</h1>
	</div>
	<div class='column'>
		{!! Form::open(['route' => 'admin_rlAPkBI5RRPqmty1pVu4.visits.index', 'method' => 'get']) !!}
			{!! Form::text('from_date', $from_date->format('d.m.Y'), ['class' => 'filter small-12 medium-3 large-2 columns with-datepicker']) !!}
			{!! Form::text('to_date', $to_date->format('d.m.Y'), ['class' => 'filter small-12 medium-3 large-2 columns with-datepicker', 'data-to-date' => Carbon\Carbon::today()->toDateString()]) !!}
			{!! Form::select('user_id', $userFilters, $user_id, ['class' => 'filter small-12 medium-3 large-2 columns']) !!}
			{!! Form::submit('филтрирай', ['class' => 'filter button']) !!}
		<h4 style="padding-top: 0.4em;">посетени: {{$visited}} непосетени: {{$not_visited}}</h4>
		<div class='clearfix'></div>
		{!! Form::checkbox('only_not_visited', 1, $only_not_visited, ['id' => 'only_not_visited', 'autocomplete' => 'false', 'class' => '']) !!}
		{!! Form::label('only_not_visited', 'Показвай само непосетени', ['class' => '']) !!}
		{!! Form::close() !!}
	</div>
	@if(count($visits) == 0)
	@else
		<table class='stack hover'>
			<thead>
				<tr>
					<th>#</th>
					<th>Дата</th>
					<th>Период</th>
					<th>ТП</th>
					<th>Обект</th>
					<th>Кафе машина</th>
					<th>BMB номер</th>
					<th>Одитиран обект</th>
				</tr>
			</thead>
			<tbody>
			@foreach($visits as $i => $verification)
				<tr class='callout {{$verification->visited ? 'success' : ''}}'>
					<td>{{$i+1 }}</td>
					<td>{{$verification->calendar_date}}</td>
					<td>{{$verification->period}}</td>
					<td>{{$verification->trading_person_name}}</td>
					<td>{{$verification->poc_name}}</td>
					<td>{{$verification->machine_model}} | {{$verification->machine_sn}}</td>
					<td>{{$verification->bmb_number}}</td>
					<td>{!! $verification->visited ? 'ДА' : '<span style="color: red">НЕ</span>'!!}</td>
				</tr>
			@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Дата</th>
					<th>Период</th>
					<th>ТП</th>
					<th>Обект</th>
					<th>Кафе машина</th>
					<th>BMB номер</th>
					<th>Одитиран обект</th>
				</tr>
			</tfoot>
		</table>
	@endif
	<div class='row column align-center text-center'>
		{!! $visits->render() !!}
	</div>
@endsection
