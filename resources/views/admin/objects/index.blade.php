@extends('admin')
<?php $i = 1; ?>
@section('content')
	<div class='column'>
		<h1>
			Зачислени обекти
		</h1>
		<div class="row">
			<div class='column small-12 large-6'>
				{!! Form::open(['route' => 'admin_rlAPkBI5RRPqmty1pVu4.objects.index', 'method' => 'get']) !!}
					{!! Form::label('operator_name', 'Оператор') !!}
					{!! Form::select('operator_name', $operators, (array_key_exists('operator_name', $additional_filters)) ? $additional_filters['operator_name'] : '', ['class' => '']) !!}
					{!! Form::label('user_id', 'Търговски представител') !!}
					{!! Form::select('user_id', $userFilters, $user_id, ['class' => '']) !!}

					{!! Form::label('poc_name', 'Обект') !!}
					{!! Form::text('poc_name', (array_key_exists('poc_name', $additional_filters)) ? $additional_filters['poc_name'] : '', ['class' => '']) !!}

					{!! Form::label('poc_responsible_name', 'Фирма') !!}
					{!! Form::text('poc_responsible_name',(array_key_exists('poc_responsible_name', $additional_filters)) ? $additional_filters['poc_responsible_name'] : '', ['class' => '']) !!}

					{!! Form::label('bmb_number', 'BMB Номер/Сериен Номер') !!}
					{!! Form::text('bmb_number', (array_key_exists('bmb_number', $additional_filters)) ? $additional_filters['bmb_number'] : '', ['class' => '']) !!}

					{!! Form::submit('филтрирай', ['class' => 'button']) !!}
				{!! Form::close() !!}
				<div class='clearfix'></div>
			</div>
		</div>
		<table class='stack hover'>
			<thead>
				<tr>
					<th>#</th>
					<th>Дата</th>
					<th>Период</th>
					<th>Оператор</th>
					<th>ТП</th>
					<th>Град</th>
					<th>Адрес</th>
					<th>Обект</th>
					<th>Кафе Машина</th>
					<th>BMB Номер</th>
					<th>Корекция</th>
				</tr>
			</thead>
			<tbody>
			@foreach($cycles as $cycle)
				<tr>
					<td>{{ $i }}</td>
					<td>{{ $cycle->start_date }}</td>
					<td>{{ $cycle->period }}</td>
					<td>{{ $cycle->operator_name }}</td>
					<td>{{ $cycle->trading_person_name }}</td>
					<td>{{ $cycle->city }}</td>
					<td>{{ $cycle->address }}</td>
					<td>{{ $cycle->poc_name }}</td>
					<td>
					    {{$cycle->machine_model}} | {{$cycle->machine_sn}}
					</td>
					<td>{{ ($cycle->bmb_number) ? $cycle->bmb_number : 'Gourmet' }}</td>
					<td><a href='{{route('admin_rlAPkBI5RRPqmty1pVu4.own_cycles.edit', $cycle)}}' style='margin-right: 10px;' title="Промени"><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a></td>
				</tr>
				<?php $i++; ?>
			@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Дата</th>
					<th>Период</th>
					<th>Оператор</th>
					<th>ТП</th>
					<th>Град</th>
					<th>Адрес</th>
					<th>Обект</th>
					<th>Кафе Машина</th>
					<th>BMB Номер</th>
					<th>Корекция</th>
				</tr>
			</tfoot>
		</table>

	</div>
@endsection
