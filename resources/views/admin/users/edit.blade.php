@extends('admin')

@section('content')
	<div class='column'>
		<div class='row align-center'>
			<div class='small-12 medium-6 columns'>
				<h1>Промяна на потребител</h1>
				{!! Form::open(['route' => ['admin_rlAPkBI5RRPqmty1pVu4.users.update', $user], 'method' => 'put']) !!}
				@include('admin/users/_form')
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection
