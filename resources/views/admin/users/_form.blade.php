				<div class='row'>
					<div class='small-12 medium-4 large-3 columns'>
						{!! Form::label('name', 'Име', ['class' => 'inline']) !!}
					</div>
					<div class='small-12 medium-8 large-9 columns'>
						{!! Form::text('name', $user->name) !!}
					</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 large-3 columns'>
						{!! Form::label('email', 'E-mail', ['class' => 'inline']) !!}
					</div>
					<div class='small-12 medium-8 large-9 columns'>
						{!! Form::email('email', $user->email) !!}
					</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 large-3 columns'>
						{!! Form::label('password', 'Парола', ['class' => 'inline']) !!}
					</div>
					<div class='small-12 medium-8 large-9 columns'>
						{!! Form::password('password') !!}
					</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 large-3 columns'>
						{!! Form::label('role', 'Роля', ['class' => 'inline']) !!}
					</div>
					<div class='small-12 medium-8 large-9 columns'>
						{!! Form::select('role', $roles, $user->roles->isEmpty() ? 2 : $user->roles->first()->id) !!}
					</div>
				</div>
				<div id='operator_container' class='row' style='{{ !$user->roles->isEmpty() && $user->roles->first()->id == 4 ? '' : 'display: none'}}'>
					<div class='small-12 medium-4 large-3 columns'>
						{!! Form::label('operator', 'Оператор', ['class' => 'inline']) !!}
					</div>
					<div class='small-12 medium-8 large-9 columns'>
						<select name='operator' id='operator'>
							<option value='' {{$user->operator == null ? ' selected' : '' }}>--</option>
							@foreach($operators as $operator)
								<option {{$user->operator == $operator->operator_name ? 'selected' : ''}} value='{{$operator->operator_name}}' >{{$operator->operator_name}}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 large-3 columns'>
						{!! Form::label('user_id', 'Супервайзър', ['class' => 'inline']) !!}
					</div>
					<div class='small-12 medium-8 large-9 columns'>
						<select name='user_id' id='user_id'>
							<option value='0' {{$user->user_id == null ? ' selected' : '' }}>Няма</option>
							@foreach($possibleSupervisors as $supervisor)
								<option {{$user->user_id == $supervisor->id ? 'selected' : ''}} value='{{$supervisor->id}}' >{{$supervisor->name}}</option>
							@endforeach
						</select>
					</div>
				</div>

				<div class='row column'>
					{!! Form::submit('Изпрати', ['class' => 'primary button']) !!}
				</div>
			</div>
