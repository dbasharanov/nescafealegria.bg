@extends('admin')

@section('content')
	<div class='column'>
		<div class='row align-center'>
			<div class='small-12 medium-6 columns'>
				<h1>Промяна на парола</h1>
				{!! Form::open(['route' => ['admin_rlAPkBI5RRPqmty1pVu4.users.update_password']]) !!}
					<div class='row'>
						<div class='small-12 medium-6 large-4'>
							{!! Form::label('password' , 'Парола') !!}
						</div>
						<div class='small-12 medium-6 large-8'>
							{!! Form::password('password') !!}
						</div>
					</div>
					<div class='row'>
						<div class='small-12 medium-6 large-4'>
							{!! Form::label('password_confirmation', 'Потвърди паролата') !!}
						</div>
						<div class='small-12 medium-6 large-8'>
							{!! Form::password('password_confirmation') !!}
						</div>
					</div>
					<div class='row'>
						<div class='small-12 medium-6 large-4'>
							{!! Form::label('current_password', 'Текуща парола') !!}
						</div>
						<div class='small-12 medium-6 large-8'>
							{!! Form::password('current_password') !!}
						</div>
					<div class='row'>
					</div>
					<div class='row'>
						{!! Form::submit('Промени', ['class' => 'button']) !!}
					</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection
