@extends('admin')

@section('content')
	<div class='column'>
		<h1>Потребители <a class='float-right primary hollow button' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.users.create')}}' title="Добави потребител"><i class="fa fa-user-plus" aria-hidden="true"></i></a></h1>
		<table>
			<tr>
				<th>Име</th>
				<th>E-mail</th>
				<th>Роля</th>
				<th>Действия</th>
			</tr>
			@foreach($users as $user)
				<tr>
					<td>{{$user->name}}</td>
					<td>{{$user->email}}</td>

					<td>{{trans("misc.".$user->getRoles()[0]."")}}</td>
					<td>
						{!! Form::open(['route' => ['admin_rlAPkBI5RRPqmty1pVu4.users.destroy', $user], 'method' => 'delete', 'class' => 'float-right']) !!}
							<button type='submit' onclick='return confirm("Are you sure?");' title="Изтрий"><i class='fa fa-trash-o' aria-hidden='true'></i></button>
            			{!! Form::close() !!}
						<a href='{{route('admin_rlAPkBI5RRPqmty1pVu4.users.edit', $user)}}' class='float-right' style='margin-right: 10px;' title="Промени"><i class='fa fa-pencil-square-o' aria-hidden="true"></i></a>
					</td>
				</tr>
			@endforeach
		</table>
		<div class='row column align-center text-center'>
			{!! $users->render() !!}
		</div>
	</div>
@endsection
