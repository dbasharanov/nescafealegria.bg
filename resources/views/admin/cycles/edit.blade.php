@extends('admin')

@section('content')
	<div class='row align-center'>
		<div class='small-12 medium-10 column'>
			<h1>Промяна на цикъл</h1>
			{!! Form::model($cycle, ['route' => ['admin_rlAPkBI5RRPqmty1pVu4.cycles.update', $cycle->id], 'method' => 'PUT']) !!}
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('operator_name', 'Дистрибутор') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('operator_name', $cycle->operator_name) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('poc_responsible_name', 'име на клиент') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('poc_responsible_name', $cycle->poc_responsible_name) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('poc_code', 'poc_code') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('poc_code', $cycle->poc_code) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('poc_name', 'poc_name') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('poc_name', $cycle->poc_name) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('contact_name', 'Име за контакт') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('contact_name', $cycle->contact_name) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('address', 'Адрес') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('address', $cycle->address) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('city', 'Град') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('city', $cycle->city) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('geographical_location', 'геолокация') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('geographical_location', $cycle->geographical_location) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('phone', 'телефон') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('phone', $cycle->phone) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('location_hint', 'подсказка') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('location_hint', $cycle->location_hint) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('bmb_number', 'bmb номер') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('bmb_number', $cycle->bmb_number) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('machine_sn', 'Сериен номер машина') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('machine_sn', $cycle->machine_sn) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('machine_model', 'Модел машина') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('machine_model', $cycle->machine_model) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('operator_code', 'Код на оператор') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('operator_code', $cycle->operator_code) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('install_date', 'Дата на инсталиране') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('install_date', $cycle->install_date) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('trading_person_name', 'ТП') !!}</div>
					<div class='small-12 medium-8 columns'>
					<select name='trading_person_name' id='trading_person_name'>
						@foreach($userFilters as $u)
							<option value='{{$u}}' {{ $cycle->trading_person_name == $u ? 'selected' : ''}}>{{$u}}</option>
						@endforeach
					</select>
					</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('start_date', 'Дата на започнане') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('start_date', $cycle->start_date, ['class' => 'with-datepicker']) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('period', 'Цикъл(дни)') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::select('period', array('7' => 7, '14' => 14, '28' => 28, '168' => 168), $cycle->period) !!}</div>
				</div>
				{!! Form::submit('Запази', ['class' => 'button']) !!}
			{!! Form::close() !!}
		</div>
	</div>
@endsection
