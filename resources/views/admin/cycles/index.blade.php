@extends('admin')
<?php $i = 1; ?>
@section('content')
	<div class='column'>
		<h1>
			Цикли на посещения <a class='primary hollow button' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.cycles.create')}}'>Gourmet обект <i class="fa fa-plus" aria-hidden="true"></i></a>
			@if(Auth::user()->isAdministrator())
				<div class='small-12 medium-12 large-8 columns float-right'>
					<a  class='float-right small secondary button' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.cycles.export')}}'>експортиране</a>
					{!! Form::open(['route' => 'admin_rlAPkBI5RRPqmty1pVu4.cycles.import', 'files' => true, 'class' => 'float-right', 'id' => 'autosubmit']) !!}
						<div class='column float-left'>
							{!! Form::checkbox('truncate', 1, false, ['id' => 'truncate', 'autocomplete' => 'false']) !!}
							{!! Form::label('truncate', 'изтрий всички цикли при import') !!}
						</div>
						<div class='small button file-upload'>
							импортиране

							{!! Form::file('file', ['class' => 'file-input']) !!}
						</div>
					{!! Form::close() !!}
				</div>
			@endif
		</h1>
		<div class="row">
			<div class='column small-12 large-6'>
				{!! Form::open(['route' => 'admin_rlAPkBI5RRPqmty1pVu4.cycles.index', 'method' => 'get']) !!}
					{!! Form::text('from_date', $from_date->format('d.m.Y'), ['class' => 'with-unlimited-datepicker']) !!}
					{!! Form::text('to_date', $to_date->format('d.m.Y'), ['class' => 'with-unlimited-datepicker', 'data-to-date' => Carbon\Carbon::today()->toDateString()]) !!}
					{!! Form::label('operator_name', 'Оператор') !!}
					{!! Form::select('operator_name', $operators, (array_key_exists('operator_name', $additional_filters)) ? $additional_filters['operator_name'] : '', ['class' => '']) !!}
					{!! Form::label('user_id', 'Търговски представител') !!}
					{!! Form::select('user_id', $userFilters, $user_id, ['class' => '']) !!}

					{!! Form::label('poc_name', 'Обект') !!}
					{!! Form::text('poc_name', (array_key_exists('poc_name', $additional_filters)) ? $additional_filters['poc_name'] : '', ['class' => '']) !!}

					{!! Form::label('poc_responsible_name', 'Фирма') !!}
					{!! Form::text('poc_responsible_name',(array_key_exists('poc_responsible_name', $additional_filters)) ? $additional_filters['poc_responsible_name'] : '', ['class' => '']) !!}

					{!! Form::label('bmb_number', 'BMB Номер/Сериен Номер') !!}
					{!! Form::text('bmb_number', (array_key_exists('bmb_number', $additional_filters)) ? $additional_filters['bmb_number'] : '', ['class' => '']) !!}

					{!! Form::checkbox('show_inactive', 1, $show_inactive, ['id' => 'show_inactive', 'autocomplete' => 'false', 'class' => '']) !!}
					{!! Form::label('show_inactive', 'Показвай само редове без период', ['class' => '']) !!}
					{!! Form::submit('филтрирай', ['class' => 'button']) !!}
				{!! Form::close() !!}
				<div class='clearfix'></div>
			</div>
			<div class='column small-12 large-6'>
				<p>
					<ul style="list-style-type: none;">
						@if(!empty($summary))
							@foreach($summary as $date => $count)
								@if($count > 0)
									<li><h3>{{ $date }}: {{ $count }} посещения </h3></li>
								@endif
							@endforeach
						@endif
					</ul>
				</p>
			</div>
		</div>
		<table class='stack hover'>
			<thead>
				<tr>
					<th>#</th>
					<th>Дата</th>
					<th>Период</th>
					<th>Оператор</th>
					<th>ТП</th>
					<th>Град</th>
					<th>Адрес</th>
					<th>Обект</th>
					<th>Кафе Машина</th>
					<th>BMB Номер</th>
					<th>Корекция</th>
				</tr>
			</thead>
			<tbody>
			@foreach($cycles as $cycle)
				<tr>
					<td>{{ $i }}</td>
					<td>{{ $cycle->start_date }}</td>
					<td>{{ $cycle->period }}</td>
					<td>{{ $cycle->operator_name }}</td>
					<td>{{ $cycle->trading_person_name }}</td>
					<td>{{ $cycle->city }}</td>
					<td>{{ $cycle->address }}</td>
					<td>{{ $cycle->poc_name }}</td>
					<td>
					    {{$cycle->machine_model}} | {{$cycle->machine_sn}}
					</td>
					<td>{{ ($cycle->bmb_number) ? $cycle->bmb_number : 'Gourmet' }}</td>
					<td class='text-right' style='font-size: 1.2rem;'>
					@if(Auth::user()->isSupervisor())
						<a href='{{route('admin_rlAPkBI5RRPqmty1pVu4.cycles.disable', $cycle)}}' style='margin-right: 10px;' title="Премахни от цикъла на ТП"><i class='fa fa-minus-square-o' aria-hidden='true'></i></a>
					@endif
					@if(Auth::user()->isAdministrator())
						<a href='{{route('admin_rlAPkBI5RRPqmty1pVu4.cycles.edit', $cycle)}}' style='margin-right: 10px;' title="Промени"><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
						{!! Form::open(['method' => 'DELETE', 'route' => ['admin_rlAPkBI5RRPqmty1pVu4.cycles.destroy', $cycle->id], 'style' => 'display: inline;']) !!}
							<button type='submit' onclick='return confirm("Are you sure?");' title="Изтрий"><i class='fa fa-trash-o' aria-hidden='true'></i></button>
						{!! Form::close() !!}
					@else
						<a href='{{route('admin_rlAPkBI5RRPqmty1pVu4.own_cycles.edit', $cycle)}}' style='margin-right: 10px;' title="Промени"><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
						@if(!$cycle->bmb_number)
							{!! Form::open(['method' => 'DELETE', 'route' => ['admin_rlAPkBI5RRPqmty1pVu4.cycles.destroy', $cycle->id], 'style' => 'display: inline;']) !!}
								<button type='submit' onclick='return confirm("Are you sure?");' title="Изтрий"><i class='fa fa-trash-o' aria-hidden='true'></i></button>
							{!! Form::close() !!}
						@endif
					@endif
					</td>
				</tr>
				<?php $i++; ?>
			@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Дата</th>
					<th>Период</th>
					<th>Оператор</th>
					<th>ТП</th>
					<th>Град</th>
					<th>Адрес</th>
					<th>Обект</th>
					<th>Кафе Машина</th>
					<th>BMB Номер</th>
					<th>Корекция</th>
				</tr>
			</tfoot>
		</table>

	</div>
@endsection
