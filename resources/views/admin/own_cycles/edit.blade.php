@extends('admin')

@section('content')
	<div class='row align-center'>
		<div class='small-12 medium-10 column'>
			<h1>Промяна на цикъл</h1>
			{!! Form::model($cycle, ['route' => ['admin_rlAPkBI5RRPqmty1pVu4.own_cycles.update', $cycle->id], 'method' => 'PUT']) !!}
				@if($cycle->bmb_number == 0)
					<div class='row'>
						<div class='small-12 medium-4 columns'>{!! Form::label('poc_name', 'Фирма') !!}</div>
						<div class='small-12 medium-8 columns'>{!! Form::text('poc_name', $cycle->poc_name) !!}</div>
					</div>
					<div class='row'>
						<div class='small-12 medium-4 columns'>{!! Form::label('address', 'Адрес') !!}</div>
						<div class='small-12 medium-8 columns'>{!! Form::text('address', $cycle->address) !!}</div>
					</div>
					<div class='row'>
						<div class='small-12 medium-4 columns'>{!! Form::label('city', 'Град') !!}</div>
						<div class='small-12 medium-8 columns'>{!! Form::text('city', $cycle->city) !!}</div>
					</div>
				@endif
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('trading_person_name', 'ТП') !!}</div>
					<div class='small-12 medium-8 columns'>
					<select name='trading_person_name' id='trading_person_name'>
						@foreach($userFilters as $u)
							<option value='{{$u}}' {{ $cycle->trading_person_name == $u ? 'selected' : ''}}>{{$u}}</option>
						@endforeach
					</select>
					</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('start_date', 'Дата на започнане') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::text('start_date', $cycle->start_date , ['class'=> 'with-datepicker', 'data-from-date' => Carbon\Carbon::today()->toDateString()]) !!}</div>
				</div>
				<div class='row'>
					<div class='small-12 medium-4 columns'>{!! Form::label('period', 'Цикъл(дни)') !!}</div>
					<div class='small-12 medium-8 columns'>{!! Form::select('period', array('7' => 7, '14' => 14, '28' => 28, '168' => 168), $cycle->period) !!}</div>
				</div>
				{!! Form::submit('Запази', ['class' => 'button']) !!}
			{!! Form::close() !!}
		</div>
	</div>
@endsection
