@extends('admin')
<?php $i = 1; ?>
@section('content')
	<div class='column'>
		<h1>
			Цикли на посещения
		</h1>
		<table class='stack hover'>
			<thead>
				<tr>
					<th>#</th>
					<th>Дата</th>
					<th>Цикъл</th>
					<th>ТП</th>
					<th>Обект</th>
					<th>Кафе Машина</th>
					<th>Корекция</th>
				</tr>
			</thead>
			<tbody>
			@foreach($cycles as $cycle)
				<tr>
					<td>{{ $i }}</td>
					<td>{{ $cycle->start_date }}</td>
					<td>{{ $cycle->period }}</td>
					<td>{{ $cycle->trading_person_name }}</td>
					<td>
						{{ $cycle->poc_name }} <br>
						{{ $cycle->address }} <br>
					</td>
					<td>
						{{$cycle->bmb_number}}, {{$cycle->machine_sn}} ({{$cycle->machine_model}})
					</td>
					<td class='text-right' style='font-size: 1.2rem;'>
						<a href='{{route('admin_rlAPkBI5RRPqmty1pVu4.own_cycles.edit', $cycle)}}' style='margin-right: 10px;'><i class='fa fa-pencil-square-o' aria-hidden='true'></i></a>
					</td>
				</tr>
				<?php $i++; ?>
			@endforeach
			</tbody>
			<tfoot>
				<tr>
					<th>#</th>
					<th>Дата</th>
					<th>Цикъл</th>
					<th>ТП</th>
					<th>Обект</th>
					<th>Кафе Машина</th>
					<th>Корекция</th>
				</tr>
			</tfoot>
		</table>
		<div class='row column align-center text-center'>
			{!! $cycles->render() !!}
		</div>
	</div>
@endsection
