@extends('admin')

@section('content')
	<div class='column'>
		<h1>
			Продажби
			<div class='small-12 medium-12 large-8 columns float-right'>
				@if(Auth::user()->isAdministrator())
					<a  class='float-right small secondary button' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.orders.export')}}'>експортиране</a>
					{!! Form::open(['route' => 'admin_rlAPkBI5RRPqmty1pVu4.orders.import', 'files' => true, 'class' => 'float-right', 'id' => 'autosubmit']) !!}
					<div class='column float-left'>
						{!! Form::checkbox('truncate', 1, false, ['id' => 'truncate', 'autocomplete' => 'false']) !!}
						{!! Form::label('truncate', 'изтрий всички поръчки при import') !!}
					</div>
					<div class='small button file-upload'>
						импортиране
						{!! Form::file('file', ['class' => 'file-input']) !!}
					</div>
					{!! Form::close() !!}
				@endif
			</div>
		</h1>
	</div>
	<div class='clearfix'></div>
	<div class='column'>
		<div class='row align-center'>
			<div class='callout small-12 medium-12 large-8 columns'>
				{!! Form::open(['method' => 'get']) !!}
					@if(Request::get('start_date') != null || Request::get('start_date') != null && is_empty($statistics))
						<p class='alert'>Няма намерени резултати за зададените критерии!</p>
					@else
						<p><i>Моля попълнете начална и крайна дата</i></p>
					@endif
					<div class='row'>
						<div class='small-12 medium-2 columns'>
							{!! Form::label('from_date', 'От', ['class' => 'middle']) !!}
						</div>
						<div class='small-12 medium-4 columns'>
							{!! Form::text('from_date', Request::get('from_date'), ['class' => 'with-datepicker', 'placeholder' => 'dd.mm.YYYY']) !!}
						</div>
						<div class='small-12 medium-2 columns'>
							{!! Form::label('to_date', 'До', ['class' => 'middle']) !!}
						</div>
						<div class='small-12 medium-4 columns'>
							{!! Form::text('to_date', Request::get('to_date'), ['class' => 'with-datepicker', 'placeholder' => 'dd.mm.YYYY']) !!}
						</div>
					</div>
					<div class='row'>
						<div class='small-12 medium-2 columns'>
							{!! Form::label('bmb_number', 'BMB номер', ['class' => 'middle']) !!}
						</div>
						<div class='small-12 medium-10 columns'>
							{!! Form::text('bmb_number', Request::get('bmb_number')) !!}
						</div>
					</div>
					<div class='row'>
						<div class='small-12 medium-2 columns'>
							{!! Form::label('machine_sn', 'Сериен номер', ['class' => 'middle']) !!}
						</div>
						<div class='small-12 medium-10 columns'>
							{!! Form::text('machine_sn', Request::get('machine_sn')) !!}
						</div>
					</div>
					<div class='row'>
						<div class='small-12 medium-2 columns'>
							{!! Form::label('poc_name', 'Фирма', ['class' => 'middle']) !!}
						</div>
						<div class='small-12 medium-10 columns'>
							{!! Form::text('poc_name', Request::get('poc_name')) !!}
						</div>
					</div>
					<div class='row'>
						<div class='small-12  columns'>
							{!! Form::submit('Филтрирай', ['class' => 'button']) !!}
						</div>
					</div>
				{!! Form::close() !!}
			</div>
		</div>
		@foreach($statistics as $poc_name => $row1)
			@foreach($row1 as $bmb_number => $row2)

				@foreach($row2 as $machine_config_code => $row)
					<table class='stack hover' style='border: 1px solid #ccc !important'>
						<thead style='border-bottom: 1px solid #ccc'>
							<tr>
								<th width="450px">{{$poc_name}}</th>

								<th colspan="{{count($row['doi'])+1}}">
									{{$row['info']['city'] }} |
									{{$row['info']['address'] }} |
									{{$row['info']['machine_model']}} |
									BMB: {{$bmb_number}} |
									СН: {{$row['info']['machine_sn']}} |
									Рецепта: {{$machine_config_code}}
								</th>
							</tr>
							<tr>
								<th><b>Напитки</b></th>
								@foreach($row['doi'] as $key => $value)
									<th width="90px"><b>{{$key}}</b></th>
								@endforeach
								<th></th>
							</tr>
						</thead>

						@foreach($row['recipes'] as $recipe => $cups)
							<tr>
								<th style='border-right: 1px solid #ccc'>{{$recipe}}</th>
									@foreach($row['doi']  as $key => $value)
										<?php
										if(array_key_exists($recipe, $value))
											echo "<td>".number_format((float)$value[$recipe], 2)."</td>";
										else
											echo "<td>0.00</td>";
										?>
									@endforeach
								<td></td>
							</tr>
						@endforeach
						<tr style='border-top: 1px solid #ccc !important'>
							<th><b>Общо</b></th>
							@foreach($row['doi'] as $key => $value)
								<?php $sum = 0; ?>
								@foreach($value as $cups)
									<?php $sum = $sum + $cups; ?>
								@endforeach
								<td><b>{{number_format((float)$sum, 2)}}</b></td>
							@endforeach
							<td></td>
						</tr>
					</table>
				@endforeach
			@endforeach
		@endforeach
	</div>
@endsection