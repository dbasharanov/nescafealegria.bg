<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="<?php echo csrf_token() ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Nescafe Alegria</title>
	<link href="{{ elixir('css/admin.css') }}" rel="stylesheet">
</head>
<body>
	@if(Auth::user()->isAdministrator())
    	@include('partials.admin._top_bar_administrator')
    @else
    	@include('partials.admin._top_bar_employee')
    @endif
    <section id='content-section'>
    	@include('partials._message')
	    @yield('content')
	</section>
	<div id='spinner-container'>
		<div class="row align-middle align-center">
			<i class="fa fa-spinner" aria-hidden="true"></i>
			<h1 id='loading-text' style='margin-left: 10px;'>импортиране...</h1>
		</div>
	<div>
	<script type='text/javascript' src='{{ elixir('js/admin.js') }}'></script>
</body>
</html>
