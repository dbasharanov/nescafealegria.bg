<!DOCTYPE html>
<html lang="en">
<head>
  @include('partials._head')
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5VCG3X"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <div class="off-canvas-wrap" data-offcanvas>
    <div class="off-canvas-wrapper-inner">
      @include('partials._top_bar')
      <section id='content-section'>
        @include('partials._message')
        @yield('content')
      </section>
      <a class="exit-off-canvas"></a>
      @include('partials._bottom_menu')
    </div>
    @include('partials._footer')
    <script type='text/javascript' src='{{ elixir('js/all.js') }}'></script>
  </div>
</body>
</html>
