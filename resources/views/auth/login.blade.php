<html>
<head>
	<meta name='robots' content='noindex,nofollow'>
	<link href="{{ elixir('css/admin.css') }}" rel="stylesheet">
	<script type='text/javascript' src='{{ elixir('js/admin.js') }}'></script>

</head>
<body>
	<div style='background-color:#979a9c;height:100%;'>
		<div class="admin-login-panel row align-center text-center align-middle">
			<div class="small-12 medium-6 large-4 columns" style='display:table; margin: 0 auto;width:500px;'>
				<div class="">
					<h1 class='text-center logo'>
						<img src="/images/nescafe-alegria-logo.png" style='width:120px;'>
					</h1>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<strong>Грешка:</strong> възникна проблем с Вашите данни.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						<form role="form" method="POST" action="{{ url('/auth/login') }}">
							<input type="hidden" name="_token" value="{{ csrf_token() }}">
							<div class="row">
								<div class="small-12 medium-12 columns text-center">
									<input type="email" name="email" value="{{ old('email') }}" placeholder='Email' class='email'>
								</div>
							</div>
							<div class="row">
								<div class="small-12 medium-12 columns">
								<input type="password" name="password" placeholder='Password' class='password'>
								</div>
							</div>
							<div class='row align-justify align-middle'>
								<div class='small-6 columns float-left text-left' style='width: 50%;'>
									<button type="submit" class="primary button" style='font-size:16px; color:#fff;'>ВХОД</button>
								</div>
								<div class='small-6 columns text-right float-right' style='width: 50%;'>
									<input id='remember-me' type="checkbox" name="remember"><label for='remember-me'>Запомни ме</label>
								</div>
							</div>
						</form>
						<div class='help-text text-center'><a href="{{ url('/password/email') }}">Забравена парола ?</a></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
