@extends('app')

@section('content')
  <div class='plan'>
    <div class='row column'>
      <h1>NESCAFE план - социална отговорност</h1>
    </div>
    <div class='row'>
      <div class='small-12 large-3 text-center info-wrapper'>
        <div class='small-12 align-center'>
            <p style='display: inline-block; position: relative; margin-left: -20px; width:220px;' class='font-72'>Кафе<br>
            	<span style='width:210px;' class='roboto font-72 bold'>to go</span>
            	<!-- <span style='position: absolute; top: 5px; right: -25px;' class='ultra-thin question-mark'>?</span> -->
            </p>

        </div>
        <div class='small-12 align-center'>
          <img src='/images/plan/01.png'>
        </div>
      </div>
      <div class='small-12 medium-offset-2 large-offset-0 medium-8 large-6 align-center curly-brackets info-wrapper'>
            <p class='uppercase'>
		  	  <img style='height: 56px; margin-right: 5px;' class='float-left' src='/images/plan/cups.png'>
          	  <span class='roboto font-30'>Търсенето</span><br>
          	  <span class='roboto bold font-42 '>нараства</span><br>
        	</p>
        	<p class='uppercase'>
          	  <span class='roboto bold font-30'>Споделено <img id='stat' src='/images/plan/stat.png'></span><br>
          	  <span class='roboto bold font-24 '>предизвикателство как да увеличим реколтата на зелено кафе </span><br>
          	  <span class='font-20'>в държавите производителки</span><br>
        	</p>
      </div>
      <div class='small-12 large-3 info-wrapper'>
        <div class='small-12 text-center'>
          <img id='users' src='/images/plan/users.png'>
        </div>
        <div class='small-12 text-center'>
        	<p class='text-left' style='display: inline-block; position: relative; width: auto;'>
            	<span class='font-30'>Производството</span><br>
            	<span class='roboto bold font-20 '>на кафе трябва да се</span><br>
            	<span class='lobster downcase font-28'>управлява отговорно</span><br>
            	<span class='ultra-thin exclamation-mark'>!</span>
            </p>
        </div>
      </div>
    </div>
    <div class='row bordered font-28 bold text-center'>
      <div class='small-12 medium-1 align-self-top'>
        <img src='/images/plan/07.png'>
      </div>
      <div class='small-12 medium-10 text-center'>
        Като <span>кафето на хората</span> NESCAFÉ&reg; носи специална отговорност да подсигури достъпността на кафето със страхотен вкус за всички.
      </div>
      <div class='small-12 medium-1 align-self-bottom'>
        <img src='/images/plan/08.png'>
      </div>
    </div>
    <div id='plan-row-3' class='row'>
      <div style='margin-bottom: 3.125rem' class='small-12 large-6 small-align-centered large-align-left columns'>
        <div class='black-arrow uppercase'>
            <p>
            <span class='lobster font-30' style='text-transform:none;'>Инвестира ме</span><br>
            <span class='roboto bold font-46 float-left'>350 млн.</span>
            <span style='font-size: 14px;line-height: 18px;margin-top: 14px;'>швейцарски  <br>франка в</span><br><br>
            <span class='roboto bold font-26'>кафето</span><span class='roboto font-26 '> на бъдещето **</span>
            </p>

        </div>
      </div>
      <div class='small-12 large-6 columns'>
      	  <div class='row'>
            <div class='columns uppercase'>
        	  <p>
          	  	  <span class='roboto font-64 bold'>180 000</span><span class='font-30 bold'> тона</span>
          	  	  <span class='roboto font-36'>отговорно отгледано</span>
          	  	  <span class='roboto font-60'>Зелено кафе</span><br>
          	  	  <span class='font-28'>чрез връзка </span><span class='font-28 bold'><br>с фермерите *</span>
          	  </p>
        	</div>
        	<div class='shrink columns'>
          	  <img src='/images/plan/farmer.png'>
        	</div>
          </div>
      </div>
    </div>
    <div id='plan-row-4' class='row'>
      <div class='small-12 medium-3 text-center'>
        <img src='/images/plan/plant.png'>
      </div>
      <div class='small-12 medium-9'>
        <div class='row'>
        	<div class='text-right columns uppercase'>
          	  <p style='display: inline-block' class='text-left'>
          	  	  <span class='roboto font-52'>Научна дейност</span><br>
          	  	  <span class='roboto font-24'>Пътуващи научно-изследователски</span><br>
          	  	  <span class='roboto font-24 bold'>центрове</span>
          	  </p>
        	</div>
        	<div class='shrink columns'>
          	  <img id='lab' src='/images/plan/lab.png'>
        	</div>
        </div>
      </div>
    </div>
    <div id='plan-row-5' class='row'>
      	<div class='small-12 large-6 columns'>
      		<div class='row'>
        <div class='small-12 text-center medium-4 columns'>
          <img style='margin-bottom: 20px;' src='/images/plan/students.png'>
        </div>
        <div class='small-12 medium-8 columns uppercase text-center'>
          <p style='display: inline-block; text-align: left;'>
          <span class='font-36 bold'>Десетки хиляди</span><br>
          <span class='font-28'>ежедневно обучавани</span><br>
            <span class='roboto font-72 bold'>Фермери</span><br>
            <span class='roboto font-59'>и работници</span><br>
          </p>
        </div>
      		</div>
      	</div>
      <div class='small-12 medium-6 columns ' style='line-height: normal;'>
          <div class='black-arrow-2 uppercase'>
          	  <p>
          	  	  <span class='bold fond-32'>създаване на</span><br>
          	  	  <span class='lobster font-46 downcase'>гордост от</span><br>
          	  	  <span class='font-30'>фермерския труд</span><br>
          	  </p>
        </div>
      </div>
    </div>
    <div id='plan-row-6' class='row align-center'>
      <div class='row small-12 large-8 align-center'>
        <div class='small-12 medium-7 columns uppercase  medium-text-right text-center small-order-2 medium-order-1'>
          <p style='display:inline-block; text-align: right;'>
            <span class='lobster font-42' style='text-transform:none;'>Повишаване</span><br>
            <span class='font-42'>ефективността</span><br>
            <span class='font-42 bold'>и приходите</span><br>
            <span class='roboto font-36'>на производителите</span>
          </p>
        </div>
        <div class='small-12 medium-3 columns text-center small-order-1 medium-order-2'>
          <img id='speedometer' src='/images/plan/gear.png'>
        </div>
      </div>
      <div class='small-12 large-4 text-center cup-wrapper'>
        <img src='/images/plan/cup.png'>
      </div>
    </div>
    <div id='plan-row-7' class='row'>
      <div class='row small-12 medium-7'>
        <div class='medium-4 small-12 text-center align-self-middle'>
          <img src='/images/plan/small-plant.png'>
        </div>
        <div class='small-8 column uppercase text-left'>
          <p>
            <span class='font-52 bold'>Разпространяване на 220млн.</span><br>
            <span class='font-36'>високопродуктивни</span><br>
            <span class='font-60 bold'>растения **</span><br>
            <!-- <span class='lobster font-42 downcase'>доставени до</span> -->
          </p>
        </div>
      </div>
      <div class='small-12 medium-5' style='line-height: normal;'>
      </div>
    </div>
    <div id='plan-row-8' class='row'>
      <div class='small-12 large-5'>
        <div class='black-arrow-3 small-10 medium-9 uppercase'>
          <p>
            <span class='font-42 bold'>Локално</span><br>
            <span class='font-30'>въздействие,</span><br>
            <span class='lobster font-36 downcase'>свързани навсякъде</span><br>
          </p>
        </div>
      </div>
      <div class='row small-12 medium-11 large-7'>
        <div class='small-12 uppercase show-for-medium local-factory'>
          <p>
            <img src='images/plan/21.png'>
            <span class='font-36 bold' style='padding-left:10px;'>Местното </span><br>
            <span class='font-36 bold' style='padding-left:30px;'>производство,</span><br>
            <span class='lobster font-36 downcase' style='line-height:32px;'>създава</span>
            <span class='font-33'>работни места</span><br>
            <span class='font-36 bold'>нашите</span><span class='font-36'> фабрики</span>
          </p>
        </div>
        <div style='padding-left: 113px;' class='small-12 uppercase show-for-small-only local-factory'>
          <p>
            <img style='height: 95px' src='images/plan/21.png'>
            <span class='font-26 bold'>Местното </span><br>
            <span class='font-26 bold' style='padding-left:5px;'>производство,</span><br>
            <span class='lobster font-26 downcase' style='line-height:37px;'>създава</span>
            <span class='font-23'>работни места</span><br>
            <span class='font-26 bold'>нашите</span><span class='font-26'> фабрики</span>
          </p>
        </div>
      </div>
    </div>
    <div id='plan-row-9' class='row align-center medium-offset-5'>
      <div class='small-11 columns'>
        <div class='row small-12 uppercase'>
          <div class='lobster capitalize clearfix font-36 small-12'>Спестяваме **</div>
          <div class='shrink align-self-middle'><span class='lobster font-52 downcase'>и</span></div>
          <div class='columns'>
            <p>
              <span class='font-64 bold'>20%</span><span class='font-33 bold'> от енергията<img style='padding-left:20px;' src='/images/plan/23B.png'></span><br>
              <span class='font-64 bold'>30%</span><span class='font-33 bold'> от водата<img style='padding-left:20px;' src='/images/plan/23A.png'></span></div>
            </p>
        </div>
      </div>
    </div>
    <div id='plan-row-10' class='row'>
      <div class='small-12 text-center large-text-left medium-12 large-7'>
        <img style='padding-left:20px; padding-bottom: 8rem' src='/images/plan/berry.png'>
      </div>
      <div class='small-12 medium- 8 large-5 uppercase text-right'>
        <div class='row  medium-text-right text-center '>
          <div class='columns uppercase text-right'>
            <p class='uppercase'>
              <span class='capitalize font-60 downcase'>над 250</span><br>
              <span class='font-42 bold'>агрономи</span><br>
              <span class='font-36'>в повече от</span><br>
              <span class='lobster font-42 downcase'>10 държави</span>
            </p>
          </div>
          <div class='columns text-center shrink'>
            <img src='/images/plan/earth.png'>
          </div>
        </div>
      </div>
    </div>
    <div id='plan-row-11' class='row'>
      <div class='small-12 text-center large-text-right large-5 columns text-right'>
        <p class='uppercase' style='display: inline-block; text-align: right;'>
          <span class='roboto font-36'>достигане на повече</span><br>
          <span class='font-49 bold'>потребители</span><br>
          <span class='lobster font-49 downcase'>навсякъде:</span><br>
        </p>
      </div>
      <div class='small-12 large-7 columns'>
        <div class='black-arrow-4 text-right'>
           <p class='uppercase'>
              <span class='font-26'>разпространяване на</span><br>
              <span class='roboto font-36 bold'>устойчивостта</span><br>
           </p>
        </div>
      </div>
    </div>
    <div id='plan-row-12' class='row'>
      	<div class='small-12 columns row text-center large-text-left'>
      		<div style='display: inline-block'>
		  	  <div class='float-left' style='padding: 15px 10px 15px 0;'><img src='/images/plan/cup-2.png'></div>
          	  <span style='line-height: 0.8' class='roboto font-42 bold'>55 000&nbsp;</span>
          	  <div style='display: inline-block'>
          		<span class='roboto font-26' style='line-height: 0.8; border-bottom: 1px solid #e70302;'>чаши на</span><br>
          		<span style='line-height: 0.9;' class='lobster downcase font-26'>секунда</span>
          	  </div><br>
          	  <span class='font-33'>като броя се увеличава</span>
          	</div>
      </div>
    </div>
    <div id='plan-row-13' class='row' style='margin-bottom:90px;'>
      <div class='column align-self-middle text-center'>
          <img src='/images/plan/nescafe-cup.png' id='nescafe-cup'>
        </div>
    </div>
    <div class='row font-20' style='margin-bottom:90px;'>
      <div class='column small-12'>
        <p>*Нашият ангажимент за 2015г.</p>
      </div>
      <div class='column small-12'>
        <p>** Нашите ангажименти за периода 2010 – 2020г. </p>
      </div>
    </div>
  </div>
@endsection
