@extends('app')

@section('content')
	<div class='deliveries'>
	<div class='row'>
		<div class='small-12 columns'>
			<h1>Условия за доставка</h1>
		</div>
		<div class='small-12 medium-offset-1 medium-4 text-center medium-text-right align-right columns'>
			<img id='nescafe-delivery' src='/images/deliveries/nescafe-delivery.png'>
		</div>
		<div id='row-1' class='small-12 medium-7 small-text-center align-middle columns'>
			<p>
				<span class='font-60'>Създайте</span><br>
				<span class='roboto font-36'>решението за Вашия бизнес</span><br>
				<span class='font-26'>на сайта</span><br>
			</p>
		</div>
	</div>
	<div class='row'>
		<div class='small-12 medium-offset-1 text-center medium-text-right medium-10 columns'>
			<img id='nescafe-address' src='/images/deliveries/nescafe-address.png'>
		</div>
	</div>
	<div id='row-2' class='row'>
		<div class='small-12 medium-offset-1 text-center medium-text-right align-bottom medium-5 columns'>
			<p>
				<span class='roboto bold font-42'>Подайте заявка</span><br>
				<span class='font-36'>за кафемашина</span>
			</p>
		</div>
		<div class='small-12 medium-5 text-center medium-text-left align-middle columns'>
			<img id='nescafe-letter' src='/images/deliveries/nescafe-letter.png'>
		</div>
	</div>
	<div data-equalize-on='medium' id='bordered-row' class='row'>
		<div data-equalizer-watch style='text-align:middle' class='small-12 medium-offset-1 medium-2 text-center medium-text-left columns bordered top'>
			<img id='nescafe-24' src='/images/deliveries/nescafe-24.png'>
		</div>
		<div data-equalizer-watch class='small-12 medium-8 columns text-center medium-text-left bordered bottom'>
			<p>
			<span class='font-33'>Наш служител ще се свърже с Вас</span><br>
			<span class='bold font-52'>в рамките на 24 часа <br>(в работни дни),</span><br>
			<span class='roboto bold font-26'>за да уточните удобно време за посещение от наш представител.</span><br>
			</p>
		</div>
	</div>
	<div class='row' style='margin-bottom: 40px;'>
		<div class='small-12 text-center medium-text-left medium-order-2 medium-5 columns'>
			<img src='/images/deliveries/nescafe-lessons.png'>
		</div>
		<div class='small-12 medium-offset-1 medium-order-1 medium-6 columns'>
			<p>
			<span class='bold font-36'>В уточнения ден и час</span><br>
			<span class='font-36'>нашият представител ще направи презентация на предложението ни и</span><br>
			<span class='roboto bold font-30'>ще съгласувате всички условия по инсталиране на машина и покупка на продукти.</span><br>
			</p>
		</div>
	</div>
	<div id='row-3' class='row' style='padding-top: 230px; padding-bottom: 150px;'>
		<div class='small-12 medium-offset-1 medium-5 text-center medium-text-left columns'>
			<img id='nescafe-maintenance' src='/images/deliveries/nescafe-maintenance.png'>
		</div>
		<div class='small-12 medium-6 columns align-bottom'>
			<p>
			<span class='roboto bold font-49'>Инсталиране на</span><br>
			<span class='font-52'>машина от техник.</span><br>
			<span class='roboto bold font-46'>Настройка и обучение на персонала Ви.</span><br>
			</p>
		</div>
	</div>
	<div class='row' style='padding-bottom: 120px;'>
		<div class='small-12 medium-2 text-center medium-text-left medium-order-2 columns'>
			<img id='nescafe-delivery-2' src='/images/deliveries/nescafe-delivery-2.png'>
		</div>
		<div class='small-12 medium-offset-1 text-center medium-text-left medium-order-1 medium-7 columns'>
			<p>
			<span class='roboto bold font-49'>Доставка на продукти,</span><br>
			<span class='font-39'>съгласувана с монтажа на машината.</span><br>
			</p>
		</div>
	</div>
	<div class='row' style='padding-bottom: 150px;'>
		<div class='small-12 text-center columns'>
			<img style='margin-bottom: 30px;' src='/images/deliveries/nescafe-drop.png'>
			<p>
			<span class='font-49' style='text-transform: none'>Наслаждавате се</span><br>
			<span class='font-39'>на напитките</span><br>
			</p>
			<img style='margin-top: 30px;' src='images/deliveries/nescafe-alegria-logo.png'>
		</div>
	</div>
	 <div style='max-width: 250px;margin: auto;padding-bottom: 150px;'>
    <a class='secondary button' href='/order'>ЗАЯВИ КАФЕМАШИНА</a>
  </div>
</div>
@endsection
