@extends('app')

@section('content')
  <div style='max-width: 250px;margin: auto;'>
     <a class='show-for-small-only secondary large button' href='/order'>ЗАЯВИ КАФЕМАШИНА</a>
  </div>
	<div class='products'>
		<div class='row column'>
			<h1>Продукти за професионалните кафемашини</h1>
		</div>
		<div class='row small-up-1 medium-up-2'>
			@foreach($products as $index => $product)
				<div class='columns rectangle'>
					<div class='product-container text-center'>
						<a href='/products/{{ $product->getSlug() }}'>
							<img src='/images/attachments/products/{{$product->image}}'>
							<div class='description-container text-left'>
								<h2>{{$product->name}}</h2>
								<p>{{$product->summary}}</p>
							</div>
						</a>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection
