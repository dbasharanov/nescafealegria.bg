@extends('app')

@section('content')
    <div class='inner-wrapper product-container'>
        <div class='row' style='margin-bottom:80px'>
            <div class='columns small-12 medium-12 large-6 img-wraper large-order-2'>
                <img src='/images/attachments/products/{{$product->image}}'>
                <!--            
                <div class='show-for-small-only share column small-6 medium-4 float-left text-right'>
                    <a data-open='share-modal'><img src='/images/machines/share.svg'>Сподели</a>
                </div> 
                -->
                <div class='show-for-small-only share column small-6 medium-4 float-right text-right'>
                  @if($prev)
                    <a class='box prev wallpaper' href='/products/{{$prev->slug}}'></a>
                  @endif
                  @if($next)
                    <a class='box next wallpaper' href='/products/{{$next->slug}}'></a>
                  @endif
                </div>
                <a class='box close wallpaper show-for-small-only' href='/products'></a>
            </div>
            <div class='columns small-12 medium-12 large-6 float-left'>
                <nav class='breadcrumbs small-12 show-for-medium'>
                    <a class='current arrow-back' href='/products'>Назад</a>
                </nav>
                <div class='text-wrapper clearfix'>
                    <h1>{{$product->name}}</h1>
                    <p>{!! $product->description !!}</p>
                    <!-- 
                    <div class='show-for-medium share column small-6 medium-4 float-left text-right'>
                        <a data-open='share-modal'><img src='/images/machines/share.svg'>Сподели</a>
                    </div>
                    -->
                </div>  
                <div class='beverages-list'>
                    <h2>Напитки:</h2>
                    <div class='row small-up-2 medium-up-3 large-up-4 text-center'>
                        @foreach($product->beverages()->orderBy('position', 'asc')->get() as $index => $beverage)
                            @if($beverage->category_id < 4)
                            <div class='column'>
                                <a href='/beverages/{!! $beverage->getSlug() !!}'>
                                    <img src='/images/attachments/beverages/{{$beverage->image}}' alt='{{$beverage->name}}'>
                                    <p>{!! $beverage->name !!}</p>
                                </a>
                            </div>
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection