<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Nescafe Alegria</title>
  <link href="{{ elixir('css/app.css') }}" rel="stylesheet">
</head>
<body>
  <div class="fullpage-design off-canvas-wrap" data-offcanvas>
    <div class="off-canvas-wrapper-inner">
      @include('partials._top_bar')
      <section id='content-section'>
        @yield('content')
      </section>
      <a class="exit-off-canvas"></a>
    </div>
    @include('partials._bottom_menu')
    <script type='text/javascript' src='{{ elixir('js/all.js') }}'></script>
  </div>
</body>
</html>
