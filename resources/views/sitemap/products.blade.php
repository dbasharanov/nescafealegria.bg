<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    @foreach ($products as $product)
        <url>
            <loc>https://www.nescafealegria.bg/products/{{ $product->slug }}</loc>
            <lastmod>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $product->updated_at)->format('Y-m-d\TH:i:s+02:00')}}</lastmod>
            <image:image>
              <image:loc>https://www.nescafealegria.bg/images/attachments/products/{{$product->image}}</image:loc>
            </image:image>
            <changefreq>weekly</changefreq>
            <priority>0.6</priority>
        </url>
    @endforeach
</urlset>