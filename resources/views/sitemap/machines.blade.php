<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
        xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">
    @foreach ($machines as $machine)
        <url>
            <loc>https://www.nescafealegria.bg/machines/{{ $machine->slug }}</loc>
            <lastmod>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $machine->updated_at)->format('Y-m-d\TH:i:s+02:00')}}</lastmod>
            <image:image>
              <image:loc>https://www.nescafealegria.bg/images/attachments/machines/{{$machine->image}}</image:loc>
            </image:image>
            <changefreq>weekly</changefreq>
            <priority>0.6</priority>
        </url>
    @endforeach
</urlset>