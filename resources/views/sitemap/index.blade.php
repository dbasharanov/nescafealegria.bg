<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>https://www.nescafealegria.bg/sitemap/beverages</loc>
        <lastmod>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $beverage->updated_at)->format('Y-m-d\TH:i:s+02:00')}}</lastmod>
    </sitemap>
    <sitemap>
        <loc>https://www.nescafealegria.bg/sitemap/machines</loc>
        <lastmod>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $machine->updated_at)->format('Y-m-d\TH:i:s+02:00')}}</lastmod>
    </sitemap>
    <sitemap>

        <loc>https://www.nescafealegria.bg/sitemap/products</loc>
        <lastmod>{{ Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $product->updated_at)->format('Y-m-d\TH:i:s+02:00')}}</lastmod>
    </sitemap>
</sitemapindex>
