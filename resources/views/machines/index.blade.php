@extends('app')

@section('content')
  <div class='row column'>
    <h1>Професионални кафемашини</h1>
    <div class='row small-up-1 medium-up-2 large-up-4 machines-list' data-equalizer data-equalize-by-row='true'>
      @foreach($machines as $machine)
        <div class='column'>
          <div class='column small-5 medium-12 float-left wallpaper img-block' style='background-image:url(/images/attachments/machines/{{$machine->image}})'>
            <a href='/machines/{{$machine->slug}}'></a>
          </div>
          <div class='column small-7 medium-12 float-left'>
            <div class='font-21 strong clearfix'>
              <a href='/machines/{{$machine->slug}}' class='machine-name'>
                {{$machine->name}}
              </a>
            </div>
            <div>
              {{$machine->slogan}}
            </div>
            <a class='arrow' href='/machines/{{$machine->slug}}'>Научи повече</a>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection