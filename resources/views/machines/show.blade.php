@extends('app')

@section('content')
  <div class='machine-container'>
    <div class='row'>
      <div class='columns small-12 medium-5 img-wraper large-order-1'>
        <img src='/images/attachments/machines/{{$machine->image}}'>
        <div class='show-for-small-only share column small-6 medium-4 float-left text-right'>
            <a data-open='share-modal'><img src='/images/machines/share.svg'>Сподели</a>
        </div>
        <div class='show-for-small-only share column small-6 medium-4 float-right text-right'>
          @if($prev)
            <a class='box prev wallpaper' href='/machines/{{$prev->slug}}'></a>
          @endif
          @if($next)
            <a class='box next wallpaper' href='/machines/{{$next->slug}}'></a>
          @endif
        </div>
        <a class='box close wallpaper show-for-small-only' href='/machines'></a>
      </div>
      <div class='columns small-12 medium-7 large-order-2'>
        <a class='show-for-large arrow-back' href='/machines/'>Назад</a>
        <h1 class='heading clearfix'>
          {{$machine->name}}
        </h1>
        <div class='features'>
          <p>{{$machine->slogan}}</p>
          <p>{!! $machine->summary !!}</p>      
        </div>
        <div class='row'>
          <div class='btn column small-7 medium-4 float-left'>
            @include('partials._order_button')
          </div>
          <div class='show-for-large share column small-6 medium-4 float-left text-right'>
            <a data-open='share-modal'><img src='/images/machines/share.svg'>Сподели</a>
          </div>
        </div>
        @include('partials._share_modal', array('link' => NescafeHelper::get_url()))
      </div>
    </div>
    <div class='info small-12 clearfix'>
      <!-- {!! $machine->description !!} -->
    </div>
  </div>
@endsection