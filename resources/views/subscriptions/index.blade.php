@extends('app')

@section('content')
    <div class='row align-justify'>
        <div class='column small-12'>
            <h1>Отписване</h1>
            <div class='small-12 medium-6' >
                <p>Благодарим Ви за интереса към бюлетина на Nestlé Professional. В случай, че желаете да се отпишете - въведете Вашият email тук:</p>
            </div>
            {!! Form::open(array('route' => 'subscriptions.unsubscribe')) !!}
                <div class='small-12 medium-6' >
                    {!! Form::email('email', null, array('class' => 'newsletter-field', 'required', 'placeholder' => 'E-mail')) !!}
                </div>
                <div class='small-12 medium-6'>
                    {!! Form::submit('ИЗПРАТИ', array('class' => 'medium button')) !!}
                </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection