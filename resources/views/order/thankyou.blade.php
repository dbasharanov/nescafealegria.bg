@extends('app')

@section('content')
  <div class='row contact-form-wrapper'>
    <p><strong>Благодарим Ви за изпратената заявка!</strong><br> Наш служител ще се свърже с Вас в рамките на 24 часа (в работни дни), за да уточните удобно време за посещение от наш представител.</p>
  </div>
@endsection
