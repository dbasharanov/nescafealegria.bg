@extends('app')

@section('content')
  <div class='row contact-form-wrapper'>
    <div class='small-12 large-5 columns large-order-2'>
      <div class='choice selected-box' id='cookie-box'>
        <p class='box-label'>Вашето решение</p>
        <div class='small-12'>
          <p>
            <strong id='machine-name'>NESCAFÉ Alegria&nbsp;&reg;&nbsp;</strong>
          </p>
        </div>
        <div class='small-6 float-left column'>
          <div class='item business'>Офис</div>
          <div class='item people'>Над 50</div>
          <div class='item cups'>30 чаши</div>
        </div>
        <div class='small-6 float-left column'>
          <img src='/images/machines/1.png' id='img-machine'>
        </div>
        <a id='configurations-select' href='#' class='' target='blank'>Научи повече <img src='/images/new_tab.svg' width='15'></a>
      </div>
      <div class='choice selected-box' id='no-cookie-box'>
        <p>Ако желаете да изготвим предложение подходящо за Вашия бизнес, моля посочете основните му характеристики тук</p>
        <div class='small-12'>
          <a class='button primary' style='width:100%' href='/configuration/step-1' id='gtm-your solution-button'>РЕШЕНИЕ ЗА ВАС</a>
        </div>
      </div>
    </div>
    <div class='small-12 column'>
      <h1>Заяви професионална кафемашина</h1>
      <h2>Попълнете формата и ще се свържем с Вас.</h2>
    </div>
    <section id='form-section' class='small-12 large-7 columns large-order-1'>
      {!! Form::open(array('route' => 'order.store')) !!}
      <div class='row'>
        <div class='small-12 large-3 columns'>
          {!! HTML::decode(Form::label('first_name', 'Лице за контакт <span>*</span>')) !!}
        </div>
        <div class='small-12 large-4 columns'>
          {!! Form::text('first_name', null, array('required', 'placeholder' => 'Име')) !!}
        </div>
        <div class='small-12 large-5 columns'>
          {!! Form::text('last_name', null, array('required', 'placeholder' => 'Фамилия')) !!}
        </div>
      </div>
      <div class='row'>
        <div class='small-12 large-3 columns'>
          {!! HTML::decode(Form::label('phone_number', 'Данни за контакт  <span>*</span>')) !!}
        </div>
        <div class='small-2 large-1 columns'><label class='text-right middle'>+359</label></div>
        <div class='small-10 large-3 columns'>
          {!! Form::text('phone_number', null, array('required', 'placeholder' => 'Телефон')) !!}
        </div>
        <div class='small-12 large-5 columns'>
          {!! Form::email('email', null, array('required', 'placeholder' => 'E-mail')) !!}
        </div>
      </div>
      <div class='row'>
        <div class='small-12 large-3 columns'>
          {!! HTML::decode(Form::label('organization_name', 'Име на фирма <span>*</span>')) !!}
        </div>
        <div class='small-12 large-9 columns'>
          {!! Form::text('organization_name', null, array('required', 'placeholder' => 'Въведете името на Вашата фирма')) !!}
        </div>
      </div>
      <div class='row'>
        <div class='small-12 large-3 columns'>
          {!! HTML::decode(Form::label('business_type', 'Вид бизнес <span>*</span>')) !!}
        </div>
        <div class='small-12 large-9 columns'>
          {!! Form::select('business_type', array("" => '', 'coffeeshop' => 'Кафетерия', 'hotel' => 'Хотел', 'shop' => 'Магазин', 'office' => 'Офис', 'petrol' => 'Бензиностанция'), "", array('required')) !!}
        </div>
      </div>
      <div class='row'>
        <div class='small-12 large-3 columns'>
          {!! HTML::decode(Form::label('city', 'Населено място <span>*</span>')) !!}
        </div>
        <div class='small-12 large-9 columns'>
          {!! Form::text('city', null, array('required', 'placeholder' => 'Въведете името на Вашето населено място')) !!}
        </div>
      </div>
      <div class='row'>
        <div class='small-12 large-3 columns'>
          {!! HTML::decode(Form::label('beverages', 'Брой напитки')) !!}
        </div>
        <div class='small-12 large-2 columns'>
          {!! Form::text('beverages', null, array('placeholder' => '0')) !!}
        </div>
        <div class='small-12 large-3 columns text-right' id='employees'>
          {!! HTML::decode(Form::label('employees', 'Брой на посетителите')) !!}
        </div>
        <div class='small-12 large-3 columns'>
          {!! Form::select('employees', array("" => '', 0 => '20 до 50', 1 => 'над 50'), "") !!}
        </div>
      </div>
      <div class='row'>
        <div class='small-12 large-3 columns'>
          {!! Form::label('message', 'Коментар') !!}
        </div>
        <div class='small-12 large-9 columns'>
          {!! Form::textarea('message', null, array('placeholder' => 'Вашия коментар')) !!}
        </div>
      </div>
      <div class='row'>
        <div class='small-12 large-9 columns large-offset-3'>
          <table class='simple'>
            <tr>
              <td>{!! Form::checkbox('accept_tos', 1, false, array('required')) !!}</td>
              <td>{!! HTML::decode(Form::label('accept_tos', 'Запознах се и се съгласявам "Нестле България" АД да обработва моите лични данни съгласно <a href=\'/information/security\'><strong>Политиката за поверителност на данните</strong></a>  <span>*</span>')) !!}</td>
            </tr>
            <tr>
              <td>{!! Form::checkbox('accept_delivery', 1, false, array('required')) !!}</td>
              <td>{!! HTML::decode(Form::label('accept_delivery', 'Запознат съм и приемам <a href=\'/information/security\'><strong>Правила и условия на сайта</strong></a> и с <a href=\'/delivery\'><strong>Условията на доставка</strong></a> за ползване на уебсайта <span>*</span>')) !!}</td>
            </tr>
            <tr>
              <td>{!! Form::checkbox('accept_subscription', 1, false) !!}</td>
              <td>{!! HTML::decode(Form::label('accept_subscription', 'Желая да получавам оферти, информация за промоции и други новини за Нестле България, марките и продуктите на компанията чрез електронни съобщeния (email). Разбирам, че личните ми данни ще бъдат обработвани в съответствие с <a href=\'/information/security\'><strong>Политиката за поверителност на данните</strong></a> . Информиран съм, че мога да оттегля съгласието си по всяко време чрез натискането на бутона “unsubscribe”, съдържащ се във всеки имейл.')) !!}</td>
            </tr>
          </table>
        </div>
      </div>
      <div class='row'>
        <div id='machine-id'>
          {!! Form::hidden('machine_id', ''); !!}
        </div>
        <div id='recipe-name'>
          {!! Form::hidden('recipe_name', ''); !!}
        </div>
        <div class='small-12 large-4 large-offset-3 columns'>
          {!! Form::submit('ЗАЯВИ КАФЕМАШИНА', array('class' => 'secondary button', 'id' => 'gtm-order-machine-form-button')) !!}
        </div>
      </div>
      {!! Form::close() !!}
      <div class='row'>
        <div class='small-12 large-3 columns'>
        </div>
        <div class='small-12 large-9 columns '>
          <p>Можете да се свържете с нас на имейл адрес: <a href='mailto:Nestle.Professional@bg.nestle.com'>Nestle.Professional@bg.nestle.com</a> или да ни се обадите на телефон: <a href="tel:070016606">0 700 16 606</a> (според тарифния план към фиксираната мрежа на A1), всеки работен ден от 9:00 до 17:30.</p>
        </div>
      </div>
    </section>
  </div>
@endsection
