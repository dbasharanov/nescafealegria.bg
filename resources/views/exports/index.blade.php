<!DOCTYPE html>
<html lang="en">
<head>
  @include('partials._head')
  <script type='text/javascript' src='{{ elixir('js/admin.js') }}'></script>
</head>
<body>
  <div class="off-canvas-wrap" data-offcanvas>
    <div class="off-canvas-wrapper-inner">

      <section id='content-section'>
        @include('partials._message')
        <div class='column'>
          <div class='row align-center'>
            <nav class='breadcrumbs small-12 medium-12 large-8 columns'>
              <a class='current arrow-back' href='/public_admin_59448435b0'>Назад</a>
            </nav>
            <div class='callout small-12 medium-12 large-8 columns'>
              {!! Form::open(array('route' => 'exports.orders', 'enctype' => 'multipart/form-data')) !!}
                @if(Request::get('start_date') != null || Request::get('start_date') != null && is_empty($statistics))
                  <p class='alert'>Няма намерени резултати за зададените критерии!</p>
                @else
                  <p><i>Моля попълнете начална и крайна дата</i></p>
                @endif
                <div class='row'>
                  <div class='small-12 medium-2 columns'>
                    {!! Form::label('from_date', 'От', ['class' => 'middle']) !!}
                  </div>
                  <div class='small-12 medium-4 columns'>
                    {!! Form::text('from_date', Request::get('from_date'), ['required', 'class' => 'with-datepicker', 'placeholder' => 'dd.mm.YYYY']) !!}
                  </div>
                  <div class='small-12 medium-2 columns'>
                    {!! Form::label('to_date', 'До', ['class' => 'middle']) !!}
                  </div>
                  <div class='small-12 medium-4 columns'>
                    {!! Form::text('to_date', Request::get('to_date'), ['required','class' => 'with-datepicker', 'placeholder' => 'dd.mm.YYYY']) !!}
                  </div>
                </div>
                <div class='row'>
                  <div class='small-12 medium-6 columns'>
                    <!-- {!! Form::select('form_type', array(0 => 'Заявки', 1 => 'Контакти', 2 => 'Абонати'), "", array('required')) !!} -->
                    {!! Form::select('form_type', array(0 => 'Заявки', 1 => 'Контакти', 3 => 'saleforce consumers', 4 => 'saleforce optin'), "", array('required')) !!}
                  </div>
                </div>
                <div class='row'>
                  <div class='small-12  columns'>
                    {!! Form::submit('експортиране', ['class' => 'button']) !!}
                  </div>
                </div>
              {!! Form::close() !!}
            </div>
          </div>
        </div>
      </section>
      <a class="exit-off-canvas"></a>
      <!-- @include('partials._bottom_menu') -->
    </div>
    <!-- @include('partials._footer') -->
  </div>
</body>
</html>


