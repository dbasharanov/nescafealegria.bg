@extends('app')

@section('content')
<div class='business-block wallpaper' style='background-image:url(/images/attachments/businesses/{!! $business->image !!})'>
    <div style='max-width: 250px;margin: auto;'>
        <a class='hide-for-large secondary button' href='/order'>ЗАЯВИ КАФЕМАШИНА</a>
    </div>
    <div class='show-for-large gradient-mask'></div>
    <img class='hide-for-large' src='/images/attachments/businesses/{!! $business->image !!}' />
    <div class='column small-12 info'>
        <nav class='breadcrumbs small-12 hide-for-small-only'>
            <a class='current arrow-back' href='/'>Начало</a>
        </nav>
        <div style='margin-top:20px;'>
            <h1>{!! $business->name !!}</h1>
            <p>{!! $business->description !!}</p>
            <div class='small-text-center'>
                <a class='button' href='/configuration/step-2' style='font-size:1.2rem;'>РЕШЕНИЕ ЗА ВАС</a>
            </div>
        </div>
    </div>
</div>
@if($testimonials->count() > 0)
    <div class="orbit row" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container small-12 " tabindex="0">
            <button class="orbit-previous" aria-label="previous" tabindex="0"><span class="show-for-sr">Previous Slide</span></button>
            <button class="orbit-next" aria-label="next" tabindex="0"><span class="show-for-sr">Next Slide</span></button>
            @foreach($testimonials as $index => $testimonial)
                <li class="orbit-slide small-12" data-slide="0" style='height:auto;    max-height: auto !important;'>
                    <div class="row align-right">
                        <div class="column medium-10 columns"><p><strong style='font-size:18px;'>Препоръки от наши клиенти</strong></p></div>
                    </div>
                    <div class='row'>
                        <div class='small-12 medium-2 columns text-center'>
                            <img src='/images/attachments/testimonials/{{$testimonial->image}}' class=''>        
                        </div>
                        <div class='small-12 medium-10 columns'>
                        {!! $testimonial->description !!}
                        <p>
                            <strong>{!! $testimonial->name !!}</strong><br>
                            <span class='position'>{{$testimonial->position}}, {{$testimonial->company}}<br>
                            {{$testimonial->type}}</span>
                        </p>
                        </div>
                    </div>
                </li>
            @endforeach
        </ul>
    </div>
@endif
@endsection