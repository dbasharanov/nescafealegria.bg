@extends('app')

@section('content')
  <div class='row'>
    <div class='small-12 column'>
      <h1>Промоции</h1>
      <h2>{{$promotion->title}}</h2>
      <img src='/images/attachments/notices/{{$promotion->image}}'>
    </div>
    <div class='large-6 medium-8 small-12 large-offset-3 medium-offset-2 columns' style='margin-bottom:80px;'>
      <p>
        {!! $promotion->description !!}
      </p>
      <div class='row'>
          <div class='btn shrink columns'>
            @include('partials._order_button')
          </div>
          <div class='share columns float-left text-right'>
            <a data-open='share-modal'><img src='/images/machines/share.svg'>Сподели</a>
          </div>
        </div>
        @include('partials._share_modal', array('link' => NescafeHelper::get_url()))
    </div>
  </div>
@endsection
