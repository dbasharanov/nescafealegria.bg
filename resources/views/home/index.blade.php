@extends('app')

@section('content')
    <div class='row'>
        <div class='small-12 home-business-wrapper'>
            <h1 class='hide'>Професионални кафемашини и кафе автомати.</h1>
            <h2 class='text-center'>Изберете своя бизнес</h2>
            <ul class='menu business-types text-center expanded icon-top'>
                <li id='shop'>
                    <a href='/businesses/shop'>
                        <div class='icon-circle'>
                            <i class='na-icons-grocery-shop'></i>
                        </div>
                        Магазин
                    </a>
                </li>
                <li id='hotel'>
                    <a href='/businesses/hotel'>
                        <div class='icon-circle'>
                            <i class='na-icons-building'></i>
                        </div>
                        Хотел
                    </a>
                </li>
                <li id='coffeeshop'>
                    <a href='/businesses/coffeeshop'>
                        <div class='icon-circle'>
                            <i class='na-icons-cup'></i>
                        </div>
                        Кафетерия
                    </a>
                </li>
                 <li id='office'>
                    <a href='/businesses/office'>
                        <div class='icon-circle'>
                            <i class='na-icons-office-chair'></i>
                        </div>
                        Офис
                    </a>
                </li>
                <li id='petrol'>
                    <a href='/businesses/petrolstation'>
                        <div class='icon-circle'>
                            <i class='na-icons-gas-station'></i>
                        </div>
                        Бензиностанция
                    </a>
                </li>
            </ul>
        </div>
    </div>

    @include('partials._slider', array('slides' => $slides))


    <section class='banner-banner'>
        @if($notice)
            <div class='small-12 medium-10 large-10 medium-offset-1 large-offset-1'>
                <div class='row align-center homepage-label'>
                    <a class='column' href='/promotions/{{$notice->slug}}'>
                        <div class='large-12'>
                            <img src='/images/attachments/notices/banners/{{$notice->banner}}' alt='{{$notice->title}}'>
                        </div>
                    </a>
                </div>
            </div>
        @endif
    </section>
    <section class='colorful' id='video'> 
        <div class='video-overlay' data-open='modalVideo'>
            <a href='javascript:;'><span class='play-button'><span class='triangle'></span></span></a>
        </div>        
        <video autoplay="autoplay" loop="loop" class="video" id='homepage-video' src="/video.mp4"></video>
        <div class="reveal large" id="modalVideo" data-reveal>
            <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
            </button>
            <div class='large-8 medium-10 small-12 large-offset-2 medium-offset-1'> 
                <div class='flex-video'>
                    <iframe width="400" height="300" frameborder="0" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
    </section>
    <section class='colorful text-left' id='installation-and-service'>
        <div class='wrapper'>
            <div class='large-6 medium-6 small-12 column float-left icon-holder'>
				<i class='na-icons-screwdriver'></i>
			</div>
            <div class='large-6 medium-6 small-12 column float-left'>
                <h2>Безплатно инсталиране и сервиз</h2>
                <p>Предлагаме Ви удобно решение - кафемашина NESCAFÉ&reg; Alegria с безплатен монтаж и сервизно обслужване. Можете да закупите само необходимите продукти и аксесоари за напитките.</p>
                <!-- <p><a href='#'>Научи повече</a></p> -->
            </div>
        </div>
    </section>
    <section class='colorful text-right' id='easy-management'>
        <div class='wrapper'>
            <div class='large-6 medium-6 small-12 column float-right icon-holder'>
                <i class='na-icons-click'></i>
            </div>
            <div class='large-6 medium-6 small-12 column float-right'>
                <h2>Лесно управление</h2>
                <p>NESCAFÉ&reg; Alegria кафемашината е лесна за използване и поддръжка. Напитката е готова само с натискането на един бутон!</p>
                <!-- <p><a href='#'>Научи повече</a></p> -->
            </div>
        </div>
    </section>
    <section class='colorful text-left' id='cheaper-solution'>
        <div class='wrapper'>
            <div class='large-6 medium-6 small-12 column float-left icon-holder'>
				<i class='na-icons-ease-of-use'></i>
			</div>
            <div class='large-6 medium-6 small-12 column float-right'>
                <h2>Икономичен разход</h2>
                <p>Ниската консумация на електричество на NESCAFÉ&reg; Alegria кафемашината позволява включването й в конвенционалната електрическа мрежа и намалява Вашите разходи.</p>
                <!-- <p><a href='#'>Научи повече</a></p> -->
            </div>
        </div>
    </section>
    <section class='colorful text-right' id='diversity'>
        <div class='wrapper'>
            <div class='large-6 medium-6 small-12 column float-right icon-holder'>
				<i class='na-icons-coffee-love'></i>
			</div>
            <div class='large-6 medium-6 small-12 column float-right'>
                <h2>Разнообразие от любими напитки</h2>
                <p>Вашите клиенти, персонал и гости ще оценят разнообразието от топли и студени напитки: еспресо, капучино, лате макиато, мокачино или топло какао.</p>
                <!-- <p><a href='#'>Научи повече</a></p> -->
            </div>
        </div>
    </section>
    <section class='colorful text-left' id='fast'>
        <div class='wrapper'>
            <div class='large-6 medium-6 small-12 column float-left icon-holder'>
				<i class='na-icons-speedometer'></i>
			</div>
            <div class='large-6 medium-6 small-12 column float-left'>
                <h2>Висока скорост</h2>
                <p>За няколко секунди NESCAFÉ&reg; Alegria кафемашината приготвя вкусна напитка по Ваш избор и с винаги високо и постоянно качество! Високата ефективност позволява приготвянето на 200 чаши на час.</p>
                <!-- <p><a href='#'>Научи повече</a></p> -->
            </div>
        </div>
    </section>
    <section class='colorful text-right' id='order'>
        <div style='max-width: 250px;margin: auto;'>
            @include('partials._order_button')
        </div>
    </section>
@endsection
