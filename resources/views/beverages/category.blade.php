@extends('app')

@section('content')
  <div class='beverages' id="section{!! $category->id - 1 !!}">
    <div class='row column'>
      <a class='show-for-large arrow-back' href='/beverages/'>Назад</a>
      <h1>{{ $category->name }}</h1>
      @foreach($category->beverages as $index => $beverage)
        <div class='section beverage'>
          <div class='row align-middle align-center'>
            <div class="small-6 medium-6 large-6 columns {!! $index%2 == 1 ? 'text-left' : 'text-right' !!} small-order-{!! $index%2 !!}">
              <h3>{{ $beverage->name }}</h3>
            </div>
            <div class="small-6 medium-6 large-6 columns {!! $index%2 == 1 ? 'text-right' : 'text-left' !!}">
              <a href='/beverages/{{ $beverage->getSlug() }}'>
                <img src='/images/attachments/beverages/{{$beverage->image}}'>
              </a>
            </div>
          </div>
        </div>
      @endforeach
    </div>
  </div>
@endsection