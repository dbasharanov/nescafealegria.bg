@extends('app')

@section('content')
  <div style='max-width: 250px;margin: auto;'>
    <a class='show-for-small-only secondary large button' href='/order'>ЗАЯВИ КАФЕМАШИНА</a>
  </div>
  <div class='row column'>
    <h1>Меню напитки</h1>
  </div>
  @foreach($categories as $index => $category)
    <div class='products' id='{{$category->slug}}'>
      <div class='row column'>
        <h2>{{$category->name}}</h2>
      </div>
      <div class='row small-up-1 medium-up-3 large-up-4'>
        @foreach($category->beverages as $index => $beverage)
          <div class='columns rectangle'>
            <div class='product-container text-center'>
              <a href='/beverages/{{ $beverage->getSlug() }}'>
                <img src='/images/attachments/beverages/{{$beverage->image}}'>
                <div class='description-container text-left'>
                  <h3>{{$beverage->name}}</h3>
                  <p>{{$beverage->summary}}</p>
                </div>
              </a>
            </div>
          </div>
        @endforeach
      </div>
    </div>
  @endforeach
@endsection