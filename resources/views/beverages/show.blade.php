@extends('app')

@section('content')
    <div class='inner-wrapper beverage-container'>
        <div class='row align-middle' style='margin-bottom:80px'>
            <div class='columns small-12 medium-12 large-6 text-center large-order-2'>
                <img src='/images/attachments/beverages/{{$beverage->image}}'>
                <div class='show-for-small-only share column small-6 medium-4 float-left text-right'>
                    <a data-open='share-modal'><img src='/images/machines/share.svg'>Сподели</a>
                </div>
                <div class='show-for-small-only share column small-6 medium-4 float-right text-right'>
                  @if($prev)
                    <a class='box prev wallpaper' href='/beverages/{{$prev->slug}}'></a>
                  @endif
                  @if($next)
                    <a class='box next wallpaper' href='/beverages/{{$next->slug}}'></a>
                  @endif
                </div>
                <a class='box close wallpaper show-for-small-only' href='/beverages'></a>
            </div>
            <div class='columns small-12 medium-12 large-6 float-left'>
                <nav class='breadcrumbs small-12 show-for-medium'>
                    <a class='current arrow-back' href='/beverages'>Назад</a>
                </nav>
                <div class='text-wrapper clearfix'>
                    <h1>{{ $beverage->name }}</h1>
                    {!! $beverage->description !!}
                    <div class='show-for-medium share small-6 medium-4 float-left text-right'>
                        <a data-open='share-modal'><img src='/images/machines/share.svg'>Сподели</a>
                    </div>
                </div>  
                <div class='products-list'>
                    <h2>Продукти:</h2>
                    <div class='row small-up-2 medium-up-3 large-up-4 text-center'>
                        @foreach($beverage->products as $index => $product)
                            <div class='column'>
                                <a href='/products/{!! $product->getSlug() !!}'>
                                    <img src='/images/attachments/products/{{$product->image}}' alt='{{$product->name}}'>
                                    <p>{!! $product->name !!}</p>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('partials._share_modal', array('link' => NescafeHelper::get_url()))
@endsection