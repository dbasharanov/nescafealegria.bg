@extends('app')

@section('content')
<div class='row'>
    <div class='small-112 columns'>
        <div class='small-12 medium-6 large-5 columns'>
        <h1>Резултати от търсене</h1>
        @if($query && $count > 0)
            <h4>Показани 1-{{$count}} от {{$count}} резултата</h4>
        @elseif ($query != NULL && $count == 0)
            <h4>Няма намерени резултати</h4>
        @endif
        </div>
<!--
        {!! Form::open(array('url' => 'search', 'method' => 'get')) !!}
        <div class='small-12 medium-6 large-4 columns'>
            <div class='input-group'>
              <input name='query' class='input-group-field' type='text' value="{{$query}}" placeholder='Търси за...'>
              <div class='input-group-button'>
                {!! Form::submit('Търси', ['class' => 'button']) !!}
              </div>
            </div>
        </div>
        {!! Form::close() !!} -->
        <div class='small-12 medium-12 large-8 columns search_results'>
            @foreach($results[0] as $machine)
            <div class='result'>
                <ul class="no-bullet">
                    <li><a href="/machines/{{$machine->getSlug() }}"><strong>{{$machine->name}}</strong></a></li>
                    <li>{{strip_tags($machine->description)}}</li>
                </ul>
            </div>
            @endforeach
            @foreach($results[1] as $product)
            <div class='result'>
                <ul class="no-bullet">
                    <li><a href="/products/{{ $product->getSlug() }}"><strong>{{$product->name}}</strong></a></li>
                    <li>{{strip_tags($product->description)}}</li>
                </ul>
            </div>
            @endforeach
            @foreach($results[2] as $beverage)
            <div class='result'>
                <ul class="no-bullet">
                    <li><a href="/beverages/{{ $beverage->getSlug() }}"><strong>{{$beverage->name}}</strong></a></li>
                    <li>{{strip_tags($beverage->description)}}</li>
                </ul>
            </div>
            @endforeach
            @foreach($results[3] as $business)
            <div class='result'>
                <ul class="no-bullet">
                    <li><a href="/businesses/{{ $business->getSlug() }}"><strong>{{$business->name}}</strong></a></li>
                    <li>{{strip_tags($business->description)}}</li>
                </ul>
            </div>
            @endforeach
        </div>
    </div>
</div>
@endsection
