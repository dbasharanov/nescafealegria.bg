<script>
  <!-- Google Tag Manager -->
  (function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
  j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
  'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
  })(window,document,'script','dataLayer','GTM-5VCG3X');
<!-- End Google Tag Manager -->
</script>
@if(!empty($meta))
  <title>{{$meta['title']}}</title>
  <meta charset="utf-8"/>

  <meta name="description" content="{{$meta['description']}}"/>
  <meta name="keywords" content="{{$meta['keywords']}}">

  <meta property="og:site_name"         content="nescafealegria.bg"/>
  <meta property="og:locale"            content="bg_BG"/>
  <meta property="og:type"              content="article"/>
  <meta property="og:title"             content="{{$meta['title']}}"/>
  <meta property="og:description"       content="{{$meta['description']}}"/>
  <meta property="og:url"               content="{{NescafeHelper::get_url()}}" />
  <meta property="og:image"             content="{{$meta['image']}}"/>
  <meta property="og:image:secure_url"  content="{{$meta['image']}}"/>
  <meta property="og:image:type"        content="image/png">
@endif
<link rel="canonical" href='{{NescafeHelper::get_url()}}'/>

<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">


<link href="https://fonts.googleapis.com/css?family=Lobster|Open+Sans:400,700|Roboto+Condensed:400,700" rel="stylesheet">
<link href="{{ elixir('css/app.css') }}" rel="stylesheet">
<script src="https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.2/rollups/aes.js"></script>
<link rel="apple-touch-icon" sizes="57x57" href="/images/app_icons/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/images/app_icons/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/images/app_icons/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/images/app_icons/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/images/app_icons/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/images/app_icons/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/images/app_icons/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/images/app_icons/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/images/app_icons/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/images/app_icons/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/images/app_icons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/images/app_icons/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/images/app_icons/favicon-16x16.png">
<link rel="manifest" href="/images/app_icons/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/images/app_icons/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">