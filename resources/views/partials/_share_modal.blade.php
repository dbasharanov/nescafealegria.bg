<div class='reveal' id='share-modal' data-reveal>
  <h1>Сподели:</h1>
  <div class='row large-up-4'>
    <div class='column'>
      <div class="show-for-medium-up hide-for-small-only">
        <a href="https://www.facebook.com/sharer/sharer.php?sdk=joey&display=popup&ref=plugin&src=share_button&app_id=1013032535404130&u={!! $link !!}" onclick='return !window.open(this.href, "Facebook", "width=640,height=300")' class="facebook socials" data-medium='facebook'></a>
      </div>
      <div class="show-for-small-only hide-for-medium-up">
        <a href="https://m.facebook.com/sharer.php?u={!! $link !!}" onclick='return !window.open(this.href, "Facebook", "width=640,height=300")'  class="facebook socials" data-medium='facebook'></a>
      </div>
    </div>
    <div class='column'>
      <a href="https://twitter.com/intent/tweet?url={!! strip_tags($link) !!}&amp;original_referer={!! $link !!}"  class="twitter socials" data-medium='twitter'></a>
    </div>
    <div class='column'>
      <a href="https://plus.google.com/share?url={!! $link !!}" onclick='return !window.open(this.href, "gplus", "width=640,height=300")' class="google socials" data-medium='google+'></a>
    </div>
    <div class='column'>
        <script async defer src="//assets.pinterest.com/js/pinit.js"></script>
        <div class="pinterest">
          <a class="social_popup_link" target="_blank" href="http://pinterest.com/pin/create/button/?url={!! $link !!}/&amp;media=https://nescafealegria.bg/{!! $meta['image']!!}&amp;description={!! $meta['description'] !!}" title="Pin It"></a>
        </div>
    </div>
  </div>
  <button class='close-button' data-close aria-label='Close modal' type='button'>
    <span aria-hidden="true">&times;</span>
  </button>
</div>