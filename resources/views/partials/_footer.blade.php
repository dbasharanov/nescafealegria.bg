<footer class='wallpaper'>

    <div class='row align-justify'>
        <div class='column small-12 medium-6'>
            <p class='small-text-center'>Абонирам се, за да получавам оферти, информация за промоции и други новини за Нестле България, марките и продуктите на компанията чрез електронни съобщeния (email). Разбирам, че личните ми данни ще бъдат обработвани в съответствие с <a href='/information/security'><strong>Политиката за поверителност на данните</strong></a>. Информиран съм, че мога да оттегля съгласието си по всяко време чрез натискането на бутона “unsubscribe”, съдържащ се във всеки email.</p>
            {!! Form::open(array('route' => 'subscriptions.store')) !!}
                <div class='small-8 float-left' style='width:100%;'>
                    {!! Form::email('email', null, array('class' => 'newsletter-field', 'required', 'placeholder' => 'Въведи мейл.')) !!}
                </div>
                <div class='small-4 float-left'>
                    {!! Form::submit('АБОНИРАЙ СЕ', array('class' => 'medium button footer-button')) !!}
                </div>
            {!! Form::close() !!}
        </div>
        <div class='column shrink align-middle'>
            <a href='/files/NESTLE Professional_presentation.pdf' target='_blank' class='pdf'>
                <i class='na-icons-pdf'></i>
                Информация за нашият продукт
            </a>
        </div>
    </div>
    <div class='row align-justify'>
        <div class='column small-12 medium-12 large-2'>
            <img src='/images/logo_rgb.svg' class='logo' alt='nestle professional'>
        </div>
        <div class='column small-12 medium-12 large-10'>
            <ul id='footer-menu' class='menu horizontal nav float-right text-center'>
                <li><a class="{{ preg_match('/delivery/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='/delivery'>Условия за доставка</a></li>
                <li><a class="{{ preg_match('/configuration/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='/configuration/step-1'>Решение за Вас</a></li>
                <li><a class="{{ preg_match('/beverages/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='{{ route('beverages.index') }}'>Меню напитки</a></li>
                <li><a class="{{ preg_match('/machines/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='{{ route('machines.index') }}'>Кафемашини</a></li>
                <li>@include('partials._order_button')</li>
                <li>
                    <div class='row column' style='display:inline-flex;'>
                        <span class='icons na-icons-phone'></span>
                        <span><a class='call' href="tel:070016606">0 700 16 606</a></span>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <div class='row align-right'>
        <div class='small-12 columns text-right block-wrapper'>
            <a href='/information/tos'>Правила и условия на сайта</a> |
            <a href='/information/security'>Политика за поверителност на данните</a>
            | <a href='/information/cookies'>Политика за Бисквитки (Cookies)</a>
            <p>{{date("Y")}} NESCAFÉ Alegria &reg; All rights reserved.</p>
            <div id='scroll-to-top' class='wallpaper'></div>
        </div>
    </div>
    <div style='width: 100%; max-width: 100%;' class='row'>
        <div class='small-12 columns text-center'>
            <div class='small-12 clearfix'>
            </div>
        </div>
        <div class='small-12 columns text-center'>

        </div>
        <div class='small-12 columns text-center'>

        </div>
        <hr>
    </div>
</footer>
