<nav id='bottom-nav'>
    <div class='row menu horizontal small-up-1 medium-up-5 large-up-5'>
        <div class='column text-center'><a class="{{ Request::is( 'promotions') ? 'nav active' : 'nav' }}" href='/promotions/'>Промоции</a></div>
        <div class='column text-center'><a class='nav' href='/configuration/step-1'>Решение за Вас</a></div>
        <div class='column text-center'><a class="{{ Request::is( 'benefits') ? 'nav active' : 'nav' }}" href='{{ route('benefits.index') }}'>Предимства</a></div>
        <div class='column text-center'><a class="{{ preg_match('/nescafe-plan/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='/nescafe-plan'>NESCAFÉ&reg; Plan</a></div>
        <div class='column text-center'><a class="{{ Request::is( 'contacts') ? 'nav active' : 'nav' }}" href='{{ route('contacts.index') }}'>Контакти</a></div>
    </div>
</nav>
