<div class='title-bar' data-responsive-toggle='main-nav'>
	<button class='menu-icon' type='button' data-toggle></button>
  	<div class='title-bar-title'>
    		<img id='logo-mobile' src='/images/nescafe-alegria-logo.svg'>
        	<span class='moonflower'>ADMIN</span>
  </div>
</div>
<div class='top-bar' id='main-nav'>
    <div class='top-bar-right'>
        <ul id='right-menu' class='horizontal dropdown menu float-right' data-dropdown-menu>
        	<li><a class='nav{{Request::is('admin_rlAPkBI5RRPqmty1pVu4/visits*') ? ' active' : null}}' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.visits.index')}}'>Чекирани обекти</a></li>
        	<li><a class='nav{{Request::is('admin_rlAPkBI5RRPqmty1pVu4/orders*') ? ' active' : null}}' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.orders.index')}}' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.orders.index')}}'>Продажби</a></li>
        	<li><a class='nav{{Request::is('admin_rlAPkBI5RRPqmty1pVu4/cycles*') ? ' active' : null}}' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.cycles.index')}}' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.cycles.index')}}'>Цикли на посещение</a></li>
            <li><a class='nav{{Request::is('admin_rlAPkBI5RRPqmty1pVu4/objects*') ? ' active' : null}}' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.objects.index')}}' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.objects.index')}}'>Зачислени Обекти</a></li>
            <li><a class='nav{{Request::is('admin_rlAPkBI5RRPqmty1pVu4/users*') ? ' active' : null}}' href='{{route('admin_rlAPkBI5RRPqmty1pVu4.users.index')}}'>Потребители</a></li>
        	<li>
				<a href='javascript:void(0)' class='nav'><i class="fa fa-user" aria-hidden="true"></i>{{Auth::user()->name}}</a>
			    <ul class='menu vertical'>
			    	<li><a class='nav' onclick="return confirm('Сигурни ли сте че искате да излезете?')" href='/auth/logout/'>Изход</a></li>
			    </ul>
			</li>
        </ul>
    </div>
    <div id='logo-container' class='top-bar-left'>
    	<a href='/admin_rlAPkBI5RRPqmty1pVu4'>
        	<div id='site-logo'>
            	<img src='/images/nescafe-alegria-logo.svg'>
            	<!-- <div class='moonflower'>ADMINISTRATION</div> -->
        	</div>
        </a>
    </div>
</div>
