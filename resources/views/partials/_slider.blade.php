<div class='orbit-wrapper'>
    <div class="orbit" role="region" aria-label="Favorite Space Pictures" data-orbit>
        <ul class="orbit-container">
            <button class="orbit-previous wallpaper"></button>
            <button class="orbit-next wallpaper"></button>
            @foreach($slides as $slide)
                <li class="is-active orbit-slide">
                  <div style='background-image:url(/images/attachments/slides/large/{{$slide->image}})' class='small-12 medium-6 wallpaper slide float-left'></div>
                  <div class='orbit-caption-wrapper small-12 medium-6 float-left'>
                        <p class='font-30 strong'>{{$slide->title}}</p>
                        <p>{{$slide->description}}</p>
                        <a class='secondary button large-6' href='{{$slide->slug}}'>Заяви кафемашина</a>
                  </div>
                </li>
            @endforeach
        </ul>
        <nav class="orbit-bullets show-for-large">
            @foreach($slides as $index=>$slide)
                <button data-slide="{{$index}}">
                    <div style='background-image:url(/images/attachments/slides/small/{{$slide->bullet}})' class='wallpaper bullet float-left'></div>
                </button>
            @endforeach
        </nav>
    </div>
</div>
<div class='show-all-machines text-center'>
    <a class='font-21' href='/machines'>
        Виж всички кафемашини
        <img src='/images/arrow-l.svg' alt='arrow' width='10'>
    </a>
</div>