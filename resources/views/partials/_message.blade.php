@if (session('message'))
    <div class='column row'>
    	<div class='{{session('status')}} callout'>
        	{{ session('message') }}
    	</div>
    </div>
@endif
@if(count($errors->all()) > 0)
    <div class='column row'>
    	<div class='danger callout'>
        	@foreach ($errors->all() as $error)
            	<p class="error">{{ $error }}</p>
        	@endforeach
    	</div>
    </div>
@endif
