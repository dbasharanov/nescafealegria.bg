<div class='top-bar show-for-large' id='main-nav'>
    <div id='logo-container' class='top-bar-left'>
        <div id='site-logo'>
            <a href='/'>
                <img src='/images/nescafe-alegria-logo.svg'>
                @if (Request::is('/'))
                  <div class=''><img src='/images/slogan.svg'></div>
                @endif
            </a>
        </div>
    </div>
    <div class='top-bar-right'>
        <ul id='right-menu' class='menu horizontal float-right'>
          <li><a class="{{ preg_match('/machines/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='{{ route('machines.index') }}'>Кафемашини</a></li>
          <li><a class="{{ preg_match('/beverages/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='{{ route('beverages.index') }}'>Меню напитки</a></li>
          <li><a class="{{ preg_match('/products/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='{{ route('products.index') }}'>Продукти</a></li>
          <li><a class="{{ preg_match('/delivery/',NescafeHelper::get_url()) ? 'nav active' : 'nav' }}" href='/delivery'>Условия за доставка</a></li>
          <li>
            <!-- <a class='nav' href='{{ route('search.index') }}'><span class="icons na-icons-search"></span></a> -->
            <div class='search-wrapper row'>
              <form action='/search' method='get' class='search-form left' style="{{ preg_match('/search/',NescafeHelper::get_url()) ? 'display:block;' : 'display:none;' }}" >
                <input type='text' name='query' id='query' placeholder='Търси' required='' value="">
              </form>
              <a href='javascript:;' class='nav search-trigger na-icons-search columns right'></a>
            </div>
          </li>
          <li>@include('partials._order_button')</li>
        </ul>
        <div id='phone-number'>
          <span class='icons na-icons-phone'></span>
          <span><a class='call' href="tel:070016606">0 700 16 606</a></span>
        </div>
    </div>
</div>
<div class='mobile-top-bar title-bar hide-for-large'>
  <a href='javascript:;' id='menu-trigger' class='unactive'></a>
  <a href='/' class='logo-mobile'></a>
	<div class='float-right' style='line-height: 66px'>
      <span class='icons na-icons-phone'></span>
      <span><a class='call' href="tel:070016606">0 700 16 606</a></span>
    </div>
</div>
<div class='dropdown-menu' id='mobile-nav'>
  <ul class='no-bullet row small-up-1 text-center'>
    <li class='column'>
      <a class='nav' href='/delivery'>Условия за доставка</a>
    </li>
    <li class='column'>
      <a class='nav' href='/configuration/step-1'>Решение за Вас</a>
    </li>
    <li class='column'>
      <a class='nav' href='{{ route('beverages.index') }}'>Меню напитки</a>
    </li>
    <li class='column'>
      <a class='nav' href='{{ route('machines.index') }}'>Кафемашини</a>
    </li>
    <li class='column'>
      <div class='search-wrapper row' style='display: inline-flex;'>
        <form action='/search' method='get' class='mobile-search-form left' style='display:block;' >
          <input type='text' name='query' id='mobile-query' placeholder='Търси' required='' value="{{ $query or ''  }}">
          <a href='javascript:;' class='nav search-trigger na-icons-search columns right'></a>
        </form>
      </div>
    </li>
    <li class='column'>
      @include('partials._order_button')
    </li>
  </ul>
</div>