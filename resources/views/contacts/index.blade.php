@extends('app')

@section('content')
	<div class='row contact-form-wrapper'>
		<div class='small-12 column'>
			<h1>Контакти</h1>
		</div>
		<div class='small-12 large-3 columns large-order-2'>
			<p>Можете да се свържете с нас на имейл адрес: <a href='mailto:Nestle.Professional@bg.nestle.com'>Nestle.Professional@bg.nestle.com</a> или да ни се обадите на телефон: <a href="tel:070016606">0 700 16 606</a> (според тарифния план към фиксираната мрежа на A1), всеки работен ден от 9:00 до 17:30.</p>
		</div>
		<section id='form-section' class='small-12 large-9 columns large-order-1'>
			{!! Form::open(array('route' => 'contacts.store')) !!}
			<div class='row'>
				<div class='small-12 large-3 columns'>
					{!! HTML::decode(Form::label('question_type', 'С какво можем да Ви бъдем полезни?  <span>*</span>')) !!}
				</div>
				<div class='small-12 large-9 columns'>
					{!! Form::select('question_type', array("" => '', 0 => 'Запитване за продукт', 1 => 'Запитване за машина', 2 => 'Контакт с търговски представител', 3 => 'Друго'), "", array('required')) !!}
				</div>
			</div>
			<div class='row'>
				<div class='small-12 large-3 columns'>
					{!! HTML::decode(Form::label('message', 'Съобщение <span>*</span>')) !!}
				</div>
				<div class='small-12 large-9 columns'>
					{!! Form::textarea('message', null, array('required', 'placeholder' => 'Вашето съобщение')) !!}
				</div>
			</div>
			<div class='row'>
				<div class='small-12 large-3 columns'>
					{!! HTML::decode(Form::label('city', 'Населено място <span>*</span>')) !!}
				</div>
				<div class='small-12 large-9 columns'>
					{!! Form::text('city', null, array('required', 'placeholder' => 'Въведете името на Вашето населено място')) !!}
				</div>
			</div>
			<div class='row'>
				<div class='small-12 large-3 columns'>
					{!! HTML::decode(Form::label('first_name', 'Лице за контакт <span>*</span>')) !!}
				</div>
				<div class='small-12 large-4 columns'>
					{!! Form::text('first_name', null, array('required', 'placeholder' => 'Име')) !!}
				</div>
				<div class='small-12 large-5 columns'>
					{!! Form::text('last_name', null, array('required', 'placeholder' => 'Фамилия')) !!}
				</div>
			</div>
			<div class='row'>
				<div class='small-12 large-3 columns'>
					{!! HTML::decode(Form::label('phone_number', 'Данни за контакт  <span>*</span>')) !!}
				</div>
				<div class='small-2 large-1 columns'><label class='text-right middle'>+359</label></div>
				<div class='small-10 large-3 columns'>
					{!! Form::text('phone_number', null, array('required', 'placeholder' => 'Телефон')) !!}
				</div>
				<div class='small-12 large-5 columns'>
					{!! Form::email('email', null, array('required', 'placeholder' => 'E-mail')) !!}
				</div>
			</div>
			<div class='row'>
				<div class='small-12 columns'>
					<table class='simple'>
						<tr>
							<td>{!! Form::checkbox('accept_tos', 1, false, array('required')) !!}</td>
							<td>{!! HTML::decode(Form::label('accept_tos', 'Запознах се и се съгласявам "Нестле България" АД да обработва моите лични данни съгласно <a href=\'/information/security\'><strong>Политиката за поверителност на данните</strong></a>  <span>*</span>')) !!}</td>
						</tr>
						<tr>
							<td>{!! Form::checkbox('accept_subscription', 1, false) !!}</td>
							<td>{!! HTML::decode(Form::label('accept_subscription', 'Желая да получавам оферти, информация за промоции и други новини за Нестле България, марките и продуктите на компанията чрез електронни съобщeния (email). Разбирам, че личните ми данни ще бъдат обработвани в съответствие с <a href=\'/information/security\'><strong>Политиката за поверителност на данните</strong></a> . Информиран съм, че мога да оттегля съгласието си по всяко време чрез натискането на бутона “unsubscribe”, съдържащ се във всеки имейл.')) !!}</td>
						</tr>
					</table>
				</div>
			</div>
			<div class='column row'>
				{!! Form::submit('Изпрати', array('class' => 'large primary button', 'id' => 'gtm-send-contact-button' )) !!}
			</div>
			{!! Form::close() !!}
		</section>
	</div>
@endsection
