@extends('app')

@section('content')
  <div style='max-width: 250px;margin: auto;'>
    <a class='show-for-small-only secondary button' href='/order'>ЗАЯВИ КАФЕМАШИНА</a>
  </div>
  <div class='row column'>
  	<h1>Предимства на професионалните кафемашини</h1>
  </div>
	<div class='benefits-row row small-up-1 medium-up-2 large-up-3'>
		<div class='square column'>
			<div id='low-expenses' class='benefit-container'>
				<div class='icon-container'>
					<div class='table'>
						<div class='content'>
							<p><i class='na-icons-coffee-love'></i></p>
							<p>Без високи разходи</p>
						</div>
					</div>
				</div>
				<div class='description-container'>
					<p>Няма нужда от първоначалната инвестиция. С Вашата NESCAFÉ&reg; Alegria избирате надеждност, ефикасност, удобство, бързина - кафе решение, което може да отговори изцяло на нуждите на Вашия бизнес.</p>
				</div>
			</div>
		</div>
		<div class='square column'>
			<div id='installation' class='benefit-container'>
				<div class='icon-container'>
					<div class='table'>
						<div class='content'>
							<p><i class='na-icons-coffee-love'></i></p>
							<p>Инсталация, поддръжка и сервиз</p>
						</div>
					</div>
				</div>
				<div class='description-container'>
					<p>Технически екип ще се погрижи изцяло за първоначалното инсталиране, както и за последваща техническа поддръжка на Вашата NESCAFÉ&reg; Alegria машина. Може да разчитате на сервиз с национално покритие, като осигурите допълнителна надеждност на своята машина.</p>
				</div>
			</div>
		</div>
		<div class='square column'>
			<div id='courses' class='benefit-container'>
				<div class='icon-container'>
					<div class='table'>
						<div class='content'>
							<p><i class='na-icons-like'></i></p>
							<p>Обучение</p>
						</div>
					</div>
				</div>
				<div class='description-container'>
					<p>При всяко посещение на наш представител може да разчитате на първоначални и опреснителни обучения за Вашия персонал. Ние ще Ви обучим ежедневно бързо и лесно да почиствате, поддържате и зареждате Вашата NESCAFÉ&reg; Alegria машина.</p>
				</div>
			</div>
		</div>
		<div class='square column'>
			<div id='recipes' class='benefit-container'>
				<div class='icon-container'>
					<div class='table'>
						<div class='content'>
							<p><i class='na-icons-recipes'></i></p>
							<p>Рецепти</p>
						</div>
					</div>
				</div>
				<div class='description-container'>
					<p>Като използваме своята кафе експертиза, ние разработваме лесни за приготвяне, апетитни напиткови рецепти. Допълнително може да разчитате и на професионални съвети за приготвянето на кафе напитки и специалитети. Предложете повече разнообразие на своите клиенти!</p>
				</div>
			</div>
		</div>
		<div class='square column'>
			<div id='quality-control' class='benefit-container'>
				<div class='icon-container'>
					<div class='table'>
						<div class='content'>
							<p><i class='na-icons-quality'></i></p>
							<p>Контрол на качеството</p>
						</div>
					</div>
				</div>
				<div class='description-container'>
					<p>С NESCAFÉ&reg; Alegria и подкрепата от нашия екип може да разчитате винаги на високо качество на напитките. Всеки път - чаша след чаша.</p>
				</div>
			</div>
		</div>
		<div class='square column'>
			<div id='accessories' class='benefit-container'>
				<div class='icon-container'>
					<div class='table'>
						<div class='content'>
							<p><i class='na-icons-advertising'></i></p>
							<p>Аксесоари и комуникационна подкрепа</p>
						</div>
					</div>
				</div>
				<div class='description-container'>
					<p>Поднесете на своите клиенти всяка напитка в автентичен вид с NESCAFÉ&reg; Alegria аксесоари, а ние ще Ви предоставим и специално разработени комуникационни и рекламни материали. Продавайте повече с NESCAFÉ&reg; Alegria!  </p>
				</div>
			</div>
		</div>
	</div>
@endsection
